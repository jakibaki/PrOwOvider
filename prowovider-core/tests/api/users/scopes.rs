/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use hyper::StatusCode;
use prowovider_core::models::{
	OrganisationModel, OrganisationScopeModel, OrganisationUserModel, OrganisationUserScopeModel,
};

use crate::api::{
	basic_auth, get, get_unsorted, global_test_administrator, path, read_settings, spawn_server,
	ORG_1_UUID,
};

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_all_scopes_on_user_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let user1 = pool.new_user(ORG_1_UUID, "user-1", "whatever").await.unwrap();
	let scope1 = pool.new_scope(ORG_1_UUID, "scowope-1").await.unwrap();
	pool.new_scope_on_user(ORG_1_UUID, user1.id, scope1.scope.as_str()).await.unwrap();
	let scope2 = pool.new_scope(ORG_1_UUID, "scowope-2").await.unwrap();
	pool.new_scope_on_user(ORG_1_UUID, user1.id, scope2.scope.as_str()).await.unwrap();
	let _scope3 = pool.new_scope(ORG_1_UUID, "scowope-3").await.unwrap();
	let scope4 = pool.new_scope(ORG_1_UUID, "scowope-4").await.unwrap();
	pool.new_scope_on_user(ORG_1_UUID, user1.id, scope4.scope.as_str()).await.unwrap();

	let admin = global_test_administrator();
	let path = format!("/api/v1/organisations/{}/users/{}/scopes", ORG_1_UUID, user1.id);
	get_unsorted::<String>(
		path.as_str(),
		port,
		Some(basic_auth(admin.username.as_str(), admin.password.as_str())),
		vec![scope1.scope, scope2.scope, scope4.scope],
	)
	.await;
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_scope_on_user_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let user1 = pool.new_user(ORG_1_UUID, "user-1", "whatever").await.unwrap();
	let scope1 = pool.new_scope(ORG_1_UUID, "scowope-1").await.unwrap();
	pool.new_scope_on_user(ORG_1_UUID, user1.id, scope1.scope.as_str()).await.unwrap();
	let scope2 = pool.new_scope(ORG_1_UUID, "scowope-2").await.unwrap();
	pool.new_scope_on_user(ORG_1_UUID, user1.id, scope2.scope.as_str()).await.unwrap();

	let admin = global_test_administrator();
	let path =
		format!("/api/v1/organisations/{}/users/{}/scopes/{}", ORG_1_UUID, user1.id, scope1.scope);
	get(
		path.as_str(),
		port,
		Some(basic_auth(admin.username.as_str(), admin.password.as_str())),
		StatusCode::OK,
		true,
	)
	.await;
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_bad_scope_on_user_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let user1 = pool.new_user(ORG_1_UUID, "user-1", "whatever").await.unwrap();
	let scope1 = pool.new_scope(ORG_1_UUID, "scowope-1").await.unwrap();
	pool.new_scope_on_user(ORG_1_UUID, user1.id, scope1.scope.as_str()).await.unwrap();
	let scope2 = pool.new_scope(ORG_1_UUID, "scowope-2").await.unwrap();

	let admin = global_test_administrator();
	let path =
		format!("/api/v1/organisations/{}/users/{}/scopes/{}", ORG_1_UUID, user1.id, scope2.scope);
	get(
		path.as_str(),
		port,
		Some(basic_auth(admin.username.as_str(), admin.password.as_str())),
		StatusCode::OK,
		false,
	)
	.await;
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn post_scope_on_user_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let user1 = pool.new_user(ORG_1_UUID, "user-1", "whatever").await.unwrap();
	let scope1 = pool.new_scope(ORG_1_UUID, "scowope-1").await.unwrap();

	let admin = global_test_administrator();
	let api_path =
		format!("/api/v1/organisations/{}/users/{}/scopes/{}", ORG_1_UUID, user1.id, scope1.scope);
	let res = reqwest::Client::default()
		.post(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	assert!(pool.user_scope_exists(ORG_1_UUID, user1.id, scope1.scope.as_str()).await.unwrap());
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn delete_scope_on_user_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let user1 = pool.new_user(ORG_1_UUID, "user-1", "whatever").await.unwrap();
	let scope1 = pool.new_scope(ORG_1_UUID, "scowope-1").await.unwrap();
	pool.new_scope_on_user(ORG_1_UUID, user1.id, scope1.scope.as_str()).await.unwrap();

	let admin = global_test_administrator();
	let api_path =
		format!("/api/v1/organisations/{}/users/{}/scopes/{}", ORG_1_UUID, user1.id, scope1.scope);
	let res = reqwest::Client::default()
		.delete(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	assert!(!pool.user_scope_exists(ORG_1_UUID, user1.id, scope1.scope.as_str()).await.unwrap());
}

/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use hyper::StatusCode;
use prowovider_core::models::{
	OrganisationClaimModel, OrganisationModel, OrganisationUserClaimModel, OrganisationUserModel,
};

use crate::api::{
	basic_auth, get, global_test_administrator, path, read_settings, spawn_server, ORG_1_UUID,
};

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_all_claims_on_user_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let user1 = pool.new_user(ORG_1_UUID, "user-1", "whatever").await.unwrap();
	let claim1 = pool.new_claim(ORG_1_UUID, "claim1", None).await.unwrap();
	pool.upsert_claim_on_user(
		ORG_1_UUID,
		user1.id,
		claim1.claim_name.as_str(),
		&serde_json::json!({ "owo": "uwu" }),
	)
	.await
	.unwrap();
	let claim2 = pool.new_claim(ORG_1_UUID, "claim2", None).await.unwrap();
	pool.upsert_claim_on_user(
		ORG_1_UUID,
		user1.id,
		claim2.claim_name.as_str(),
		&serde_json::json!("test"),
	)
	.await
	.unwrap();

	let admin = global_test_administrator();
	let path = format!("/api/v1/organisations/{}/users/{}/claims", ORG_1_UUID, user1.id);
	get(
		path.as_str(),
		port,
		Some(basic_auth(admin.username.as_str(), admin.password.as_str())),
		StatusCode::OK,
		serde_json::json!({
			"claim1": {
				"owo": "uwu"
			},
			"claim2": "test"
		}),
	)
	.await;
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_claim_on_user_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let user1 = pool.new_user(ORG_1_UUID, "user-1", "whatever").await.unwrap();
	let claim1 = pool.new_claim(ORG_1_UUID, "claim1", None).await.unwrap();
	pool.upsert_claim_on_user(
		ORG_1_UUID,
		user1.id,
		claim1.claim_name.as_str(),
		&serde_json::json!({ "owo": "uwu" }),
	)
	.await
	.unwrap();
	let claim2 = pool.new_claim(ORG_1_UUID, "claim2", None).await.unwrap();
	pool.upsert_claim_on_user(
		ORG_1_UUID,
		user1.id,
		claim2.claim_name.as_str(),
		&serde_json::json!("test"),
	)
	.await
	.unwrap();

	let admin = global_test_administrator();
	let path = format!(
		"/api/v1/organisations/{}/users/{}/claims/{}",
		ORG_1_UUID, user1.id, claim2.claim_name
	);
	get(
		path.as_str(),
		port,
		Some(basic_auth(admin.username.as_str(), admin.password.as_str())),
		StatusCode::OK,
		String::from("test"),
	)
	.await;
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn put_claims_on_user_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let user1 = pool.new_user(ORG_1_UUID, "user-1", "whatever").await.unwrap();
	let claim1 = pool.new_claim(ORG_1_UUID, "claim1", None).await.unwrap();

	let admin = global_test_administrator();
	let api_path = format!(
		"/api/v1/organisations/{}/users/{}/claims/{}",
		ORG_1_UUID, user1.id, claim1.claim_name
	);
	let res = reqwest::Client::default()
		.put(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.json(&serde_json::json!("some-val"))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	assert_eq!(
		Some(serde_json::json!("some-val")),
		pool.get_claim_by_user_id_and_claim_name(ORG_1_UUID, user1.id, claim1.claim_name.as_str())
			.await
			.unwrap()
			.map(|x| x.claim_value)
	);
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn delete_claim_on_user_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let user1 = pool.new_user(ORG_1_UUID, "user-1", "whatever").await.unwrap();
	let claim1 = pool.new_claim(ORG_1_UUID, "claim1", None).await.unwrap();
	pool.upsert_claim_on_user(
		ORG_1_UUID,
		user1.id,
		claim1.claim_name.as_str(),
		&serde_json::json!("test"),
	)
	.await
	.unwrap();

	let admin = global_test_administrator();
	let api_path = format!(
		"/api/v1/organisations/{}/users/{}/claims/{}",
		ORG_1_UUID, user1.id, claim1.claim_name
	);
	let res = reqwest::Client::default()
		.delete(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	assert!(!pool
		.user_claim_exists(ORG_1_UUID, user1.id, claim1.claim_name.as_str())
		.await
		.unwrap());
}

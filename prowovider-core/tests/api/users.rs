/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

pub mod claims;
pub mod scopes;

use std::borrow::Cow;

use hyper::StatusCode;
use prowovider_api::{User, UserPassword};
use prowovider_core::models::{OrganisationModel, OrganisationUserModel};
use uuid::Uuid;

use crate::api::path;

use super::{
	basic_auth, get, get_unsorted, global_test_administrator, read_settings, spawn_server,
	ORG_1_UUID,
};

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_all_users_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let user1 = pool.new_user(ORG_1_UUID, "user-1", "whatever").await.unwrap();
	let user2 = pool.new_user(ORG_1_UUID, "user-2", "whatever-2").await.unwrap();

	let admin = global_test_administrator();
	let path = format!("/api/v1/organisations/{}/users", ORG_1_UUID);
	get_unsorted::<User>(
		path.as_str(),
		port,
		Some(basic_auth(admin.username.as_str(), admin.password.as_str())),
		vec![user1.into(), user2.into()],
	)
	.await;
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn post_user_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();

	let admin = global_test_administrator();
	let api_path = format!("/api/v1/organisations/{}/users", ORG_1_UUID);

	let res = reqwest::Client::default()
		.post(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.json(&UserPassword {
			username: Some(Cow::Borrowed("some-user")),
			password: Some(Cow::Borrowed("some-password")),
		})
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	let res = res.json::<prowovider_api::ApiResult<prowovider_api::User>>().await.unwrap();
	let out = match res {
		prowovider_api::ApiResult::Ok(ok) => ok,
		prowovider_api::ApiResult::Err(err) => panic!("API Error: {:?}", err),
	};

	assert_eq!(out, pool.get_user_by_user_id(ORG_1_UUID, out.id).await.unwrap().unwrap().into());
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_user_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let user1 = pool.new_user(ORG_1_UUID, "user-1", "whatever").await.unwrap();
	let _user2 = pool.new_user(ORG_1_UUID, "user-2", "whatever-2").await.unwrap();

	let admin = global_test_administrator();
	let path = format!("/api/v1/organisations/{}/users/{}", ORG_1_UUID, user1.id);
	get::<User>(
		path.as_str(),
		port,
		Some(basic_auth(admin.username.as_str(), admin.password.as_str())),
		StatusCode::OK,
		user1.into(),
	)
	.await;
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn change_user_password() {
	const USERNAME: &str = "user-1";
	const STARTING_PASSWORD: &str = "somepassword";
	const NEW_PASSWORD: &str = "somenewpassword";

	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let user1 = pool.new_user(ORG_1_UUID, USERNAME, STARTING_PASSWORD).await.unwrap();
	assert!(pool
		.verify_credentials_and_fetch_user(ORG_1_UUID, USERNAME, STARTING_PASSWORD)
		.await
		.unwrap()
		.is_some());

	let admin = global_test_administrator();
	let api_path = format!("/api/v1/organisations/{}/users/{}", ORG_1_UUID, user1.id);

	let res = reqwest::Client::default()
		.put(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.json(&UserPassword { username: None, password: Some(Cow::Borrowed(NEW_PASSWORD)) })
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	assert!(pool
		.verify_credentials_and_fetch_user(ORG_1_UUID, USERNAME, NEW_PASSWORD)
		.await
		.unwrap()
		.is_some());
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn change_user_username() {
	const STARTING_USERNAME: &str = "user-1";
	const NEW_USERNAME: &str = "user-2";
	const PASSWORD: &str = "somepassword";

	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let user1 = pool.new_user(ORG_1_UUID, STARTING_USERNAME, PASSWORD).await.unwrap();
	assert!(pool
		.verify_credentials_and_fetch_user(ORG_1_UUID, STARTING_USERNAME, PASSWORD)
		.await
		.unwrap()
		.is_some());

	let admin = global_test_administrator();
	let api_path = format!("/api/v1/organisations/{}/users/{}", ORG_1_UUID, user1.id);

	let res = reqwest::Client::default()
		.put(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.json(&UserPassword { username: Some(Cow::Borrowed(NEW_USERNAME)), password: None })
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	assert!(pool
		.verify_credentials_and_fetch_user(ORG_1_UUID, NEW_USERNAME, PASSWORD)
		.await
		.unwrap()
		.is_some());
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn change_user_username_password() {
	const STARTING_USERNAME: &str = "user-1";
	const NEW_USERNAME: &str = "user-2";
	const STARTING_PASSWORD: &str = "somepassword";
	const NEW_PASSWORD: &str = "somenewpassword";

	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let user1 = pool.new_user(ORG_1_UUID, STARTING_USERNAME, STARTING_PASSWORD).await.unwrap();
	assert!(pool
		.verify_credentials_and_fetch_user(ORG_1_UUID, STARTING_USERNAME, STARTING_PASSWORD)
		.await
		.unwrap()
		.is_some());

	let admin = global_test_administrator();
	let api_path = format!("/api/v1/organisations/{}/users/{}", ORG_1_UUID, user1.id);

	let res = reqwest::Client::default()
		.put(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.json(&UserPassword {
			username: Some(Cow::Borrowed(NEW_USERNAME)),
			password: Some(Cow::Borrowed(NEW_PASSWORD)),
		})
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	assert!(pool
		.verify_credentials_and_fetch_user(ORG_1_UUID, NEW_USERNAME, NEW_PASSWORD)
		.await
		.unwrap()
		.is_some());
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn delete_user() {
	const USERNAME: &str = "user-1";
	const PASSWORD: &str = "somepassword";

	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let user1 = pool.new_user(ORG_1_UUID, USERNAME, PASSWORD).await.unwrap();
	assert!(pool
		.verify_credentials_and_fetch_user(ORG_1_UUID, USERNAME, PASSWORD)
		.await
		.unwrap()
		.is_some());

	let admin = global_test_administrator();
	let api_path = format!("/api/v1/organisations/{}/users/{}", ORG_1_UUID, user1.id);

	let res = reqwest::Client::default()
		.delete(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	assert!(!pool.user_exists_by_id(ORG_1_UUID, user1.id).await.unwrap())
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn resolve_username() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let user1 = pool.new_user(ORG_1_UUID, "user-1", "whatever").await.unwrap();
	let _user2 = pool.new_user(ORG_1_UUID, "user-2", "whatever-2").await.unwrap();

	let admin = global_test_administrator();
	let path = format!("/api/v1/organisations/{}/users/resolve/{}", ORG_1_UUID, user1.username);
	get::<Uuid>(
		path.as_str(),
		port,
		Some(basic_auth(admin.username.as_str(), admin.password.as_str())),
		StatusCode::OK,
		user1.id,
	)
	.await;
}

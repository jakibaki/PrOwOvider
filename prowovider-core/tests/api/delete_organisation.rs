/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use hyper::StatusCode;
use prowovider_core::models::OrganisationModel;

use crate::api::{global_test_administrator, ORG_1_UUID};

use super::{read_settings, spawn_server};

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn delete_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let _org2 = pool.create_organisation("org-2", None).await.unwrap();

	let admin = global_test_administrator();

	let path = format!("/api/v1/organisations/{}", ORG_1_UUID);
	let res = reqwest::Client::default()
		.delete(super::path(path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	assert!(!pool.organisation_exists_by_id(ORG_1_UUID).await.unwrap());
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn delete_non_existent_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let admin = super::global_test_administrator();

	let path = format!("/api/v1/organisations/{}", ORG_1_UUID);
	let res = reqwest::Client::default()
		.delete(super::path(path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::NOT_FOUND);
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn delete_organisation_scoped_administrator() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let _org2 = pool.create_organisation("org-2", None).await.unwrap();

	let admin = super::org1_test_administrator();

	let path = format!("/api/v1/organisations/{}", ORG_1_UUID);
	let res = reqwest::Client::default()
		.delete(super::path(path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::UNAUTHORIZED);

	assert!(pool.organisation_exists_by_id(ORG_1_UUID).await.unwrap());
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn delete_organisation_wrong_scoped_administrator() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let org2 = pool.create_organisation("org-2", None).await.unwrap();

	let admin = super::org1_test_administrator();

	let path = format!("/api/v1/organisations/{}", org2.id);
	let res = reqwest::Client::default()
		.delete(super::path(path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::UNAUTHORIZED);

	assert!(pool.organisation_exists_by_id(ORG_1_UUID).await.unwrap());
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn delete_organisation_missing_auth() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let org2 = pool.create_organisation("org-2", None).await.unwrap();

	let path = format!("/api/v1/organisations/{}", org2.id);
	let res =
		reqwest::Client::default().delete(super::path(path.as_str(), port)).send().await.unwrap();
	assert_eq!(res.status(), StatusCode::UNAUTHORIZED);

	assert!(pool.organisation_exists_by_id(ORG_1_UUID).await.unwrap());
}

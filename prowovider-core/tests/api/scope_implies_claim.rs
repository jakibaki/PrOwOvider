/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use hyper::StatusCode;
use prowovider_core::models::{
	OrganisationClaimModel, OrganisationModel, OrganisationScopeClaimMapModel,
	OrganisationScopeModel,
};

use crate::api::{basic_auth, read_settings, spawn_server, ORG_1_UUID};

use super::{get, global_test_administrator};

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_implied_claims_by_scope_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let sc1 = pool.new_scope(ORG_1_UUID, "scope-1").await.unwrap();
	let c1 = pool.new_claim(ORG_1_UUID, "claim-1", None).await.unwrap();
	pool.create_scope_claim_mapping(ORG_1_UUID, sc1.scope.as_str(), c1.claim_name.as_str())
		.await
		.unwrap();

	let admin = global_test_administrator();
	let path = format!("/api/v1/organisations/{}/scopes/{}/claims", ORG_1_UUID, sc1.scope);
	get(
		path.as_str(),
		port,
		Some(basic_auth(admin.username.as_str(), admin.password.as_str())),
		StatusCode::OK,
		vec![c1.claim_name],
	)
	.await;
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn post_implied_claims_by_scope_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let sc1 = pool.new_scope(ORG_1_UUID, "scope-1").await.unwrap();
	let c1 = pool.new_claim(ORG_1_UUID, "claim-1", None).await.unwrap();

	let admin = global_test_administrator();
	let path = format!(
		"/api/v1/organisations/{}/scopes/{}/claims/{}",
		ORG_1_UUID, sc1.scope, c1.claim_name
	);
	let res = reqwest::Client::default()
		.post(super::path(path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	assert!(pool
		.scope_claim_mapping_exists(ORG_1_UUID, sc1.scope.as_str(), c1.claim_name.as_str())
		.await
		.unwrap());
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn delete_implied_claims_by_scope_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let sc1 = pool.new_scope(ORG_1_UUID, "scope-1").await.unwrap();
	let c1 = pool.new_claim(ORG_1_UUID, "claim-1", None).await.unwrap();
	pool.create_scope_claim_mapping(ORG_1_UUID, sc1.scope.as_str(), c1.claim_name.as_str())
		.await
		.unwrap();

	let admin = global_test_administrator();
	let path = format!(
		"/api/v1/organisations/{}/scopes/{}/claims/{}",
		ORG_1_UUID, sc1.scope, c1.claim_name
	);
	let res = reqwest::Client::default()
		.delete(super::path(path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	assert!(!pool
		.scope_claim_mapping_exists(ORG_1_UUID, sc1.scope.as_str(), c1.claim_name.as_str())
		.await
		.unwrap());
}

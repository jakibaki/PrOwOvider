/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use std::borrow::Cow;

use hyper::StatusCode;
use prowovider_core::models::OrganisationModel;

use super::{
	basic_auth, get, global_test_administrator, org1_test_administrator, read_settings,
	spawn_server, ORG_1_UUID,
};

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let _org2 = pool.create_organisation("org-2", None).await.unwrap();

	let admin = global_test_administrator();
	let path = format!("/api/v1/organisations/{}", ORG_1_UUID);
	get::<prowovider_api::Organisation>(
		path.as_str(),
		port,
		Some(basic_auth(admin.username.as_str(), admin.password.as_str())),
		StatusCode::OK,
		org1.into(),
	)
	.await;
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_non_existent_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let admin = super::global_test_administrator();

	let path = format!("/api/v1/organisations/{}", ORG_1_UUID);
	get(
		path.as_str(),
		port,
		Some(basic_auth(admin.username.as_str(), admin.password.as_str())),
		StatusCode::NOT_FOUND,
		prowovider_api::JsonError::new(Cow::Borrowed("ITEM_NOT_FOUND"), None),
	)
	.await;
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_organisation_with_limited_scope_administrator() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let _org2 = pool.create_organisation("org-2", None).await.unwrap();

	let admin = org1_test_administrator();
	let path = format!("/api/v1/organisations/{}", ORG_1_UUID);
	get::<prowovider_api::Organisation>(
		path.as_str(),
		port,
		Some(basic_auth(admin.username.as_str(), admin.password.as_str())),
		StatusCode::OK,
		org1.into(),
	)
	.await;
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_organisation_with_bad_limited_scope_administrator() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let org2 = pool.create_organisation("org-2", None).await.unwrap();

	let admin = org1_test_administrator();
	let path = format!("/api/v1/organisations/{}", org2.id);
	get(
		path.as_str(),
		port,
		Some(basic_auth(admin.username.as_str(), admin.password.as_str())),
		StatusCode::UNAUTHORIZED,
		prowovider_api::JsonError::new(Cow::Borrowed("INVALID_AUTH"), None),
	)
	.await;
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_organisation_unauthed() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let _org2 = pool.create_organisation("org-2", None).await.unwrap();

	let path = format!("/api/v1/organisations/{}", ORG_1_UUID);
	get(
		path.as_str(),
		port,
		None,
		StatusCode::UNAUTHORIZED,
		prowovider_api::JsonError::new(Cow::Borrowed("INVALID_AUTH"), None),
	)
	.await;
}

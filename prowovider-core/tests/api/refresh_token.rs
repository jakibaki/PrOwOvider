use std::borrow::Cow;

use hyper::StatusCode;
use openid_types::endpoints::{
	requests::{
		AuthenticationRequest, CodeCallback, CodeTokenRequest, CodeTokenRequestExtras,
		RefreshTokenRequest, RefreshTokenRequestExtras, ResponseType,
	},
	responses::TokenResponse,
};
use prowovider_core::{
	api::tokens::{LoginState, Token},
	models::{
		ClientRedirectUriModel, ClientType, OrganisationClientModel, OrganisationModel,
		OrganisationUserModel,
	},
};
use uuid::Uuid;

use crate::api::{path, read_settings, spawn_server, ORG_1_UUID};

pub const REDIRECT_URI: &str = "https://some_place.example";
pub const STATE: &str = "5HuLH8fD";
pub const NONCE: &str = "4aEad3Wo";

fn authentication_request(client_id: Uuid) -> AuthenticationRequest {
	AuthenticationRequest {
		scope: vec![String::from("openid")],
		response_type: vec![ResponseType::Code],
		client_id: client_id.to_string(),
		redirect_uri: String::from(REDIRECT_URI),
		state: Some(String::from(STATE)),
		response_mode: vec![],
		nonce: Some(String::from(NONCE)),
		display: None,
		prompt: vec![],
		max_age: None,
		ui_locales: vec![],
		id_token_hint: None,
		login_hint: None,
		acr_values: vec![],
	}
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn trade_refresh_token() {
	let settings = read_settings();
	let oidc_settings = settings.openid_connect.get(&ORG_1_UUID).unwrap().clone();
	let port = spawn_server(settings, pool.clone()).await;
	let settings = oidc_settings;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();

	pool.new_user(ORG_1_UUID, "test", "test").await.unwrap();
	let client =
		pool.new_client(ORG_1_UUID, "test-client", ClientType::Confidential, None).await.unwrap();
	pool.create_redirect_uri(client.id, REDIRECT_URI).await.unwrap();

	let key = settings.jwt.by_optional_id(None).unwrap();

	let authentication_request = authentication_request(client.id);
	let login_state = Token::LoginState(Cow::Owned(LoginState::new(
		vec![String::from("openid")],
		authentication_request,
		ORG_1_UUID,
		&settings,
	)))
	.encode(key)
	.unwrap();

	let login_resp = reqwest::Client::builder()
		.redirect(reqwest::redirect::Policy::none())
		.build()
		.unwrap()
		.post(path(format!("/api/v1/openid/{}/login", ORG_1_UUID).as_str(), port))
		.form(&prowovider_api::LoginRequest {
			login_state,
			username: String::from("test"),
			password: String::from("test"),
		})
		.send()
		.await
		.unwrap();
	assert_eq!(
		login_resp.status(),
		StatusCode::FOUND,
		"JSON: {}",
		serde_json::to_string_pretty(&login_resp.json::<serde_json::Value>().await.unwrap())
			.unwrap()
	);

	let redirect_uri = url::Url::parse(
		login_resp.headers().get(hyper::header::LOCATION).unwrap().to_str().unwrap(),
	)
	.unwrap();
	let parsed_query: CodeCallback =
		serde_urlencoded::from_str(redirect_uri.query().unwrap()).unwrap();
	assert_eq!(parsed_query.state.as_deref(), Some(STATE));

	let resp: TokenResponse = reqwest::Client::default()
		.post(path(format!("/api/v1/openid/{}/token", ORG_1_UUID).as_str(), port))
		.form(&CodeTokenRequest {
			grant_type: openid_types::endpoints::requests::GrantType::AuthorizationCode,
			extras: CodeTokenRequestExtras {
				code: parsed_query.code,
				redirect_uri: Some(String::from(REDIRECT_URI)),
				client_id: client.id.to_string(),
				client_secret: Some(client.secret.clone()),
			},
		})
		.send()
		.await
		.unwrap()
		.json()
		.await
		.unwrap();

	let refresh_token = resp.refresh_token.unwrap();

	let _resp: TokenResponse = reqwest::Client::default()
		.post(path(format!("/api/v1/openid/{}/token", ORG_1_UUID).as_str(), port))
		.form(&RefreshTokenRequest {
			grant_type: openid_types::endpoints::requests::GrantType::RefreshToken,
			extras: RefreshTokenRequestExtras {
				refresh_token,
				scope: vec![String::from("openid")],
				client_id: client.id.to_string(),
				client_secret: Some(client.secret),
			},
		})
		.send()
		.await
		.unwrap()
		.json()
		.await
		.unwrap();
}

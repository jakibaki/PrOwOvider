/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use hyper::StatusCode;
use prowovider_api::{ClaimParameters, ClaimType};
use prowovider_core::models::{OrganisationClaimModel, OrganisationModel, OrganisationUserModel};

use crate::api::{global_test_administrator, path, read_settings, spawn_server, ORG_1_UUID};

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn put_typed_claim_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();

	let admin = global_test_administrator();
	let api_path = format!("/api/v1/organisations/{}/claims/{}", ORG_1_UUID, "claim-1");
	let res = reqwest::Client::default()
		.put(super::path(api_path.as_str(), port))
		.json(&ClaimParameters { r#type: Some(ClaimType::String) })
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	assert_eq!(
		pool.get_claim_type_by_organisation_id_and_claim_name(ORG_1_UUID, "claim-1")
			.await
			.unwrap()
			.unwrap(),
		Some(prowovider_core::models::ClaimType::String)
	);

	let user1 = pool.new_user(ORG_1_UUID, "user-1", "whatever").await.unwrap();

	let admin = global_test_administrator();
	let api_path =
		format!("/api/v1/organisations/{}/users/{}/claims/claim-1", ORG_1_UUID, user1.id);
	let res = reqwest::Client::default()
		.put(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.json(&serde_json::json!("some-val"))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn put_bad_typed_claim_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();

	let admin = global_test_administrator();
	let api_path = format!("/api/v1/organisations/{}/claims/{}", ORG_1_UUID, "claim-1");
	let res = reqwest::Client::default()
		.put(super::path(api_path.as_str(), port))
		.json(&ClaimParameters { r#type: Some(ClaimType::String) })
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	assert_eq!(
		pool.get_claim_type_by_organisation_id_and_claim_name(ORG_1_UUID, "claim-1")
			.await
			.unwrap()
			.unwrap(),
		Some(prowovider_core::models::ClaimType::String)
	);

	let user1 = pool.new_user(ORG_1_UUID, "user-1", "whatever").await.unwrap();

	let admin = global_test_administrator();
	let api_path =
		format!("/api/v1/organisations/{}/users/{}/claims/claim-1", ORG_1_UUID, user1.id);
	let res = reqwest::Client::default()
		.put(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.json(&serde_json::json!({}))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::BAD_REQUEST);
}

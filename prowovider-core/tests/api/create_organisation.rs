/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use std::borrow::Cow;

use hyper::StatusCode;
use prowovider_core::models::OrganisationModel;

use crate::api::{
	global_test_administrator, org1_test_administrator, path, read_settings, spawn_server,
	ORG_1_UUID,
};

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn create_new_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool).await;

	let query = prowovider_api::OrganisationNameQuery { name: Cow::Borrowed("new-org") };
	let admin = global_test_administrator();

	let res = reqwest::Client::default()
		.post(path("/api/v1/organisations", port))
		.basic_auth(admin.username, Some(admin.password))
		.query(&query)
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	let res = res.json::<prowovider_api::ApiResult<prowovider_api::Organisation>>().await.unwrap();
	let out = match res {
		prowovider_api::ApiResult::Ok(ok) => ok,
		prowovider_api::ApiResult::Err(err) => panic!("API Error: {:?}", err),
	};

	assert_eq!(out.name, query.name);
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn create_new_conflicting_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let _org2 = pool.create_organisation("org-2", None).await.unwrap();

	let query = prowovider_api::OrganisationNameQuery { name: Cow::Borrowed(org1.name.as_str()) };
	let admin = global_test_administrator();

	let res = reqwest::Client::default()
		.post(path("/api/v1/organisations", port))
		.basic_auth(admin.username, Some(admin.password))
		.query(&query)
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::CONFLICT);

	let res = res.json::<prowovider_api::ApiResult<prowovider_api::Organisation>>().await.unwrap();
	let out = match res {
		prowovider_api::ApiResult::Ok(_) => panic!("Expected API Error"),
		prowovider_api::ApiResult::Err(err) => err,
	};

	assert_eq!(out.error_code.as_ref(), "ITEM_ALREADY_EXISTS");
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn create_new_organisation_as_scoped_administrator() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let _org2 = pool.create_organisation("org-2", None).await.unwrap();

	let query = prowovider_api::OrganisationNameQuery { name: Cow::Borrowed("new-org") };
	let admin = org1_test_administrator();

	let res = reqwest::Client::default()
		.post(path("/api/v1/organisations", port))
		.basic_auth(admin.username, Some(admin.password))
		.query(&query)
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::UNAUTHORIZED);

	let res = res.json::<prowovider_api::ApiResult<prowovider_api::Organisation>>().await.unwrap();
	let out = match res {
		prowovider_api::ApiResult::Ok(_) => panic!("Expected API Error"),
		prowovider_api::ApiResult::Err(err) => err,
	};

	assert_eq!(out.error_code.as_ref(), "INVALID_AUTH");
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn create_new_organisation_no_auth() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let _org2 = pool.create_organisation("org-2", None).await.unwrap();

	let query = prowovider_api::OrganisationNameQuery { name: Cow::Borrowed("new-org") };

	let res = reqwest::Client::default()
		.post(path("/api/v1/organisations", port))
		.query(&query)
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::UNAUTHORIZED);

	let res = res.json::<prowovider_api::ApiResult<prowovider_api::Organisation>>().await.unwrap();
	let out = match res {
		prowovider_api::ApiResult::Ok(_) => panic!("Expected API Error"),
		prowovider_api::ApiResult::Err(err) => err,
	};

	assert_eq!(out.error_code.as_ref(), "INVALID_AUTH");
}

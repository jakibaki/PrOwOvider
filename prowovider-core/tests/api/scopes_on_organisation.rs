/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use std::borrow::Cow;

use hyper::StatusCode;
use prowovider_core::models::{OrganisationModel, OrganisationScopeModel};

use super::{
	basic_auth, get, get_unsorted, global_test_administrator, org1_test_administrator,
	read_settings, spawn_server, ORG_1_UUID,
};

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_scopes_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let sc1 = pool.new_scope(ORG_1_UUID, "scope-1").await.unwrap();
	let sc2 = pool.new_scope(ORG_1_UUID, "scope-2").await.unwrap();

	let admin = global_test_administrator();
	let path = format!("/api/v1/organisations/{}/scopes", ORG_1_UUID);
	get_unsorted::<String>(
		path.as_str(),
		port,
		Some(basic_auth(admin.username.as_str(), admin.password.as_str())),
		vec![sc1.scope, sc2.scope],
	)
	.await;
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_scopes_none_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let org2 = pool.create_organisation("org-2", None).await.unwrap();
	let _sc1 = pool.new_scope(ORG_1_UUID, "scope-1").await.unwrap();
	let _sc2 = pool.new_scope(ORG_1_UUID, "scope-2").await.unwrap();

	let admin = global_test_administrator();
	let path = format!("/api/v1/organisations/{}/scopes", org2.id);
	get_unsorted::<String>(
		path.as_str(),
		port,
		Some(basic_auth(admin.username.as_str(), admin.password.as_str())),
		vec![],
	)
	.await;
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_scopes_as_scoped_administrator() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let sc1 = pool.new_scope(ORG_1_UUID, "scope-1").await.unwrap();
	let sc2 = pool.new_scope(ORG_1_UUID, "scope-2").await.unwrap();

	let admin = org1_test_administrator();
	let path = format!("/api/v1/organisations/{}/scopes", ORG_1_UUID);
	get_unsorted::<String>(
		path.as_str(),
		port,
		Some(basic_auth(admin.username.as_str(), admin.password.as_str())),
		vec![sc1.scope, sc2.scope],
	)
	.await;
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_scopes_as_wrongly_scoped_administrator() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let org2 = pool.create_organisation("org-2", None).await.unwrap();
	let _sc1 = pool.new_scope(org2.id, "scope-1").await.unwrap();
	let _sc2 = pool.new_scope(org2.id, "scope-2").await.unwrap();

	let admin = org1_test_administrator();
	let path = format!("/api/v1/organisations/{}/scopes", org2.id);
	get(
		path.as_str(),
		port,
		Some(basic_auth(admin.username.as_str(), admin.password.as_str())),
		StatusCode::UNAUTHORIZED,
		prowovider_api::JsonError::new(Cow::Borrowed("INVALID_AUTH"), None),
	)
	.await;
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_scopes_unauthed() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let _org2 = pool.create_organisation("org-2", None).await.unwrap();
	let _sc1 = pool.new_scope(ORG_1_UUID, "scope-1").await.unwrap();
	let _sc2 = pool.new_scope(ORG_1_UUID, "scope-2").await.unwrap();

	let path = format!("/api/v1/organisations/{}", ORG_1_UUID);
	get(
		path.as_str(),
		port,
		None,
		StatusCode::UNAUTHORIZED,
		prowovider_api::JsonError::new(Cow::Borrowed("INVALID_AUTH"), None),
	)
	.await;
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_scope_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let sc1 = pool.new_scope(ORG_1_UUID, "scope-1").await.unwrap();

	let admin = global_test_administrator();
	let path = format!("/api/v1/organisations/{}/scopes/{}", ORG_1_UUID, sc1.scope);
	let res = reqwest::Client::default()
		.get(super::path(path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_scope_on_organisation_missing() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let _sc1 = pool.new_scope(ORG_1_UUID, "scope-1").await.unwrap();

	let admin = global_test_administrator();
	let path = format!("/api/v1/organisations/{}/scopes/some-non-scope", ORG_1_UUID);
	let res = reqwest::Client::default()
		.get(super::path(path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::NOT_FOUND);
	assert_eq!(
		res.json::<prowovider_api::JsonError>().await.unwrap(),
		prowovider_api::JsonError::new(Cow::Borrowed("ITEM_NOT_FOUND"), None)
	);
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_scope_on_organisation_unauthed() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let sc1 = pool.new_scope(ORG_1_UUID, "scope-1").await.unwrap();

	let path = format!("/api/v1/organisations/{}/scopes/{}", ORG_1_UUID, sc1.scope);
	let res =
		reqwest::Client::default().get(super::path(path.as_str(), port)).send().await.unwrap();
	assert_eq!(res.status(), StatusCode::UNAUTHORIZED);
	assert_eq!(
		res.json::<prowovider_api::JsonError>().await.unwrap(),
		prowovider_api::JsonError::new(Cow::Borrowed("INVALID_AUTH"), None)
	);
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_scope_on_organisation_as_scoped_administrator() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let sc1 = pool.new_scope(ORG_1_UUID, "scope-1").await.unwrap();

	let admin = org1_test_administrator();
	let path = format!("/api/v1/organisations/{}/scopes/{}", ORG_1_UUID, sc1.scope);
	let res = reqwest::Client::default()
		.get(super::path(path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_scope_on_organisation_as_bad_scoped_administrator() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let org2 = pool.create_organisation("org-2", None).await.unwrap();
	let sc1 = pool.new_scope(org2.id, "scope-1").await.unwrap();

	let admin = org1_test_administrator();
	let path = format!("/api/v1/organisations/{}/scopes/{}", org2.id, sc1.scope);
	let res = reqwest::Client::default()
		.get(super::path(path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::UNAUTHORIZED);
	assert_eq!(
		res.json::<prowovider_api::JsonError>().await.unwrap(),
		prowovider_api::JsonError::new(Cow::Borrowed("INVALID_AUTH"), None)
	);
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn put_scope_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();

	let admin = global_test_administrator();
	let path = format!("/api/v1/organisations/{}/scopes/{}", ORG_1_UUID, "scope-1");
	let res = reqwest::Client::default()
		.put(super::path(path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	assert!(pool.scope_exists(ORG_1_UUID, "scope-1").await.unwrap());
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn put_scope_on_organisation_as_scoped_administrator() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();

	let admin = org1_test_administrator();
	let path = format!("/api/v1/organisations/{}/scopes/{}", ORG_1_UUID, "scope-1");
	let res = reqwest::Client::default()
		.put(super::path(path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	assert!(pool.scope_exists(ORG_1_UUID, "scope-1").await.unwrap());
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn put_scope_on_organisation_as_bad_scoped_administrator() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let org2 = pool.create_organisation("org-2", None).await.unwrap();

	let admin = org1_test_administrator();
	let path = format!("/api/v1/organisations/{}/scopes/{}", org2.id, "scope-1");
	let res = reqwest::Client::default()
		.put(super::path(path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::UNAUTHORIZED);
	assert_eq!(
		res.json::<prowovider_api::JsonError>().await.unwrap(),
		prowovider_api::JsonError::new(Cow::Borrowed("INVALID_AUTH"), None)
	);

	assert!(!pool.scope_exists(org2.id, "scope-1").await.unwrap());
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn put_scope_on_organisation_unauthed() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let org2 = pool.create_organisation("org-2", None).await.unwrap();

	let path = format!("/api/v1/organisations/{}/scopes/{}", org2.id, "scope-1");
	let res =
		reqwest::Client::default().put(super::path(path.as_str(), port)).send().await.unwrap();
	assert_eq!(res.status(), StatusCode::UNAUTHORIZED);
	assert_eq!(
		res.json::<prowovider_api::JsonError>().await.unwrap(),
		prowovider_api::JsonError::new(Cow::Borrowed("INVALID_AUTH"), None)
	);

	assert!(!pool.scope_exists(org2.id, "scope-1").await.unwrap());
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn put_scope_on_organisation_conflicting() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let _sc1 = pool.new_scope(ORG_1_UUID, "scope-1").await.unwrap();

	let admin = global_test_administrator();
	let path = format!("/api/v1/organisations/{}/scopes/{}", ORG_1_UUID, "scope-1");
	let res = reqwest::Client::default()
		.put(super::path(path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::CONFLICT);
	assert_eq!(
		res.json::<prowovider_api::JsonError>().await.unwrap(),
		prowovider_api::JsonError::new(Cow::Borrowed("ITEM_ALREADY_EXISTS"), None)
	);

	assert!(pool.scope_exists(ORG_1_UUID, "scope-1").await.unwrap());
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn put_scope_on_organisation_bad_chars() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();

	let admin = global_test_administrator();
	let path = format!("/api/v1/organisations/{}/scopes/{}", ORG_1_UUID, "O.O");
	let res = reqwest::Client::default()
		.put(super::path(path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::BAD_REQUEST);
	assert_eq!(
		res.json::<prowovider_api::JsonError>().await.unwrap().error_code.as_ref(),
		prowovider_core::api::error::WrappedApiError::PARAMAETER_CONTAINS_BAD_CHARACTER
	);

	assert!(!pool.scope_exists(ORG_1_UUID, "O.O").await.unwrap());
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn delete_scope_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let _sc1 = pool.new_scope(ORG_1_UUID, "scope-1").await.unwrap();

	let admin = global_test_administrator();
	let path = format!("/api/v1/organisations/{}/scopes/{}", ORG_1_UUID, "scope-1");
	let res = reqwest::Client::default()
		.delete(super::path(path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	assert!(!pool.scope_exists(ORG_1_UUID, "scope-1").await.unwrap());
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn delete_scope_on_organisation_as_scoped_administrator() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let _sc1 = pool.new_scope(ORG_1_UUID, "scope-1").await.unwrap();

	let admin = org1_test_administrator();
	let path = format!("/api/v1/organisations/{}/scopes/{}", ORG_1_UUID, "scope-1");
	let res = reqwest::Client::default()
		.delete(super::path(path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	assert!(!pool.scope_exists(ORG_1_UUID, "scope-1").await.unwrap());
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn delete_scope_on_organisation_as_bad_scoped_administrator() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let org2 = pool.create_organisation("org-2", None).await.unwrap();
	let _sc1 = pool.new_scope(org2.id, "scope-1").await.unwrap();

	let admin = org1_test_administrator();
	let path = format!("/api/v1/organisations/{}/scopes/{}", org2.id, "scope-1");
	let res = reqwest::Client::default()
		.delete(super::path(path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::UNAUTHORIZED);
	assert_eq!(
		res.json::<prowovider_api::JsonError>().await.unwrap(),
		prowovider_api::JsonError::new(Cow::Borrowed("INVALID_AUTH"), None)
	);

	assert!(pool.scope_exists(org2.id, "scope-1").await.unwrap());
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn delete_scope_on_organisation_unauthed() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let org2 = pool.create_organisation("org-2", None).await.unwrap();
	let _sc1 = pool.new_scope(org2.id, "scope-1").await.unwrap();

	let path = format!("/api/v1/organisations/{}/scopes/{}", org2.id, "scope-1");
	let res =
		reqwest::Client::default().delete(super::path(path.as_str(), port)).send().await.unwrap();
	assert_eq!(res.status(), StatusCode::UNAUTHORIZED);
	assert_eq!(
		res.json::<prowovider_api::JsonError>().await.unwrap(),
		prowovider_api::JsonError::new(Cow::Borrowed("INVALID_AUTH"), None)
	);

	assert!(pool.scope_exists(org2.id, "scope-1").await.unwrap());
}

/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use std::borrow::Cow;

use hyper::StatusCode;
use prowovider_api::{
	ClientList, ClientRegistrationParameters, PreferredKeyUpdate, RedirectUriQuery,
};
use prowovider_core::models::{
	ClientRedirectUriModel, ClientType, OrganisationClientModel, OrganisationModel,
};

use super::{
	basic_auth, get_unsorted, global_test_administrator, org1_test_administrator, path,
	read_settings, spawn_server, ORG_1_UUID,
};

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_all_clients_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let client1 =
		pool.new_client(ORG_1_UUID, "client-1", ClientType::Confidential, None).await.unwrap();
	let client2 =
		pool.new_client(ORG_1_UUID, "client-1", ClientType::Confidential, None).await.unwrap();

	let admin = global_test_administrator();
	let api_path = format!("/api/v1/organisations/{}/clients", ORG_1_UUID);
	let res = reqwest::Client::default()
		.get(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	let res: ClientList = res.json().await.unwrap();
	assert!(res.confidential.contains(&client1.id));
	assert!(res.confidential.contains(&client2.id));
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_all_clients_on_non_existant_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let admin = global_test_administrator();
	let api_path = format!("/api/v1/organisations/{}/clients", ORG_1_UUID);
	let res = reqwest::Client::default()
		.get(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::NOT_FOUND);

	let err = res.json::<prowovider_api::JsonError<'static, 'static>>().await.unwrap();
	assert_eq!(err.error_code.as_ref(), "ITEM_NOT_FOUND");
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_all_clients_on_organisation_as_scoped_administrator() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let client1 =
		pool.new_client(ORG_1_UUID, "client-1", ClientType::Confidential, None).await.unwrap();
	let client2 =
		pool.new_client(ORG_1_UUID, "client-2", ClientType::Confidential, None).await.unwrap();

	let admin = org1_test_administrator();
	let api_path = format!("/api/v1/organisations/{}/clients", ORG_1_UUID);
	let res = reqwest::Client::default()
		.get(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	let res: ClientList = res.json().await.unwrap();
	assert!(res.confidential.contains(&client1.id));
	assert!(res.confidential.contains(&client2.id));
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_all_clients_on_organisation_as_bad_scoped_administrator() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let org2 = pool.create_organisation("org-2", None).await.unwrap();
	let _client1 =
		pool.new_client(org2.id, "client-1", ClientType::Confidential, None).await.unwrap();
	let _client2 =
		pool.new_client(org2.id, "client-2", ClientType::Confidential, None).await.unwrap();

	let admin = org1_test_administrator();
	let api_path = format!("/api/v1/organisations/{}/clients", org2.id);
	let res = reqwest::Client::default()
		.get(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::UNAUTHORIZED);

	let err = res.json::<prowovider_api::JsonError<'static, 'static>>().await.unwrap();
	assert_eq!(err.error_code.as_ref(), "INVALID_AUTH");
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_all_clients_on_organisation_no_auth() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let org2 = pool.create_organisation("org-2", None).await.unwrap();
	let _client1 =
		pool.new_client(org2.id, "client-1", ClientType::Confidential, None).await.unwrap();
	let _client2 =
		pool.new_client(org2.id, "client-2", ClientType::Confidential, None).await.unwrap();

	let api_path = format!("/api/v1/organisations/{}/clients", org2.id);
	let res = reqwest::Client::default().get(path(api_path.as_str(), port)).send().await.unwrap();
	assert_eq!(res.status(), StatusCode::UNAUTHORIZED);

	let err = res.json::<prowovider_api::JsonError<'static, 'static>>().await.unwrap();
	assert_eq!(err.error_code.as_ref(), "INVALID_AUTH");
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn post_client_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();

	let admin = global_test_administrator();
	let api_path = format!("/api/v1/organisations/{}/clients", ORG_1_UUID);
	let res = reqwest::Client::default()
		.post(path(api_path.as_str(), port))
		.json(&ClientRegistrationParameters {
			public: false,
			name: String::from("client-1"),
			preferred_key: None,
		})
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	let json_res: prowovider_api::RegisteredClient = res.json().await.unwrap();
	let res = pool.get_clients_by_organisation_id(ORG_1_UUID).await.unwrap();
	assert_eq!(res.len(), 1);
	assert_eq!(res[0].id, json_res.id);
	assert_eq!(res[0].secret, json_res.secret.unwrap());
	assert_eq!(res[0].name.as_str(), "client-1");
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn post_client_on_organisation_as_scoped_administrator() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();

	let admin = org1_test_administrator();
	let api_path = format!("/api/v1/organisations/{}/clients", ORG_1_UUID);
	let res = reqwest::Client::default()
		.post(path(api_path.as_str(), port))
		.json(&ClientRegistrationParameters {
			public: false,
			name: String::from("client-1"),
			preferred_key: None,
		})
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	let json_res: prowovider_api::RegisteredClient = res.json().await.unwrap();
	let res = pool.get_clients_by_organisation_id(ORG_1_UUID).await.unwrap();
	assert_eq!(res.len(), 1);
	assert_eq!(res[0].id, json_res.id);
	assert_eq!(res[0].secret, json_res.secret.unwrap());
	assert_eq!(res[0].name.as_str(), "client-1");
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn post_client_on_organisation_as_bad_scoped_administrator() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let org2 = pool.create_organisation("org-2", None).await.unwrap();

	let admin = org1_test_administrator();
	let api_path = format!("/api/v1/organisations/{}/clients", org2.id);
	let res = reqwest::Client::default()
		.post(path(api_path.as_str(), port))
		.json(&ClientRegistrationParameters {
			public: false,
			name: String::from("client-1"),
			preferred_key: None,
		})
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::UNAUTHORIZED);

	let err = res.json::<prowovider_api::JsonError<'static, 'static>>().await.unwrap();
	assert_eq!(err.error_code.as_ref(), "INVALID_AUTH");
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn post_client_on_organisation_no_auth() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();

	let api_path = format!("/api/v1/organisations/{}/clients", ORG_1_UUID);
	let res = reqwest::Client::default()
		.post(path(api_path.as_str(), port))
		.json(&ClientRegistrationParameters {
			public: false,
			name: String::from("client-1"),
			preferred_key: None,
		})
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::UNAUTHORIZED);

	let err = res.json::<prowovider_api::JsonError<'static, 'static>>().await.unwrap();
	assert_eq!(err.error_code.as_ref(), "INVALID_AUTH");
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn post_client_on_non_existant_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let admin = global_test_administrator();
	let api_path = format!("/api/v1/organisations/{}/clients", ORG_1_UUID);
	let res = reqwest::Client::default()
		.post(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.json(&ClientRegistrationParameters {
			public: false,
			name: String::from("client-1"),
			preferred_key: None,
		})
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::NOT_FOUND);

	let err = res.json::<prowovider_api::JsonError<'static, 'static>>().await.unwrap();
	assert_eq!(err.error_code.as_ref(), "ITEM_NOT_FOUND");
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_client_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let client1 =
		pool.new_client(ORG_1_UUID, "client-1", ClientType::Confidential, None).await.unwrap();

	let admin = global_test_administrator();
	let api_path = format!("/api/v1/organisations/{}/clients/{}", ORG_1_UUID, client1.id);
	let res = reqwest::Client::default()
		.get(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);
	assert_eq!(res.text().await.unwrap().as_str(), "client-1");
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_client_on_organisation_as_scoped_administrator() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let client1 =
		pool.new_client(ORG_1_UUID, "client-1", ClientType::Confidential, None).await.unwrap();

	let admin = org1_test_administrator();
	let api_path = format!("/api/v1/organisations/{}/clients/{}", ORG_1_UUID, client1.id);
	let res = reqwest::Client::default()
		.get(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_client_on_organisation_as_bad_scoped_administrator() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let org2 = pool.create_organisation("org-2", None).await.unwrap();
	let client1 =
		pool.new_client(org2.id, "client-1", ClientType::Confidential, None).await.unwrap();

	let admin = org1_test_administrator();
	let api_path = format!("/api/v1/organisations/{}/clients/{}", org2.id, client1.id);
	let res = reqwest::Client::default()
		.get(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::UNAUTHORIZED);

	let err = res.json::<prowovider_api::JsonError<'static, 'static>>().await.unwrap();
	assert_eq!(err.error_code.as_ref(), "INVALID_AUTH");
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_non_existant_client_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();

	let admin = global_test_administrator();
	let api_path = format!("/api/v1/organisations/{}/clients/{}", ORG_1_UUID, uuid::Uuid::new_v4());
	let res = reqwest::Client::default()
		.get(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::NOT_FOUND);

	let err = res.json::<prowovider_api::JsonError<'static, 'static>>().await.unwrap();
	assert_eq!(err.error_code.as_ref(), "ITEM_NOT_FOUND");
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_non_existant_client_on_non_existant_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let admin = global_test_administrator();
	let api_path = format!("/api/v1/organisations/{}/clients/{}", ORG_1_UUID, uuid::Uuid::new_v4());
	let res = reqwest::Client::default()
		.get(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::NOT_FOUND);

	let err = res.json::<prowovider_api::JsonError<'static, 'static>>().await.unwrap();
	assert_eq!(err.error_code.as_ref(), "ITEM_NOT_FOUND");
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn delete_client_from_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let client1 =
		pool.new_client(ORG_1_UUID, "client-1", ClientType::Confidential, None).await.unwrap();

	let admin = global_test_administrator();
	let api_path = format!("/api/v1/organisations/{}/clients/{}", ORG_1_UUID, client1.id);
	let res = reqwest::Client::default()
		.delete(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	assert!(!pool.client_exists(client1.id, ORG_1_UUID).await.unwrap());
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn delete_client_from_organisation_as_scoped_administrator() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let client1 =
		pool.new_client(ORG_1_UUID, "client-1", ClientType::Confidential, None).await.unwrap();

	let admin = org1_test_administrator();
	let api_path = format!("/api/v1/organisations/{}/clients/{}", ORG_1_UUID, client1.id);
	let res = reqwest::Client::default()
		.delete(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	assert!(!pool.client_exists(client1.id, ORG_1_UUID).await.unwrap());
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn delete_client_from_organisation_as_bad_scoped_administrator() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let org2 = pool.create_organisation("org-2", None).await.unwrap();
	let client1 =
		pool.new_client(org2.id, "client-1", ClientType::Confidential, None).await.unwrap();

	let admin = org1_test_administrator();
	let api_path = format!("/api/v1/organisations/{}/clients/{}", org2.id, client1.id);
	let res = reqwest::Client::default()
		.delete(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::UNAUTHORIZED);

	let err = res.json::<prowovider_api::JsonError<'static, 'static>>().await.unwrap();
	assert_eq!(err.error_code.as_ref(), "INVALID_AUTH");

	assert!(pool.client_exists(client1.id, org2.id).await.unwrap());
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn delete_non_existing_client_from_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();

	let admin = global_test_administrator();
	let api_path = format!("/api/v1/organisations/{}/clients/{}", ORG_1_UUID, uuid::Uuid::new_v4());
	let res = reqwest::Client::default()
		.delete(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::NOT_FOUND);

	let err = res.json::<prowovider_api::JsonError<'static, 'static>>().await.unwrap();
	assert_eq!(err.error_code.as_ref(), "ITEM_NOT_FOUND");
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn delete_non_existant_client_from_non_existant_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let admin = global_test_administrator();
	let api_path = format!("/api/v1/organisations/{}/clients/{}", ORG_1_UUID, uuid::Uuid::new_v4());
	let res = reqwest::Client::default()
		.delete(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::NOT_FOUND);

	let err = res.json::<prowovider_api::JsonError<'static, 'static>>().await.unwrap();
	assert_eq!(err.error_code.as_ref(), "ITEM_NOT_FOUND");
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_redirect_urls_on_client_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let client1 =
		pool.new_client(ORG_1_UUID, "client-1", ClientType::Confidential, None).await.unwrap();
	pool.create_redirect_uri(client1.id, "https://owo.example").await.unwrap();
	pool.create_redirect_uri(client1.id, "https://uwu.example").await.unwrap();

	let admin = global_test_administrator();
	let api_path =
		format!("/api/v1/organisations/{}/clients/{}/redirect_uris", ORG_1_UUID, client1.id);
	get_unsorted::<String>(
		api_path.as_str(),
		port,
		Some(basic_auth(admin.username.as_str(), admin.password.as_str())),
		vec![String::from("https://owo.example"), String::from("https://uwu.example")],
	)
	.await;
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn put_redirect_url_on_client_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let client1 =
		pool.new_client(ORG_1_UUID, "client-1", ClientType::Confidential, None).await.unwrap();

	let admin = global_test_administrator();
	let api_path =
		format!("/api/v1/organisations/{}/clients/{}/redirect_uris", ORG_1_UUID, client1.id);
	let res = reqwest::Client::default()
		.put(path(api_path.as_str(), port))
		.query(&RedirectUriQuery { uri: Cow::Borrowed("https://owo.example") })
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	assert!(pool.redirect_uri_exists(client1.id, "https://owo.example").await.unwrap());
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn delete_redirect_url_on_client_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let client1 =
		pool.new_client(ORG_1_UUID, "client-1", ClientType::Confidential, None).await.unwrap();
	pool.create_redirect_uri(client1.id, "https://owo.example").await.unwrap();

	let admin = global_test_administrator();
	let api_path =
		format!("/api/v1/organisations/{}/clients/{}/redirect_uris", ORG_1_UUID, client1.id);
	let res = reqwest::Client::default()
		.delete(path(api_path.as_str(), port))
		.query(&RedirectUriQuery { uri: Cow::Borrowed("https://owo.example") })
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	assert!(!pool.redirect_uri_exists(client1.id, "https://owo.example/").await.unwrap());
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn get_preferred_key_on_client_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let client1 = pool
		.new_client(ORG_1_UUID, "client-1", ClientType::Confidential, Some("some-key"))
		.await
		.unwrap();

	let admin = global_test_administrator();
	let api_path =
		format!("/api/v1/organisations/{}/clients/{}/preferred_key", ORG_1_UUID, client1.id);

	let mut req = reqwest::Client::default().get(path(api_path.as_str(), port));
	req = req.header("Authorization", basic_auth(admin.username.as_str(), admin.password.as_str()));

	let res = req.send().await.unwrap();

	assert_eq!(res.status(), StatusCode::OK);
	assert_eq!(res.text().await.unwrap().as_str(), "some-key");
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn put_preferred_key_on_client_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let client1 =
		pool.new_client(ORG_1_UUID, "client-1", ClientType::Confidential, None).await.unwrap();

	let admin = global_test_administrator();
	let api_path =
		format!("/api/v1/organisations/{}/clients/{}/preferred_key", ORG_1_UUID, client1.id);
	let res = reqwest::Client::default()
		.put(path(api_path.as_str(), port))
		.query(&PreferredKeyUpdate { id: Cow::Borrowed("some-key") })
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	assert_eq!(
		pool.get_client_by_id(client1.id, ORG_1_UUID)
			.await
			.unwrap()
			.unwrap()
			.preferred_key
			.as_deref(),
		Some("some-key")
	);
}

#[sqlx_database_tester::test(pool(variable = "pool"))]
async fn delete_preferred_key_on_client_on_organisation() {
	let settings = read_settings();
	let port = spawn_server(settings, pool.clone()).await;

	let _org1 = pool.create_organisation("org-1", Some(ORG_1_UUID)).await.unwrap();
	let client1 = pool
		.new_client(ORG_1_UUID, "client-1", ClientType::Confidential, Some("some-key"))
		.await
		.unwrap();
	pool.create_redirect_uri(client1.id, "https://owo.example").await.unwrap();

	let admin = global_test_administrator();
	let api_path =
		format!("/api/v1/organisations/{}/clients/{}/preferred_key", ORG_1_UUID, client1.id);
	let res = reqwest::Client::default()
		.delete(path(api_path.as_str(), port))
		.basic_auth(admin.username, Some(admin.password))
		.send()
		.await
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	assert!(pool
		.get_client_by_id(client1.id, ORG_1_UUID)
		.await
		.unwrap()
		.unwrap()
		.preferred_key
		.is_none());
}

/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

pub mod claim_destinations;
pub mod claims_on_organisation;
pub mod clients;
pub mod create_organisation;
pub mod delete_organisation;
pub mod get_organisation;
pub mod get_organisations;
pub mod refresh_token;
pub mod rename_organisation;
pub mod scope_implies_claim;
pub mod scopes_on_organisation;
pub mod typed_claims;
pub mod users;

use std::{
	net::{Ipv4Addr, SocketAddr, SocketAddrV4},
	str::FromStr,
};

use axum::http::HeaderValue;
use hyper::{server::conn::AddrIncoming, StatusCode};
use once_cell::sync::OnceCell;
use prowovider_core::{
	models::Backend,
	settings::{
		jwk::{ExpiryConfig, KeyConfig, KeySet},
		Administrator, OpenIDConnectSettings, Settings,
	},
};
use serde::de::DeserializeOwned;
use sqlx::PgPool;
use uuid::Uuid;

const ORG_1_UUID: Uuid = Uuid::from_u128(0x00112233445566778899AABBCCDDEEFF);

fn global_test_administrator() -> Administrator {
	Administrator {
		username: String::from("global_test_administrator"),
		password: String::from("some_password"),
		organisations: vec![],
	}
}

fn org1_test_administrator() -> Administrator {
	Administrator {
		username: String::from("org1_test_administrator"),
		password: String::from("some_other_password"),
		organisations: vec![ORG_1_UUID],
	}
}

pub async fn spawn_server(settings: Settings, pool: PgPool) -> u16 {
	static INSTANCE: OnceCell<()> = OnceCell::new();
	INSTANCE.get_or_init(|| {
		let subscriber = tracing_subscriber::FmtSubscriber::builder()
			.with_max_level(settings.log_level)
			.finish();
		tracing::subscriber::set_global_default(subscriber).unwrap();
	});

	let (tx, rx) = tokio::sync::oneshot::channel::<u16>();

	tokio::spawn(async {
		let addr =
			AddrIncoming::bind(&SocketAddr::V4(SocketAddrV4::new(Ipv4Addr::LOCALHOST, 0))).unwrap();

		tx.send(addr.local_addr().port()).unwrap();

		axum::Server::builder(addr)
			.serve(
				prowovider_core::api::router(settings, Backend::Postgres(pool), false)
					.into_make_service(),
			)
			.await
			.unwrap()
	});

	let port = rx.await.unwrap();
	tracing::debug!("Spawned test on port: {}", port);
	port
}

fn path(path: &str, port: u16) -> String {
	assert_eq!(path.chars().next(), Some('/'));
	format!("http://127.0.0.1:{}{}", port, path)
}

pub fn basic_auth(username: &str, password: &str) -> reqwest::header::HeaderValue {
	reqwest::header::HeaderValue::from_str(
		format!("Basic {}", base64::encode(format!("{}:{}", username, password))).as_str(),
	)
	.unwrap()
}

async fn get<T: DeserializeOwned + PartialEq + std::fmt::Debug>(
	api_path: &str,
	port: u16,
	auth: Option<HeaderValue>,
	expected_status: StatusCode,
	expected: T,
) {
	let mut req = reqwest::Client::default().get(path(api_path, port));

	if let Some(auth) = auth {
		req = req.header("Authorization", auth);
	}

	let res = req.send().await.unwrap();

	assert_eq!(res.status(), expected_status);
	assert_eq!(
		res.json::<prowovider_api::ApiResult<T>>().await.unwrap(),
		prowovider_api::ApiResult::Ok(expected)
	);
}

async fn get_unsorted<T: DeserializeOwned + PartialEq + std::fmt::Debug>(
	api_path: &str,
	port: u16,
	auth: Option<HeaderValue>,
	expected: Vec<T>,
) {
	let mut req = reqwest::Client::default().get(path(api_path, port));

	if let Some(auth) = auth {
		req = req.header("Authorization", auth);
	}

	let res = req.send().await.unwrap();

	assert_eq!(res.status(), StatusCode::OK);

	let out = res.json::<prowovider_api::ApiResult<Vec<T>>>().await.unwrap();

	let out = match out {
		prowovider_api::ApiResult::Ok(ok) => ok,
		prowovider_api::ApiResult::Err(err) => panic!("API Error: {:?}", err),
	};

	assert_eq!(out.len(), expected.len());

	for expected in &expected {
		assert!(out.contains(expected), "Value mismatch");
	}
}

fn read_settings() -> Settings {
	let mut settings = prowovider_core::settings::Settings::read(None).unwrap();
	settings.administrators.push(global_test_administrator());
	settings.administrators.push(org1_test_administrator());
	settings.openid_connect.insert(
		ORG_1_UUID,
		OpenIDConnectSettings {
			base_url: url::Url::from_str("http://localhost").unwrap(),
			jwt: KeyConfig {
				exp: ExpiryConfig {
					authorization_code_expiry: time::Duration::seconds(1800),
					access_token_expiry: time::Duration::seconds(86400),
					id_token_expiry: time::Duration::seconds(86400),
					refresh_token_expiry: time::Duration::seconds(86400),
					generic_expiry: time::Duration::seconds(1800),
				},
				keys: KeySet(vec![serde_json::from_value(serde_json::json!(
					{
						"kty": "oct",
						"k": "f8bug36wf4jonutx",
						"use": "sig",
						"kid": "ONwr4FJtDlp32JbjoKuC2kqEMizahzPjkFKtLdhT2VM",
						"alg": "HS512"
					}
				))
				.unwrap()]),
			},
		},
	);
	settings
}

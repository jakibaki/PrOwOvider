/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use std::path::Path;

use crate::{models::Backend, settings::Settings};

#[derive(Debug, thiserror::Error)]
pub enum ProviderError {
	#[error("Config error: {0}")]
	Config(#[from] config::ConfigError),
	#[error("Failed to set default global tracing subscriber: {0}")]
	TracingGlobal(#[from] tracing::subscriber::SetGlobalDefaultError),
	#[error("Sqlx error: {0}")]
	Sqlx(#[from] sqlx::Error),
	#[error("Failed to run database migrations: {0}")]
	SqlxMigration(#[from] sqlx::migrate::MigrateError),
	#[error("Hyper error: {0}")]
	Hyper(#[from] hyper::Error),
}

pub async fn init(
	config_path_override: Option<&Path>,
) -> Result<(Settings, Backend), ProviderError> {
	let settings = crate::settings::Settings::read(config_path_override)?;
	let backend = Backend::new(&settings.backend).await?;

	let subscriber =
		tracing_subscriber::FmtSubscriber::builder().with_max_level(settings.log_level).finish();
	tracing::subscriber::set_global_default(subscriber)?;

	Ok((settings, backend))
}

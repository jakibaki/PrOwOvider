/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use clap::StructOpt;

use crate::args::Args;

pub mod api;
pub mod args;
pub mod models;
pub mod provider;
pub mod settings;

#[tokio::main]
async fn main() -> Result<(), provider::ProviderError> {
	let args = Args::parse();
	let (settings, backend) = provider::init(args.config.as_deref()).await?;
	tracing::info!("Binding to {}", &settings.bind_address);
	axum::Server::bind(&settings.bind_address)
		.serve(api::router(settings, backend, true).into_make_service())
		.await?;

	Ok(())
}

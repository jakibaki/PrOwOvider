/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

pub const BCRYPT_DIFFICULTY: u32 = 12;

pub type DbResult<T> = Result<T, sqlx::Error>;
pub type PgTransaction<'c> = sqlx::Transaction<'c, sqlx::Postgres>;

mod organisation;
pub mod postgres;

pub use organisation::*;
use sqlx::PgPool;
use uuid::Uuid;

use crate::{api::error::WrappedApiError, provider::ProviderError, settings::BackendSettings};

use self::postgres::PostgresModelError;

pub type ModelResult<Success, Other> = Result<Success, ModelError<Other>>;

pub trait ModelErrorMarker: Into<WrappedApiError> + std::error::Error {}

#[derive(Debug)]
pub enum ModelError<Other: ModelErrorMarker> {
	/// Action is unsupported by storage backend
	Unsupported,
	/// Other error
	Other(Other),
}

#[derive(Debug, Clone)]
pub enum Backend {
	Postgres(PgPool),
}

impl Backend {
	pub async fn new(settings: &BackendSettings) -> Result<Self, ProviderError> {
		let backend = match settings {
			BackendSettings::Postgres(settings) => Self::Postgres(postgres::init(settings).await?),
		};

		Ok(backend)
	}
}

#[derive(Debug, thiserror::Error)]
pub enum BackendError {
	#[error(transparent)]
	Postgres(#[from] PostgresModelError),
}

impl ModelErrorMarker for BackendError {}

impl From<BackendError> for ModelError<BackendError> {
	fn from(err: BackendError) -> Self {
		Self::Other(err)
	}
}

impl From<ModelError<PostgresModelError>> for ModelError<BackendError> {
	fn from(err: ModelError<PostgresModelError>) -> Self {
		match err {
			ModelError::Unsupported => Self::Unsupported,
			ModelError::Other(err) => Self::Other(BackendError::Postgres(err)),
		}
	}
}

impl From<BackendError> for WrappedApiError {
	fn from(err: BackendError) -> Self {
		match err {
			BackendError::Postgres(err) => err.into(),
		}
	}
}

/// This macro will generate a body for each async function signature in a trait that passes the call to the variable backend
macro_rules! handoff {
	(impl $ttrait:ident for $ttype:ty { $(async fn $fnname:ident(&self, $($aname:ident: $atype:ty),*) -> $res:ty;)* }) => {
		#[async_trait::async_trait]
		impl $ttrait for $ttype {
			type Error = BackendError;

			$(async fn $fnname(&self$(, $aname: $atype)*) -> $res {
				Ok(match self {
					Self::Postgres(postgres) => postgres.$fnname($($aname),*).await?,
				})
			})*
		}
	};
}

handoff!(
	impl OrganisationModel for Backend {
		async fn all_organisations(
			&self,
		) -> ModelResult<Vec<Organisation>, Self::Error>;

		async fn organisation_exists_by_id(
			&self,
			organisation_id: Uuid
		) -> ModelResult<bool, Self::Error>;

		async fn organisation_exists_by_name(&self, name: &str) -> ModelResult<bool, Self::Error>;

		async fn create_organisation(
			&self,
			name: &str,
			organisation_id: Option<Uuid>
		) -> ModelResult<Organisation, Self::Error>;

		async fn delete_organisation(&self, organisation_id: Uuid) -> ModelResult<(), Self::Error>;

		async fn rename_organisation(
			&self,
			organisation_id: Uuid,
			name: &str
		) -> ModelResult<(), Self::Error>;

		async fn get_organisation_by_name(
			&self,
			name: &str
		) -> ModelResult<Option<Organisation>, Self::Error>;

		async fn get_organisation_by_id(
			&self,
			organisation_id: Uuid
		) -> ModelResult<Option<Organisation>, Self::Error>;
	}
);

handoff!(
	impl OrganisationUserModel for Backend {
		async fn get_users_by_organisation_id(
			&self,
			organisation_id: Uuid
		) -> ModelResult<Vec<OrganisationUser>, Self::Error>;

		async fn get_user_id_by_username(
			&self,
			organisation_id: Uuid,
			username: &str
		) -> ModelResult<Option<Uuid>, Self::Error>;

		async fn get_user_by_user_id(
			&self,
			organisation_id: Uuid,
			user_id: Uuid
		) -> ModelResult<Option<OrganisationUser>, Self::Error>;

		async fn verify_credentials_and_fetch_user(
			&self,
			organisation_id: Uuid,
			username: &str,
			password: &str
		) -> ModelResult<Option<OrganisationUser>, Self::Error>;

		async fn new_user(
			&self,
			organisation_id: Uuid,
			username: &str,
			password: &str
		) -> ModelResult<OrganisationUser, Self::Error>;

		async fn delete_user(
			&self,
			organisation_id: Uuid,
			user_id: Uuid
		) -> ModelResult<(), Self::Error>;

		async fn user_exists_by_username(
			&self,
			organisation_id: Uuid,
			username: &str
		) -> ModelResult<bool, Self::Error>;

		async fn user_exists_by_id(
			&self,
			organisation_id: Uuid,
			id: Uuid
		) -> ModelResult<bool, Self::Error>;

		async fn user_update_username(
			&self,
			organisation_id: Uuid,
			id: Uuid,
			username: &str
		) -> ModelResult<(), Self::Error>;

		async fn user_update_password(
			&self,
			organisation_id: Uuid,
			id: Uuid,
			password: &str
		) -> ModelResult<(), Self::Error>;

		async fn user_update_username_password(
			&self,
			organisation_id: Uuid,
			id: Uuid,
			username: &str,
			password: &str
		) -> ModelResult<(), Self::Error>;
	}
);

handoff!(
	impl OrganisationClaimModel for Backend {
		async fn claim_exists(
			&self,
			organisation_id: Uuid,
			claim_name: &str
		) -> ModelResult<bool, Self::Error>;

		async fn get_claims_by_organisation_id(
			&self,
			organisation_id: Uuid
		) -> ModelResult<Vec<OrganisationClaim>, Self::Error>;

		async fn get_claim_type_by_organisation_id_and_claim_name(
			&self,
			organisation_id: Uuid,
			claim_name: &str
		) -> ModelResult<Option<Option<ClaimType>>, Self::Error>;

		async fn new_claim(
			&self,
			organisation_id: Uuid,
			claim_name: &str,
			r#type: Option<ClaimType>
		) -> ModelResult<OrganisationClaim, Self::Error>;

		async fn delete_claim(
			&self,
			organisation_id: Uuid,
			claim_name: &str
		) -> ModelResult<(), Self::Error>;

		async fn rename_claim(
			&self,
			organisation_id: Uuid,
			old_claim_name: &str,
			new_claim_name: &str
		) -> ModelResult<(), Self::Error>;
	}
);

handoff!(
	impl OrganisationUserClaimModel for Backend {
		async fn user_claim_exists(
			&self,
			organisation_id: Uuid,
			user_id: Uuid,
			claim_name: &str
		) -> ModelResult<bool, Self::Error>;

		async fn get_claims_by_user_id(
			&self,
			organisation_id: Uuid,
			user_id: Uuid
		) -> ModelResult<Vec<OrganisationUserClaim>, Self::Error>;

		async fn get_claims_by_user_id_and_scopes_with_destination(
			&self,
			organisation_id: Uuid,
			user_id: Uuid,
			scopes: &[String],
			destination: ClaimDestination
		) -> ModelResult<serde_json::Map<String, serde_json::Value>, Self::Error>;

		async fn get_claim_by_user_id_and_claim_name(
			&self,
			organisation_id: Uuid,
			user_id: Uuid,
			claim_name: &str
		) -> ModelResult<Option<OrganisationUserClaim>, Self::Error>;

		async fn upsert_claim_on_user(
			&self,
			organisation_id: Uuid,
			user_id: Uuid,
			claim_name: &str,
			claim_value: &serde_json::Value
		) -> ModelResult<(), Self::Error>;

		async fn delete_claim_on_user(
			&self,
			organisation_id: Uuid,
			user_id: Uuid,
			claim_name: &str
		) -> ModelResult<(), Self::Error>;
	}
);

handoff!(
	impl OrganisationScopeModel for Backend {
		async fn scope_exists(
			&self,
			organisation_id: Uuid,
			scope: &str
		) -> ModelResult<bool, Self::Error>;

		async fn get_scopes_by_organisation_id(
			&self,
			organisation_id: Uuid
		) -> ModelResult<Vec<OrganisationScope>, Self::Error>;

		async fn new_scope(
			&self,
			organisation_id: Uuid,
			scope: &str
		) -> ModelResult<OrganisationScope, Self::Error>;

		async fn delete_scope(
			&self,
			organisation_id: Uuid,
			scope: &str
		) -> ModelResult<(), Self::Error>;
	}
);

handoff!(
	impl OrganisationUserScopeModel for Backend {
		async fn user_scope_exists(
			&self,
			organisation_id: Uuid,
			user_id: Uuid,
			scope: &str
		) -> ModelResult<bool, Self::Error>;

		async fn get_scopes_by_user_id(
			&self,
			organisation_id: Uuid,
			user_id: Uuid
		) -> ModelResult<Vec<OrganisationUserScope>, Self::Error>;

		async fn get_scope_by_user_id_and_scope_name(
			&self,
			organisation_id: Uuid,
			user_id: Uuid,
			scope: &str
		) -> ModelResult<Option<OrganisationUserScope>, Self::Error>;

		async fn new_scope_on_user(
			&self,
			organisation_id: Uuid,
			user_id: Uuid,
			scope: &str
		) -> ModelResult<OrganisationUserScope, Self::Error>;

		async fn delete_scope_on_user(
			&self,
			organisation_id: Uuid,
			user_id: Uuid,
			scope: &str
		) -> ModelResult<Option<OrganisationUserScope>, Self::Error>;
	}
);

handoff!(
	impl OrganisationClientModel for Backend {
		async fn new_client(
			&self,
			organisation_id: Uuid,
			name: &str,
			client_type: ClientType,
			preferred_key: Option<&str>
		) -> ModelResult<OrganisationClient, Self::Error>;

		async fn set_preferred_key_for_client(
			&self,
			id: Uuid,
			organisation_id: Uuid,
			preferred_key: Option<&str>
		) -> ModelResult<(), Self::Error>;

		async fn get_clients_by_organisation_id(
			&self,
			organisation_id: Uuid
		) -> ModelResult<Vec<OrganisationClient>, Self::Error>;

		async fn get_client_by_id(
			&self,
			id: Uuid,
			organisation_id: Uuid
		) -> ModelResult<Option<OrganisationClient>, Self::Error>;

		async fn get_name_of_client(
			&self,
			id: Uuid,
			organisation_id: Uuid
		) -> ModelResult<Option<String>, Self::Error>;

		async fn client_exists(
			&self,
			id: Uuid,
			organisation_id: Uuid
		) -> ModelResult<bool, Self::Error>;

		async fn delete_client(
			&self,
			id: Uuid,
			organisation_id: Uuid
		) -> ModelResult<(), Self::Error>;
	}
);

handoff!(
	impl ClientRedirectUriModel for Backend {
		async fn get_redirect_uris_by_client_id(
			&self,
			client_id: Uuid
		) -> ModelResult<Vec<String>, Self::Error>;

		async fn delete_redirect_uri(
			&self,
			client_id: Uuid,
			redirect_uri: &str
		) -> ModelResult<(), Self::Error>;

		async fn create_redirect_uri(
			&self,
			client_id: Uuid,
			redirect_uri: &str
		) -> ModelResult<(), Self::Error>;

		async fn redirect_uri_exists(
			&self,
			client_id: Uuid,
			redirect_uri: &str
		) -> ModelResult<bool, Self::Error>;
	}
);

handoff!(
	impl OrganisationClaimDestinationModel for Backend {
		async fn destination_exists_for_claim(
			&self,
			organisation_id: Uuid,
			claim_name: &str,
			destination: ClaimDestination
		) -> ModelResult<bool, Self::Error>;

		async fn create_destination(
			&self,
			organisation_id: Uuid,
			claim_name: &str,
			destination: ClaimDestination
		) -> ModelResult<(), Self::Error>;

		async fn get_destinations_by_claim_name(
			&self,
			organisation_id: Uuid,
			claim_name: &str
		) -> ModelResult<Vec<ClaimDestination>, Self::Error>;

		async fn delete_destination(
			&self,
			organisation_id: Uuid,
			claim_name: &str,
			destination: ClaimDestination
		) -> ModelResult<(), Self::Error>;
	}
);

handoff!(
	impl OrganisationScopeClaimMapModel for Backend {
		async fn scope_claim_mapping_exists(
			&self,
			organisation_id: Uuid,
			scope: &str,
			claim_name: &str
		) -> ModelResult<bool, Self::Error>;

		async fn create_scope_claim_mapping(
			&self,
			organisation_id: Uuid,
			scope: &str,
			claim_name: &str
		) -> ModelResult<(), Self::Error>;

		async fn get_mapped_claims_by_scope(
			&self,
			organisation_id: Uuid,
			scope: &str
		) -> ModelResult<Vec<String>, Self::Error>;

		async fn get_scopes_by_claim_name(
			&self,
			organisation_id: Uuid,
			claim_name: &str
		) -> ModelResult<Vec<String>, Self::Error>;

		async fn delete_scope_claim_mapping(
			&self,
			organisation_id: Uuid,
			scope: &str,
			claim_name: &str
		) -> ModelResult<(), Self::Error>;
	}
);

/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

mod admin_auth;
pub mod error;
pub mod logger;
pub mod openid;
mod organisations;
pub mod tokens;

use std::{borrow::Cow, sync::Arc};

use axum::{
	body::Body,
	handler::Handler,
	routing::{get, post},
	Json, Router,
};
use hyper::Method;
use tower_http::cors::{Any, CorsLayer};

use crate::{models::Backend, settings::Settings};

use error::WrappedApiError;

pub type ApiResult<T> = Result<T, WrappedApiError>;
pub type JsonResult<T> = ApiResult<Json<T>>;

macro_rules! static_route {
	($i:ident, $path:literal, $ct:literal) => {
		$i.route(
			$path,
			get(|| async {
				let mut response = axum::response::Response::new(axum::body::boxed(
					axum::body::Body::from(include_str!(concat!("..", $path))),
				));
				response.headers_mut().insert(
					axum::http::header::CONTENT_TYPE,
					axum::http::HeaderValue::from_static($ct),
				);
				response
			}),
		)
	};
}

fn validate_url_parameter(parameter: &str) -> Result<(), WrappedApiError> {
	if parameter.len() < 128 {
		let match_res = lazy_regex::regex!("[a-z0-9-_]{1, 127}").find(parameter);

		if let Some(r#match) = match_res {
			if r#match.start() == 0 && r#match.end() == parameter.len() {
				return Ok(());
			}
		}
	}

	Err(WrappedApiError::parameter_bad_character(Some(Cow::Borrowed("Parameter must be less than 128 characters and only contain alphanumeric characters, minus symbols, and underscores"))))
}

async fn not_found_fallback() -> Json<error::WrappedApiError> {
	Json(error::WrappedApiError::path_not_found(Some(Cow::Borrowed(
		"Ensure your URL does not contain typos and is formatted correctly",
	))))
}

pub fn router(settings: Settings, backend: Backend, log_requests: bool) -> Router<Body> {
	let router = Router::new()
		.route("/", get(|| async { "Mew!" }))
		.route(
			"/api/v1/organisations",
			get(organisations::organisations_list).post(organisations::create_new_organisation),
		)
		.route(
			"/api/v1/organisations/:organisation_id",
			get(organisations::get_organisation)
				.put(organisations::rename_organisation)
				.delete(organisations::delete_organisation),
		)
		.route(
			"/api/v1/organisations/:organisation_id/scopes",
			get(organisations::scopes::list_scopes_on_organisation),
		)
		.route(
			"/api/v1/organisations/:organisation_id/scopes/:scope_name",
			get(organisations::scopes::get_scope_on_organisation)
				.put(organisations::scopes::put_scope_on_organisation)
				.delete(organisations::scopes::delete_scope_on_organisation),
		)
		.route(
			"/api/v1/organisations/:organisation_id/scopes/:scope_name/claims",
			get(organisations::scopes::get_implied_claims_for_scope_on_organisation),
		)
		.route(
			"/api/v1/organisations/:organisation_id/scopes/:scope_name/claims/:claim_name",
			post(organisations::scopes::post_implied_claim_for_scope_on_organisation)
				.delete(organisations::scopes::delete_implied_claim_for_scope_on_organisation),
		)
		.route(
			"/api/v1/organisations/:organisation_id/claims",
			get(organisations::claims::list_claims_on_organisation),
		)
		.route(
			"/api/v1/organisations/:organisation_id/claims/:claim_name",
			get(organisations::claims::get_claim_on_organisation)
				.put(organisations::claims::put_claim_on_organisation)
				.delete(organisations::claims::delete_claim_on_organisation),
		)
		.route(
			"/api/v1/organisations/:organisation_id/claims/:claim_name/destinations",
			get(organisations::claims::get_destinations_for_claims_on_organisation),
		)
		.route(
			"/api/v1/organisations/:organisation_id/claims/:claim_name/destinations/id_token",
			post(organisations::claims::post_dest_id_token_on_claim_on_organisation)
				.delete(organisations::claims::delete_dest_id_token_on_claim_on_organisation),
		)
		.route(
			"/api/v1/organisations/:organisation_id/claims/:claim_name/destinations/access_token",
			post(organisations::claims::post_dest_access_token_on_claim_on_organisation)
				.delete(organisations::claims::delete_dest_access_token_on_claim_on_organisation),
		)
		.route(
			"/api/v1/organisations/:organisation_id/claims/:claim_name/destinations/user_info",
			post(organisations::claims::post_dest_user_info_on_claim_on_organisation)
				.delete(organisations::claims::delete_dest_user_info_on_claim_on_organisation),
		)
		.route(
			"/api/v1/organisations/:organisation_id/users",
			get(organisations::users::list_users_on_organisation)
				.post(organisations::users::create_user_on_organisation),
		)
		.route(
			"/api/v1/organisations/:organisation_id/users/:user_id",
			get(organisations::users::get_user_on_organisation)
				.put(organisations::users::update_username_or_password_of_user_on_organisation)
				.delete(organisations::users::delete_user_on_organisation),
		)
		.route(
			"/api/v1/organisations/:organisation_id/users/resolve/:username",
			get(organisations::users::resolve_username),
		)
		.route(
			"/api/v1/organisations/:organisation_id/users/:user_id/scopes",
			get(organisations::users::scopes::get_scopes_on_user_on_organisation),
		)
		.route(
			"/api/v1/organisations/:organisation_id/users/:user_id/scopes/:scope_name",
			get(organisations::users::scopes::get_scope_on_user_on_organisation)
				.post(organisations::users::scopes::allow_scope_on_user_on_organisation)
				.delete(organisations::users::scopes::delete_scope_on_user_on_organisations),
		)
		.route(
			"/api/v1/organisations/:organisation_id/users/:user_id/claims",
			get(organisations::users::claims::get_claims_on_user_on_organisation),
		)
		.route(
			"/api/v1/organisations/:organisation_id/users/:user_id/claims/:claim_name",
			get(organisations::users::claims::get_claim_on_user_on_organisation)
				.put(organisations::users::claims::upsert_claim_on_user_on_organisations)
				.delete(organisations::users::claims::delete_claim_on_user_on_organisations),
		)
		.route(
			"/api/v1/organisations/:organisation_id/clients",
			get(organisations::clients::list_clients_on_organisation)
				.post(organisations::clients::post_client_on_organisation),
		)
		.route(
			"/api/v1/organisations/:organisation_id/clients/:client_id",
			get(organisations::clients::get_client_on_organisation)
				.delete(organisations::clients::delete_client_on_organisation),
		)
		.route(
			"/api/v1/organisations/:organisation_id/clients/:client_id/redirect_uris",
			get(organisations::clients::list_redirect_uris_on_client_on_organisation)
				.put(organisations::clients::put_redirect_uri_on_client_on_organisation)
				.delete(organisations::clients::delete_redirect_uri_on_client_on_organisation),
		)
		.route(
			"/api/v1/organisations/:organisation_id/clients/:client_id/preferred_key",
			get(organisations::clients::get_preferred_key_id_on_client)
				.put(organisations::clients::put_preferred_key_id_on_client)
				.delete(organisations::clients::delete_preferred_key_id_on_client),
		)
		.route("/api/v1/openid/:organisation_id/authorize", get(openid::authentication_endpoint))
		.route("/api/v1/openid/:organisation_id/login", post(openid::login_endpoint))
		.route("/api/v1/openid/:organisation_id/token", post(openid::token_endpoint))
		.route(
			"/api/v1/openid/:organisation_id/.well-known/openid-configuration",
			get(openid::discovery_configuration_endpoint),
		)
		.route("/api/v1/openid/:organisation_id/jwks.json", get(openid::jwks_endpoint))
		.route(
			"/api/v1/openid/:organisation_id/userinfo",
			get(openid::user_info_endpoint).post(openid::user_info_endpoint),
		)
		.fallback(not_found_fallback.into_service())
		.layer(axum::extract::Extension(Arc::new(settings)))
		.layer(axum::extract::Extension(backend))
		.layer(
			CorsLayer::new()
				.allow_methods(vec![Method::GET, Method::POST, Method::DELETE, Method::OPTIONS])
				.allow_origin(Any)
				.allow_headers(Any),
		);

	let router = static_route!(router, "/static/css/login.css", "text/css");

	match log_requests {
		true => router.layer(logger::LogLayer),
		false => router,
	}
}

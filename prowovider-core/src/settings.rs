/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

pub mod jwk;

use config::Config;
use serde_with::{serde_as, DisplayFromStr};
use std::{collections::HashMap, net::SocketAddr, path::Path};
use uuid::Uuid;

use self::jwk::KeyConfig;

/// Tracing log level implementing [serde::Deserialize]
#[derive(Debug, serde::Deserialize, Clone, Copy, PartialEq, Eq)]
pub enum Level {
	/// Equivalent to [tracing::level_filters::LevelFilter::TRACE]
	Trace,
	/// Equivalent to [tracing::level_filters::LevelFilter::DEBUG]
	Debug,
	/// Equivalent to [tracing::level_filters::LevelFilter::INFO]
	Info,
	/// Equivalent to [tracing::level_filters::LevelFilter::WARN]
	Warn,
	/// Equivalent to [tracing::level_filters::LevelFilter::ERROR]
	Error,
}

impl Default for Level {
	fn default() -> Self {
		Self::Info
	}
}

impl From<Level> for tracing::level_filters::LevelFilter {
	fn from(level: Level) -> Self {
		match level {
			Level::Trace => Self::TRACE,
			Level::Debug => Self::DEBUG,
			Level::Info => Self::INFO,
			Level::Warn => Self::WARN,
			Level::Error => Self::ERROR,
		}
	}
}

#[derive(Debug, Clone, serde::Deserialize)]
pub struct DatabaseSettings {
	pub url: String,
	pub max_connections: Option<u32>,
}

#[derive(Debug, Clone, serde::Deserialize)]
#[serde(tag = "type", content = "settings")]
pub enum BackendSettings {
	#[serde(alias = "postgres")]
	Postgres(DatabaseSettings),
}

#[derive(Debug, Clone, serde::Deserialize)]
pub struct Administrator {
	/// Basic auth username
	pub username: String,
	/// Basic auth password
	pub password: String,
	/// Optional list of organisations, if not-present or empty the administrator will be a global administrator
	#[serde(default)]
	pub organisations: Vec<Uuid>,
}

#[serde_as]
#[derive(Debug, Clone, serde::Deserialize)]
pub struct OpenIDConnectSettings {
	#[serde_as(as = "DisplayFromStr")]
	pub base_url: url::Url,
	pub jwt: KeyConfig,
}

#[derive(Debug, Clone, serde::Deserialize)]
pub struct Settings {
	pub backend: BackendSettings,
	pub log_level: Level,
	pub bind_address: SocketAddr,
	#[serde(default)]
	pub administrators: Vec<Administrator>,
	pub openid_connect: HashMap<Uuid, OpenIDConnectSettings>,
}

impl Settings {
	pub fn read(config_path_override: Option<&Path>) -> Result<Self, config::ConfigError> {
		Config::builder()
			.add_source(match config_path_override {
				Some(config) => config::File::from(config),
				None => config::File::with_name("settings"),
			})
			.add_source(config::Environment::with_prefix("PROWOVIDER"))
			.build()?
			.try_deserialize()
	}
}

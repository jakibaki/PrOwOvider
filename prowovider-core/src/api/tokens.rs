/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

//! This module contains defintions of models for JWTs and helpers for encoding and decoding them

use std::borrow::Cow;

use openid_types::{endpoints::requests::AuthenticationRequest, token::CodeTokenClaims};
use serde::{Deserialize, Serialize};
use serde_json::{Map, Value};
use serde_with::{serde_as, DisplayFromStr};
use time::OffsetDateTime;
use uuid::Uuid;

use crate::{
	api::openid::helpers::Subject,
	settings::{
		jwk::{KeyConfig, ParsedKey},
		OpenIDConnectSettings,
	},
};

fn iss(organisation_id: Uuid, settings: &OpenIDConnectSettings) -> String {
	format!("{}/api/v1/openid/{}", settings.base_url, organisation_id)
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq, Eq)]
pub struct LoginState {
	/// Issuer URL
	pub iss: String,
	/// Expiry of the token
	#[serde(with = "time::serde::timestamp")]
	pub exp: OffsetDateTime,
	/// Requested scopes
	pub scopes: Vec<String>,
	/// Original request parameters
	pub original_request: AuthenticationRequest,
	/// Organisation ID
	pub organisation_id: Uuid,
}

impl LoginState {
	pub fn new(
		scopes: Vec<String>,
		original_request: AuthenticationRequest,
		organisation_id: Uuid,
		settings: &OpenIDConnectSettings,
	) -> Self {
		let iss = iss(organisation_id, settings);
		let exp = OffsetDateTime::now_utc() + settings.jwt.exp.generic_expiry;

		Self { scopes, original_request, organisation_id, iss, exp }
	}
}

#[serde_as]
#[derive(Debug, Clone, Deserialize, Serialize, PartialEq, Eq)]
pub struct OpenIDAuthorizationCode {
	/// Issuer URL
	pub iss: String,
	/// Expiry of the token
	#[serde(with = "time::serde::timestamp")]
	pub exp: OffsetDateTime,
	/// Subject
	#[serde_as(as = "DisplayFromStr")]
	pub sub: Subject,
	/// Requested scopes
	pub scopes: Vec<String>,
	/// Original request parameters
	pub original_request: AuthenticationRequest,
}

impl OpenIDAuthorizationCode {
	pub fn new(
		sub: Subject,
		scopes: Vec<String>,
		original_request: AuthenticationRequest,
		settings: &OpenIDConnectSettings,
	) -> Self {
		let iss = iss(sub.organisation, settings);
		let exp = OffsetDateTime::now_utc() + settings.jwt.exp.access_token_expiry;

		Self { scopes, original_request, sub, iss, exp }
	}
}

#[serde_as]
#[derive(Debug, Clone, Deserialize, Serialize, PartialEq, Eq)]
pub struct OpenIDAccessToken {
	/// Issuer URL
	pub iss: String,
	/// Expiry of the token
	#[serde(with = "time::serde::timestamp")]
	pub exp: OffsetDateTime,
	/// Subject of the token
	#[serde_as(as = "DisplayFromStr")]
	pub sub: Subject,
	/// Requested scopes
	pub scopes: Vec<String>,
	/// ID of the client used
	pub client_id: Uuid,
	#[serde(flatten)]
	pub extra: serde_json::Map<String, serde_json::Value>,
}

impl OpenIDAccessToken {
	pub fn new(
		sub: Subject,
		scopes: Vec<String>,
		client_id: Uuid,
		extra: serde_json::Map<String, serde_json::Value>,
		settings: &OpenIDConnectSettings,
	) -> Self {
		let iss = iss(sub.organisation, settings);
		let exp = OffsetDateTime::now_utc() + settings.jwt.exp.access_token_expiry;

		Self { sub, scopes, client_id, extra, iss, exp }
	}
}

#[serde_as]
#[derive(Debug, Clone, Deserialize, Serialize, PartialEq, Eq)]
pub struct OpenIDRefreshToken {
	/// Expiry of the token
	#[serde(with = "time::serde::timestamp")]
	pub exp: OffsetDateTime,
	/// Claims of the issued access token
	pub original_claims: OpenIDAccessToken,
	/// Client ID the token was issued to
	pub client_id: Uuid,
}

impl OpenIDRefreshToken {
	pub fn new(
		original_claims: OpenIDAccessToken,
		client_id: Uuid,
		settings: &OpenIDConnectSettings,
	) -> Self {
		let exp = OffsetDateTime::now_utc() + settings.jwt.exp.refresh_token_expiry;

		Self { exp, original_claims, client_id }
	}
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(tag = "token_type")]
pub enum Token<'a> {
	LoginState(Cow<'a, LoginState>),
	OpenIDAuthorizationCode(Cow<'a, OpenIDAuthorizationCode>),
	OpenIDAccessToken(Cow<'a, OpenIDAccessToken>),
	OpenIDRefreshToken(Cow<'a, OpenIDRefreshToken>),
	OpenIDToken(Cow<'a, CodeTokenClaims<Map<String, Value>>>),
}

impl Token<'_> {
	pub fn encode(&self, key: &ParsedKey) -> jsonwebtoken::errors::Result<String> {
		let mut header = jsonwebtoken::Header::new(key.algorithm);
		header.kid = Some(key.jwk.key_id.clone());
		jsonwebtoken::encode(&header, self, &key.encoding)
	}

	pub fn decode(token: &str, config: &KeyConfig) -> Option<jsonwebtoken::errors::Result<Self>> {
		let header = match jsonwebtoken::decode_header(token) {
			Ok(header) => header,
			Err(why) => return Some(Err(why)),
		};

		let key = header.kid.and_then(|kid| config.keys.by_id(kid.as_str()))?;

		if key.algorithm != header.alg {
			return None;
		}

		let validation = jsonwebtoken::Validation::new(key.algorithm);
		match jsonwebtoken::decode(token, &key.decoding, &validation) {
			Ok(claims) => Some(Ok(claims.claims)),
			Err(why) => Some(Err(why)),
		}
	}

	pub fn decode_openid_access_token(
		token: &str,
		config: &KeyConfig,
	) -> Option<jsonwebtoken::errors::Result<OpenIDAccessToken>> {
		match Self::decode(token, config)? {
			Ok(Token::OpenIDAccessToken(token)) => Some(Ok(token.into_owned())),
			Err(why) => Some(Err(why)),
			_ => None,
		}
	}

	pub fn decode_openid_authorization_code(
		token: &str,
		config: &KeyConfig,
	) -> Option<jsonwebtoken::errors::Result<OpenIDAuthorizationCode>> {
		match Self::decode(token, config)? {
			Ok(Token::OpenIDAuthorizationCode(token)) => Some(Ok(token.into_owned())),
			Err(why) => Some(Err(why)),
			_ => None,
		}
	}

	pub fn decode_openid_refresh_token(
		token: &str,
		config: &KeyConfig,
	) -> Option<jsonwebtoken::errors::Result<OpenIDRefreshToken>> {
		match Self::decode(token, config)? {
			Ok(Token::OpenIDRefreshToken(token)) => Some(Ok(token.into_owned())),
			Err(why) => Some(Err(why)),
			_ => None,
		}
	}

	pub fn decode_login_state(
		token: &str,
		config: &KeyConfig,
	) -> Option<jsonwebtoken::errors::Result<LoginState>> {
		match Self::decode(token, config)? {
			Ok(Token::LoginState(token)) => Some(Ok(token.into_owned())),
			Err(why) => Some(Err(why)),
			_ => None,
		}
	}
}

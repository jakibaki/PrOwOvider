/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use std::{borrow::Cow, sync::Arc};

use axum::{
	extract::{FromRequest, RequestParts},
	http::HeaderValue,
};
use if_chain::if_chain;

use crate::{api::WrappedApiError, settings::Administrator};

pub fn invalid_auth() -> WrappedApiError {
	WrappedApiError::invalid_auth(
		None,
		HeaderValue::from_static(r#"Basic realm="Accessing admin api", charset="UTF-8""#),
	)
}

#[axum::async_trait]
impl<B: Send> FromRequest<B> for Administrator {
	type Rejection = WrappedApiError;

	async fn from_request(req: &mut RequestParts<B>) -> Result<Self, Self::Rejection> {
		let settings = match req.extensions().get::<Arc<crate::settings::Settings>>() {
			Some(settings) => settings.clone(),
			None => {
				tracing::error!("Failed to find Arc<Settings> extension");
				return Err(WrappedApiError::internal_error(Some(Cow::Borrowed(
					"Failed to find Arc<Settings> extension",
				))));
			}
		};

		let headers = req.headers();
		if_chain! {
			if let Some(Ok(header)) = headers.get(axum::http::header::AUTHORIZATION).map(axum::http::HeaderValue::to_str);
			if let Some(stripped) = header.strip_prefix("Basic ");
			if let Ok(decoded) = base64::decode(stripped);
			if let Ok(parsed_string) = String::from_utf8(decoded);
			if let Some((username, password)) = parsed_string.split_once(':');
			if let Some(administrator) = settings.administrators.iter().find(|administrator| {
				administrator.username.as_str() == username
					&& administrator.password.as_str() == password
			});
			then {
				Ok(administrator.clone())
			} else {
				Err(invalid_auth())
			}
		}
	}
}

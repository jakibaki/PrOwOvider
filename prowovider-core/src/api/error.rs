/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use std::borrow::Cow;

use axum::{
	http::HeaderValue,
	response::{IntoResponse, Response},
};
use hyper::StatusCode;

use crate::models::{ModelError, ModelErrorMarker};

#[derive(Debug, PartialEq, Eq, serde::Serialize)]
pub struct WrappedApiError {
	#[serde(skip)]
	headers: Vec<(&'static str, HeaderValue)>,
	#[serde(skip)]
	status: StatusCode,
	error_code: &'static str,
	error_message: Option<Cow<'static, str>>,
}

macro_rules! error {
    ($error:ident, $code:ident, $status:ident$(, $headername:ident=$headerlit:literal)*) => {
		pub const $code: &'static str = stringify!($code);
        pub fn $error(error_message: Option<Cow<'static, str>>$(, $headername: HeaderValue)*) -> Self {
            let headers = vec![$(($headerlit, $headername)),*];
            Self {
                headers,
                status: StatusCode::$status,
                error_code: Self::$code,
                error_message,
            }
        }
    };
}

impl WrappedApiError {
	error!(database, DATABASE_ERROR, INTERNAL_SERVER_ERROR);
	error!(bcrypt, BCRYPT_ERROR, INTERNAL_SERVER_ERROR);
	error!(internal_error, INTERNAL_ERROR, INTERNAL_SERVER_ERROR);
	error!(invalid_auth, INVALID_AUTH, UNAUTHORIZED, www_authenticate = "WWW-Authenticate");
	error!(item_already_exists, ITEM_ALREADY_EXISTS, CONFLICT);
	error!(item_not_found, ITEM_NOT_FOUND, NOT_FOUND);
	error!(path_not_found, PATH_NOT_FOUND, NOT_FOUND);
	error!(missing_parameter, MISSING_PARAMETER, BAD_REQUEST);
	error!(bad_request, BAD_REQUEST, BAD_REQUEST);
	error!(login_failed, LOGIN_FAILED, BAD_REQUEST);
	error!(parameter_bad_character, PARAMAETER_CONTAINS_BAD_CHARACTER, BAD_REQUEST);
	error!(bad_name, BAD_NAME, BAD_REQUEST);
	error!(claim_typed_wrong, CLAIM_TYPED_WRONG, BAD_REQUEST);
	error!(unsupported_backend, UNSUPPORTED_BACKEND, NOT_IMPLEMENTED);

	pub fn key_not_found() -> Self {
		tracing::error!("Failed to find a suitible key");
		Self::internal_error(Some(Cow::Borrowed("Failed to find suitable key to fulfil request")))
	}
}

impl From<sqlx::Error> for WrappedApiError {
	fn from(err: sqlx::Error) -> Self {
		tracing::error!("Database error: {}", err);
		Self::database(None)
	}
}

impl From<bcrypt::BcryptError> for WrappedApiError {
	fn from(err: bcrypt::BcryptError) -> Self {
		tracing::error!("Bcrypt error: {}", err);
		Self::bcrypt(None)
	}
}

impl<Other: ModelErrorMarker> From<ModelError<Other>> for WrappedApiError {
	fn from(err: ModelError<Other>) -> Self {
		match err {
			ModelError::Unsupported => Self::unsupported_backend(Some(Cow::Borrowed(
				"This operation is not supported by the storage backend used",
			))),
			ModelError::Other(other) => other.into(),
		}
	}
}

impl From<serde_json::Error> for WrappedApiError {
	fn from(err: serde_json::Error) -> Self {
		tracing::error!("Serde JSON error: {}", err);
		Self::internal_error(Some(Cow::Borrowed("Serialization error")))
	}
}

impl From<axum::http::Error> for WrappedApiError {
	fn from(err: axum::http::Error) -> Self {
		tracing::error!("Axum HTTP error: {}", err);
		Self::internal_error(Some(Cow::Borrowed("HTTP error")))
	}
}

impl IntoResponse for WrappedApiError {
	fn into_response(self) -> Response {
		let json = match serde_json::to_string(&self) {
			Ok(json) => json,
			Err(err) => {
				tracing::error!("Failed to serialize WrappedApiError: {}", err);
				let mut response = Response::default();
				*response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
				return response;
			}
		};

		let mut builder = Response::builder().status(self.status);

		for (lhs, rhs) in self.headers {
			builder = builder.header(lhs, rhs);
		}

		match builder
			.header(axum::http::header::CONTENT_TYPE, "application/json")
			.body(axum::body::boxed(axum::body::Full::from(json.into_bytes())))
		{
			Ok(res) => res,
			Err(err) => {
				tracing::error!("Failed to build response: {}", err);
				let mut response = Response::default();
				*response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
				response
			}
		}
	}
}

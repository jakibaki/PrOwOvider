/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use std::task::{Context, Poll};

use axum::{body::Body, http::Request, response::Response};
use tower::{Layer, Service};

#[derive(Debug, Clone, Copy)]
pub struct LogService<S> {
	service: S,
}

impl<S> Service<Request<Body>> for LogService<S>
where
	S: Service<Request<Body>, Response = Response> + Send + 'static,
	S::Future: Send + 'static,
{
	type Response = S::Response;
	type Error = S::Error;
	type Future = S::Future;

	fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
		self.service.poll_ready(cx)
	}

	fn call(&mut self, request: Request<Body>) -> Self::Future {
		tracing::info!("[{:?}] {}", request.method(), request.uri());
		self.service.call(request)
	}
}

#[derive(Debug, Clone, Copy)]
pub struct LogLayer;

impl<S> Layer<S> for LogLayer {
	type Service = LogService<S>;

	fn layer(&self, service: S) -> Self::Service {
		LogService { service }
	}
}

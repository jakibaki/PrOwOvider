/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

pub mod claims;
pub mod clients;
pub mod scopes;
pub mod users;

use std::borrow::Cow;

use axum::{extract, Json};
use prowovider_api::{Organisation, OrganisationNameQuery};
use uuid::Uuid;

use crate::{
	models::{Backend, OrganisationModel},
	settings::Administrator,
};

use super::{admin_auth::invalid_auth, error::WrappedApiError, ApiResult, JsonResult};

fn client_not_found() -> WrappedApiError {
	WrappedApiError::item_not_found(Some(Cow::Borrowed("Client not found")))
}

pub async fn organisations_list(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
) -> JsonResult<Vec<Organisation<'static>>> {
	let organisation_list = backend.all_organisations().await?;

	let out_organisations = if !administrator.organisations.is_empty() {
		organisation_list
			.into_iter()
			.filter(|x| administrator.organisations.contains(&x.id))
			.map(Into::into)
			.collect()
	} else {
		organisation_list.into_iter().map(Into::into).collect()
	};

	Ok(Json(out_organisations))
}

pub async fn create_new_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	query: extract::Query<OrganisationNameQuery<'_>>,
) -> JsonResult<Organisation<'static>> {
	// If administrator is scoped to specific organisations then they should not be able to create new organisations
	if !administrator.organisations.is_empty() {
		return Err(invalid_auth());
	}

	match backend.organisation_exists_by_name(query.name.as_ref()).await? {
		true => Err(crate::api::error::WrappedApiError::item_already_exists(Some(Cow::Borrowed(
			"Organisation with name already exists",
		)))),
		false => {
			let organisation = backend.create_organisation(query.name.as_ref(), None).await?;
			Ok(Json(organisation.into()))
		}
	}
}

pub async fn get_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path(uuid): extract::Path<Uuid>,
) -> JsonResult<Organisation<'static>> {
	if !administrator.organisations.is_empty() && !administrator.organisations.contains(&uuid) {
		return Err(invalid_auth());
	}

	match backend.get_organisation_by_id(uuid).await? {
		Some(organisation) => Ok(Json(organisation.into())),
		None => Err(WrappedApiError::item_not_found(Some(Cow::Borrowed("Organisation not found")))),
	}
}

pub async fn rename_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path(uuid): extract::Path<Uuid>,
	query: extract::Query<OrganisationNameQuery<'_>>,
) -> ApiResult<()> {
	// If administrator is scoped to specific organisations then they should not be able to rename organisations, even if they are scoped to it
	if !administrator.organisations.is_empty() {
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(uuid).await?;

	if backend.organisation_exists_by_name(query.name.as_ref()).await? {
		return Err(crate::api::error::WrappedApiError::item_already_exists(Some(Cow::Borrowed(
			"Organisation with name already exists",
		))));
	}

	backend.rename_organisation(uuid, query.name.as_ref()).await?;
	Ok(())
}

pub async fn delete_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path(uuid): extract::Path<Uuid>,
) -> ApiResult<()> {
	// Scoped administrators cannot delete organisations, not even ones they belong to
	if !administrator.organisations.is_empty() {
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(uuid).await?;

	backend.delete_organisation(uuid).await?;
	Ok(())
}

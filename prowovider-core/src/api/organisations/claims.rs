/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use std::borrow::Cow;

use axum::{extract, Json};
use prowovider_api::{ClaimDestination, ClaimParameters};
use uuid::Uuid;

use crate::{
	api::{
		admin_auth::invalid_auth, error::WrappedApiError, validate_url_parameter, ApiResult,
		JsonResult,
	},
	models::{
		self, Backend, OrganisationClaimDestinationModel, OrganisationClaimModel, OrganisationModel,
	},
	settings::Administrator,
};

const RESTRICTED_CLAIM_NAMES: &[&str] = &[
	"iss",
	"sub",
	"aud",
	"exp",
	"iat",
	"auth_time",
	"nonce",
	"acr",
	"amr",
	"azp",
	"at_hash",
	"scopes",
	"client_id",
	"original_request",
	"token_type",
];

pub async fn list_claims_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path(uuid): extract::Path<Uuid>,
) -> JsonResult<Vec<String>> {
	if !administrator.organisations.is_empty() && !administrator.organisations.contains(&uuid) {
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(uuid).await?;

	Ok(Json(
		backend
			.get_claims_by_organisation_id(uuid)
			.await?
			.into_iter()
			.map(|models::OrganisationClaim { claim_name, .. }| claim_name)
			.collect(),
	))
}

pub async fn get_claim_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, claim_name)): extract::Path<(Uuid, String)>,
) -> JsonResult<ClaimParameters> {
	if !administrator.organisations.is_empty()
		&& !administrator.organisations.contains(&organisation_id)
	{
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(organisation_id).await?;

	let r#type = backend
		.get_claim_type_by_organisation_id_and_claim_name(organisation_id, claim_name.as_str())
		.await?
		.ok_or_else(|| WrappedApiError::item_not_found(Some(Cow::Borrowed("Claim not found"))))?
		.map(Into::into);
	Ok(Json(ClaimParameters { r#type }))
}

pub async fn put_claim_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, claim_name)): extract::Path<(Uuid, String)>,
	body: Option<extract::Json<ClaimParameters>>,
) -> ApiResult<()> {
	if !administrator.organisations.is_empty()
		&& !administrator.organisations.contains(&organisation_id)
	{
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(organisation_id).await?;
	validate_url_parameter(claim_name.as_str())?;

	if RESTRICTED_CLAIM_NAMES.contains(&claim_name.as_str()) {
		return Err(WrappedApiError::bad_name(Some(Cow::Borrowed("Name is restricted"))));
	}

	if backend.claim_exists(organisation_id, claim_name.as_str()).await? {
		return Err(WrappedApiError::item_already_exists(None));
	}

	backend
		.new_claim(
			organisation_id,
			claim_name.as_str(),
			body.and_then(|x| x.0.r#type).map(Into::into),
		)
		.await?;
	backend
		.create_destination(organisation_id, claim_name.as_str(), models::ClaimDestination::IdToken)
		.await?;
	backend
		.create_destination(
			organisation_id,
			claim_name.as_str(),
			models::ClaimDestination::UserInfo,
		)
		.await?;
	Ok(())
}

pub async fn delete_claim_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((uuid, claim_name)): extract::Path<(Uuid, String)>,
) -> ApiResult<()> {
	if !administrator.organisations.is_empty() && !administrator.organisations.contains(&uuid) {
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(uuid).await?;
	backend.validate_claim_exists(uuid, claim_name.as_str()).await?;

	backend.delete_claim(uuid, claim_name.as_str()).await?;
	Ok(())
}

pub async fn get_destinations_for_claims_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, claim_name)): extract::Path<(Uuid, String)>,
) -> JsonResult<Vec<ClaimDestination>> {
	if !administrator.organisations.is_empty()
		&& !administrator.organisations.contains(&organisation_id)
	{
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(organisation_id).await?;
	backend.validate_claim_exists(organisation_id, claim_name.as_str()).await?;

	Ok(Json(
		backend
			.get_destinations_by_claim_name(organisation_id, claim_name.as_str())
			.await?
			.into_iter()
			.map(Into::into)
			.collect(),
	))
}

async fn put_destination_on_claim_on_organisation(
	administrator: Administrator,
	backend: &Backend,
	organisation_id: Uuid,
	claim_name: &str,
	destination: ClaimDestination,
) -> ApiResult<()> {
	if !administrator.organisations.is_empty()
		&& !administrator.organisations.contains(&organisation_id)
	{
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(organisation_id).await?;
	backend.validate_claim_exists(organisation_id, claim_name).await?;

	if backend.destination_exists_for_claim(organisation_id, claim_name, destination.into()).await?
	{
		return Err(WrappedApiError::item_already_exists(Some(Cow::Borrowed(
			"Destination already exists for this claim",
		))));
	}

	backend.create_destination(organisation_id, claim_name, destination.into()).await?;
	Ok(())
}

async fn delete_destination_on_claim_on_organisation(
	administrator: Administrator,
	backend: &Backend,
	organisation_id: Uuid,
	claim_name: &str,
	destination: ClaimDestination,
) -> ApiResult<()> {
	if !administrator.organisations.is_empty()
		&& !administrator.organisations.contains(&organisation_id)
	{
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(organisation_id).await?;
	backend.validate_claim_exists(organisation_id, claim_name).await?;

	if !backend
		.destination_exists_for_claim(organisation_id, claim_name, destination.into())
		.await?
	{
		return Err(WrappedApiError::item_not_found(Some(Cow::Borrowed(
			"Destination does not exist for this claim",
		))));
	}

	backend.delete_destination(organisation_id, claim_name, destination.into()).await?;
	Ok(())
}

pub async fn post_dest_id_token_on_claim_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, claim_name)): extract::Path<(Uuid, String)>,
) -> ApiResult<()> {
	put_destination_on_claim_on_organisation(
		administrator,
		&backend.0,
		organisation_id,
		claim_name.as_str(),
		ClaimDestination::IdToken,
	)
	.await
}

pub async fn delete_dest_id_token_on_claim_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, claim_name)): extract::Path<(Uuid, String)>,
) -> ApiResult<()> {
	delete_destination_on_claim_on_organisation(
		administrator,
		&backend.0,
		organisation_id,
		claim_name.as_str(),
		ClaimDestination::IdToken,
	)
	.await
}

pub async fn post_dest_access_token_on_claim_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, claim_name)): extract::Path<(Uuid, String)>,
) -> ApiResult<()> {
	put_destination_on_claim_on_organisation(
		administrator,
		&backend.0,
		organisation_id,
		claim_name.as_str(),
		ClaimDestination::AccessToken,
	)
	.await
}

pub async fn delete_dest_access_token_on_claim_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, claim_name)): extract::Path<(Uuid, String)>,
) -> ApiResult<()> {
	delete_destination_on_claim_on_organisation(
		administrator,
		&backend.0,
		organisation_id,
		claim_name.as_str(),
		ClaimDestination::AccessToken,
	)
	.await
}

pub async fn post_dest_user_info_on_claim_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, claim_name)): extract::Path<(Uuid, String)>,
) -> ApiResult<()> {
	put_destination_on_claim_on_organisation(
		administrator,
		&backend.0,
		organisation_id,
		claim_name.as_str(),
		ClaimDestination::UserInfo,
	)
	.await
}

pub async fn delete_dest_user_info_on_claim_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, claim_name)): extract::Path<(Uuid, String)>,
) -> ApiResult<()> {
	delete_destination_on_claim_on_organisation(
		administrator,
		&backend.0,
		organisation_id,
		claim_name.as_str(),
		ClaimDestination::UserInfo,
	)
	.await
}

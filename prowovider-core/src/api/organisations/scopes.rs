/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use std::borrow::Cow;

use axum::{extract, Json};
use uuid::Uuid;

use crate::{
	api::{
		admin_auth::invalid_auth, error::WrappedApiError, validate_url_parameter, ApiResult,
		JsonResult,
	},
	models::{
		self, Backend, OrganisationClaimModel, OrganisationModel, OrganisationScopeClaimMapModel,
		OrganisationScopeModel,
	},
	settings::Administrator,
};

pub async fn list_scopes_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path(uuid): extract::Path<Uuid>,
) -> JsonResult<Vec<String>> {
	if !administrator.organisations.is_empty() && !administrator.organisations.contains(&uuid) {
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(uuid).await?;
	Ok(Json(
		backend
			.get_scopes_by_organisation_id(uuid)
			.await?
			.into_iter()
			.map(|models::OrganisationScope { scope, .. }| scope)
			.collect(),
	))
}

pub async fn get_scope_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((uuid, scope_name)): extract::Path<(Uuid, String)>,
) -> ApiResult<()> {
	if !administrator.organisations.is_empty() && !administrator.organisations.contains(&uuid) {
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(uuid).await?;
	backend.validate_scope_exists(uuid, scope_name.as_str()).await?;

	Ok(())
}

pub async fn put_scope_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((uuid, scope_name)): extract::Path<(Uuid, String)>,
) -> ApiResult<()> {
	if !administrator.organisations.is_empty() && !administrator.organisations.contains(&uuid) {
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(uuid).await?;
	validate_url_parameter(scope_name.as_str())?;

	if backend.scope_exists(uuid, scope_name.as_str()).await? {
		return Err(WrappedApiError::item_already_exists(None));
	}

	backend.new_scope(uuid, scope_name.as_str()).await?;
	Ok(())
}

pub async fn delete_scope_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((uuid, scope_name)): extract::Path<(Uuid, String)>,
) -> ApiResult<()> {
	if !administrator.organisations.is_empty() && !administrator.organisations.contains(&uuid) {
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(uuid).await?;
	backend.validate_scope_exists(uuid, scope_name.as_str()).await?;

	backend.delete_scope(uuid, scope_name.as_str()).await?;
	Ok(())
}

pub async fn get_implied_claims_for_scope_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, scope_name)): extract::Path<(Uuid, String)>,
) -> JsonResult<Vec<String>> {
	if !administrator.organisations.is_empty()
		&& !administrator.organisations.contains(&organisation_id)
	{
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(organisation_id).await?;
	backend.validate_scope_exists(organisation_id, scope_name.as_str()).await?;

	Ok(Json(backend.get_mapped_claims_by_scope(organisation_id, scope_name.as_str()).await?))
}

pub async fn post_implied_claim_for_scope_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, scope_name, claim_name)): extract::Path<(Uuid, String, String)>,
) -> ApiResult<()> {
	if !administrator.organisations.is_empty()
		&& !administrator.organisations.contains(&organisation_id)
	{
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(organisation_id).await?;
	backend.validate_scope_exists(organisation_id, scope_name.as_str()).await?;
	backend.validate_claim_exists(organisation_id, claim_name.as_str()).await?;

	if backend
		.scope_claim_mapping_exists(organisation_id, scope_name.as_str(), claim_name.as_str())
		.await?
	{
		return Err(WrappedApiError::item_already_exists(Some(Cow::Borrowed(
			"Claim is already implied for scope",
		))));
	}

	backend
		.create_scope_claim_mapping(organisation_id, scope_name.as_str(), claim_name.as_str())
		.await?;
	Ok(())
}

pub async fn delete_implied_claim_for_scope_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, scope_name, claim_name)): extract::Path<(Uuid, String, String)>,
) -> ApiResult<()> {
	if !administrator.organisations.is_empty()
		&& !administrator.organisations.contains(&organisation_id)
	{
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(organisation_id).await?;
	backend.validate_scope_exists(organisation_id, scope_name.as_str()).await?;
	backend.validate_claim_exists(organisation_id, claim_name.as_str()).await?;

	if !backend
		.scope_claim_mapping_exists(organisation_id, scope_name.as_str(), claim_name.as_str())
		.await?
	{
		return Err(WrappedApiError::item_not_found(Some(Cow::Borrowed(
			"Claim is not implied for scope",
		))));
	}

	backend
		.delete_scope_claim_mapping(organisation_id, scope_name.as_str(), claim_name.as_str())
		.await?;
	Ok(())
}

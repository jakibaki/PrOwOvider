/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use std::{borrow::Cow, str::FromStr};

use axum::{extract, Json};
use prowovider_api::{
	ClientList, ClientRegistrationParameters, PreferredKeyUpdate, RedirectUriQuery,
	RegisteredClient,
};
use uuid::Uuid;

use crate::{
	api::{admin_auth::invalid_auth, error::WrappedApiError, ApiResult, JsonResult},
	models::{
		Backend, ClientRedirectUriModel, ClientType, OrganisationClientModel, OrganisationModel,
	},
	settings::Administrator,
};

pub async fn list_clients_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path(uuid): extract::Path<Uuid>,
) -> JsonResult<ClientList> {
	if !administrator.organisations.is_empty() && !administrator.organisations.contains(&uuid) {
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(uuid).await?;

	Ok(Json(backend.get_clients_by_organisation_id(uuid).await?.into_iter().fold(
		ClientList::default(),
		|mut list, client| {
			match client.client_type {
				ClientType::Confidential => list.confidential.push(client.id),
				ClientType::Public => list.public.push(client.id),
			}
			list
		},
	)))
}

pub async fn post_client_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path(uuid): extract::Path<Uuid>,
	extract::Json(params): extract::Json<ClientRegistrationParameters>,
) -> JsonResult<RegisteredClient> {
	if !administrator.organisations.is_empty() && !administrator.organisations.contains(&uuid) {
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(uuid).await?;

	let registered_client = backend
		.new_client(
			uuid,
			params.name.as_str(),
			match params.public {
				true => ClientType::Public,
				false => ClientType::Confidential,
			},
			params.preferred_key.as_deref(),
		)
		.await?;

	Ok(Json(registered_client.into()))
}

pub async fn get_client_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, client_id)): extract::Path<(Uuid, Uuid)>,
) -> ApiResult<String> {
	if !administrator.organisations.is_empty()
		&& !administrator.organisations.contains(&organisation_id)
	{
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(organisation_id).await?;

	backend
		.get_name_of_client(client_id, organisation_id)
		.await?
		.ok_or_else(super::client_not_found)
}

pub async fn delete_client_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, client_id)): extract::Path<(Uuid, Uuid)>,
) -> ApiResult<()> {
	if !administrator.organisations.is_empty()
		&& !administrator.organisations.contains(&organisation_id)
	{
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(organisation_id).await?;
	backend.validate_client_exists(organisation_id, client_id).await?;

	backend.delete_client(client_id, organisation_id).await?;

	Ok(())
}

async fn validate_redirect_uri(uri: &str) -> Result<(), WrappedApiError> {
	match url::Url::from_str(uri) {
		Ok(parsed) if !parsed.cannot_be_a_base() => Ok(()),
		_ => Err(WrappedApiError::bad_request(Some(Cow::Borrowed("Failed to parse redirect_uri")))),
	}
}

pub async fn list_redirect_uris_on_client_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, client_id)): extract::Path<(Uuid, Uuid)>,
) -> JsonResult<Vec<String>> {
	if !administrator.organisations.is_empty()
		&& !administrator.organisations.contains(&organisation_id)
	{
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(organisation_id).await?;
	backend.validate_client_exists(organisation_id, client_id).await?;

	Ok(Json(backend.get_redirect_uris_by_client_id(client_id).await?))
}

pub async fn put_redirect_uri_on_client_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, client_id)): extract::Path<(Uuid, Uuid)>,
	extract::Query(query): extract::Query<RedirectUriQuery<'_>>,
) -> ApiResult<()> {
	if !administrator.organisations.is_empty()
		&& !administrator.organisations.contains(&organisation_id)
	{
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(organisation_id).await?;
	backend.validate_client_exists(organisation_id, client_id).await?;
	validate_redirect_uri(query.uri.as_ref()).await?;

	if backend.redirect_uri_exists(client_id, query.uri.as_ref()).await? {
		return Err(WrappedApiError::item_already_exists(Some(Cow::Borrowed(
			"Redirect URI already exists",
		))));
	}

	backend.create_redirect_uri(client_id, query.uri.as_ref()).await?;
	Ok(())
}

pub async fn delete_redirect_uri_on_client_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, client_id)): extract::Path<(Uuid, Uuid)>,
	extract::Query(query): extract::Query<RedirectUriQuery<'_>>,
) -> ApiResult<()> {
	if !administrator.organisations.is_empty()
		&& !administrator.organisations.contains(&organisation_id)
	{
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(organisation_id).await?;
	backend.validate_client_exists(organisation_id, client_id).await?;
	validate_redirect_uri(query.uri.as_ref()).await?;

	backend.delete_redirect_uri(client_id, query.uri.as_ref()).await?;
	Ok(())
}

pub async fn get_preferred_key_id_on_client(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, client_id)): extract::Path<(Uuid, Uuid)>,
) -> ApiResult<String> {
	if !administrator.organisations.is_empty()
		&& !administrator.organisations.contains(&organisation_id)
	{
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(organisation_id).await?;

	match backend.get_client_by_id(client_id, organisation_id).await? {
		Some(client) => client.preferred_key.ok_or_else(|| {
			WrappedApiError::item_not_found(Some(Cow::Borrowed("No preferred key")))
		}),
		None => Err(super::client_not_found()),
	}
}

pub async fn put_preferred_key_id_on_client(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, client_id)): extract::Path<(Uuid, Uuid)>,
	extract::Query(query): extract::Query<PreferredKeyUpdate<'_>>,
) -> ApiResult<()> {
	if !administrator.organisations.is_empty()
		&& !administrator.organisations.contains(&organisation_id)
	{
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(organisation_id).await?;
	backend.validate_client_exists(organisation_id, client_id).await?;

	backend
		.set_preferred_key_for_client(client_id, organisation_id, Some(query.id.as_ref()))
		.await?;
	Ok(())
}

pub async fn delete_preferred_key_id_on_client(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, client_id)): extract::Path<(Uuid, Uuid)>,
) -> ApiResult<()> {
	if !administrator.organisations.is_empty()
		&& !administrator.organisations.contains(&organisation_id)
	{
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(organisation_id).await?;
	backend.validate_client_exists(organisation_id, client_id).await?;

	backend.set_preferred_key_for_client(client_id, organisation_id, None).await?;
	Ok(())
}

/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use std::borrow::Cow;

use axum::{extract, Json};
use uuid::Uuid;

use crate::{
	api::{admin_auth::invalid_auth, error::WrappedApiError, ApiResult, JsonResult},
	models::{
		self, Backend, OrganisationModel, OrganisationScopeModel, OrganisationUserModel,
		OrganisationUserScopeModel,
	},
	settings::Administrator,
};

pub async fn get_scopes_on_user_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, user_id)): extract::Path<(Uuid, Uuid)>,
) -> JsonResult<Vec<String>> {
	if !administrator.organisations.is_empty()
		&& !administrator.organisations.contains(&organisation_id)
	{
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(organisation_id).await?;
	backend.validate_user_exists(organisation_id, user_id).await?;

	Ok(Json(
		backend
			.get_scopes_by_user_id(organisation_id, user_id)
			.await?
			.into_iter()
			.map(|models::OrganisationUserScope { scope, .. }| scope)
			.collect(),
	))
}

pub async fn get_scope_on_user_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, user_id, scope_name)): extract::Path<(Uuid, Uuid, String)>,
) -> JsonResult<bool> {
	if !administrator.organisations.is_empty()
		&& !administrator.organisations.contains(&organisation_id)
	{
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(organisation_id).await?;
	backend.validate_user_exists(organisation_id, user_id).await?;

	Ok(Json(backend.user_scope_exists(organisation_id, user_id, scope_name.as_str()).await?))
}

pub async fn allow_scope_on_user_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, user_id, scope_name)): extract::Path<(Uuid, Uuid, String)>,
) -> ApiResult<()> {
	if !administrator.organisations.is_empty()
		&& !administrator.organisations.contains(&organisation_id)
	{
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(organisation_id).await?;
	backend.validate_user_exists(organisation_id, user_id).await?;
	backend.validate_scope_exists(organisation_id, scope_name.as_str()).await?;

	if backend.user_scope_exists(organisation_id, user_id, scope_name.as_str()).await? {
		return Err(WrappedApiError::item_already_exists(Some(Cow::Borrowed(
			"User can already use scope",
		))));
	}

	backend.new_scope_on_user(organisation_id, user_id, scope_name.as_str()).await?;
	Ok(())
}

pub async fn delete_scope_on_user_on_organisations(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, user_id, scope_name)): extract::Path<(Uuid, Uuid, String)>,
) -> ApiResult<()> {
	if !administrator.organisations.is_empty()
		&& !administrator.organisations.contains(&organisation_id)
	{
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(organisation_id).await?;
	backend.validate_user_exists(organisation_id, user_id).await?;

	if !backend.user_scope_exists(organisation_id, user_id, scope_name.as_str()).await? {
		return Err(WrappedApiError::item_not_found(Some(Cow::Borrowed("Scope not found"))));
	}

	backend.delete_scope_on_user(organisation_id, user_id, scope_name.as_str()).await?;
	Ok(())
}

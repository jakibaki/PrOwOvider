/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use std::borrow::Cow;

use axum::{extract, Json};
use uuid::Uuid;

use crate::{
	api::{admin_auth::invalid_auth, error::WrappedApiError, ApiResult, JsonResult},
	models::{
		self, Backend, OrganisationClaimModel, OrganisationModel, OrganisationUserClaimModel,
		OrganisationUserModel,
	},
	settings::Administrator,
};

pub async fn get_claims_on_user_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, user_id)): extract::Path<(Uuid, Uuid)>,
) -> JsonResult<serde_json::Value> {
	if !administrator.organisations.is_empty()
		&& !administrator.organisations.contains(&organisation_id)
	{
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(organisation_id).await?;
	backend.validate_user_exists(organisation_id, user_id).await?;

	let map = backend.get_claims_by_user_id(organisation_id, user_id).await?.into_iter().fold(
		serde_json::Map::<String, serde_json::Value>::new(),
		|mut lhs, rhs| {
			lhs.insert(rhs.claim_name, rhs.claim_value);
			lhs
		},
	);

	Ok(Json(serde_json::Value::Object(map)))
}

pub async fn get_claim_on_user_on_organisation(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, user_id, claim_name)): extract::Path<(Uuid, Uuid, String)>,
) -> JsonResult<serde_json::Value> {
	if !administrator.organisations.is_empty()
		&& !administrator.organisations.contains(&organisation_id)
	{
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(organisation_id).await?;
	backend.validate_user_exists(organisation_id, user_id).await?;

	let models::OrganisationUserClaim { claim_value, .. } = backend
		.get_claim_by_user_id_and_claim_name(organisation_id, user_id, claim_name.as_str())
		.await?
		.ok_or_else(|| WrappedApiError::item_not_found(Some(Cow::Borrowed("Claim not found"))))?;

	Ok(Json(claim_value))
}

pub async fn upsert_claim_on_user_on_organisations(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, user_id, claim_name)): extract::Path<(Uuid, Uuid, String)>,
	extract::Json(value): extract::Json<serde_json::Value>,
) -> ApiResult<()> {
	if !administrator.organisations.is_empty()
		&& !administrator.organisations.contains(&organisation_id)
	{
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(organisation_id).await?;
	backend.validate_user_exists(organisation_id, user_id).await?;

	let claim_type = match backend
		.get_claim_type_by_organisation_id_and_claim_name(organisation_id, claim_name.as_str())
		.await?
	{
		Some(claim_type) => Ok(claim_type),
		None => Err(WrappedApiError::item_not_found(Some(Cow::Borrowed("Claim not found")))),
	}?;

	let typed_ok = match claim_type {
		Some(claim_type) => match &value {
			serde_json::Value::Null => false,
			serde_json::Value::Bool(_) => claim_type == models::ClaimType::Bool,
			serde_json::Value::Number(_) => claim_type == models::ClaimType::Number,
			serde_json::Value::String(_) => claim_type == models::ClaimType::String,
			serde_json::Value::Array(_) => claim_type == models::ClaimType::Array,
			serde_json::Value::Object(_) => claim_type == models::ClaimType::Object,
		},
		None => true,
	};

	if !typed_ok {
		return Err(WrappedApiError::claim_typed_wrong(None));
	}

	backend.upsert_claim_on_user(organisation_id, user_id, claim_name.as_str(), &value).await?;
	Ok(())
}

pub async fn delete_claim_on_user_on_organisations(
	administrator: Administrator,
	backend: extract::Extension<Backend>,
	extract::Path((organisation_id, user_id, claim_name)): extract::Path<(Uuid, Uuid, String)>,
) -> ApiResult<()> {
	if !administrator.organisations.is_empty()
		&& !administrator.organisations.contains(&organisation_id)
	{
		return Err(invalid_auth());
	}

	backend.validate_organisation_exists(organisation_id).await?;
	backend.validate_user_exists(organisation_id, user_id).await?;
	backend.validate_claim_exists(organisation_id, claim_name.as_str()).await?;

	if !backend.user_claim_exists(organisation_id, user_id, claim_name.as_str()).await? {
		return Err(WrappedApiError::item_not_found(Some(Cow::Borrowed("Claim not found"))));
	}

	backend.delete_claim_on_user(organisation_id, user_id, claim_name.as_str()).await?;
	Ok(())
}

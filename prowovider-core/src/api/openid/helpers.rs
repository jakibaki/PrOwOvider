/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use std::{borrow::Cow, collections::HashMap, fmt::Display, str::FromStr};

use axum::{body::boxed, http::HeaderValue, response::IntoResponse};
use hyper::StatusCode;
use openid_types::endpoints::{
	errors::{
		token::{TokenError, INVALID_CLIENT, INVALID_GRANT},
		SERVER_ERROR,
	},
	requests::{CodeCallback, HybridCallback, ImplicitCallback},
};
use serde::{Deserialize, Serialize};
use url::Url;
use uuid::Uuid;

use crate::{
	api::error::WrappedApiError,
	models::{
		postgres::PostgresModelError, Backend, BackendError, ClientRedirectUriModel, ModelError,
	},
};

pub struct Redirect<T: Serialize>(pub Url, pub T);

impl<T: Serialize> IntoResponse for Redirect<T> {
	fn into_response(mut self) -> axum::response::Response {
		let original_query: Option<HashMap<String, String>> = match self.0.query() {
			Some(query) => match serde_urlencoded::from_str(query) {
				Ok(map) => Some(map),
				Err(why) => {
					tracing::error!("Failed to deserialize redirect_uri query parameters: {}", why);
					return WrappedApiError::internal_error(Some(Cow::Borrowed(
						"Failed to deserialize redirect_uri query parameters",
					)))
					.into_response();
				}
			},
			None => None,
		};

		let serialized = match serde_urlencoded::to_string(self.1) {
			Ok(serialized) => serialized,
			Err(why) => {
				tracing::error!("Failed to serialize redirect parameter: {}", why);
				return WrappedApiError::internal_error(Some(Cow::Borrowed(
					"Failed to serialize query parameters",
				)))
				.into_response();
			}
		};

		let query = match original_query {
			Some(original_query) => {
				let deserialized_new_params: HashMap<String, String> =
					match serde_urlencoded::from_str(serialized.as_str()) {
						Ok(params) => params,
						Err(why) => {
							tracing::error!(
								"Failed to deserialize serialized redirect parameter: {}",
								why
							);
							return WrappedApiError::internal_error(Some(Cow::Borrowed(
								"Failed to deserialize serialized query parameters",
							)))
							.into_response();
						}
					};

				let mut query = original_query;
				for (k, v) in deserialized_new_params {
					query.insert(k, v);
				}

				match serde_urlencoded::to_string(query) {
					Ok(serialized) => serialized,
					Err(why) => {
						tracing::error!(
							"Failed to serialize finalised redirect parameter: {}",
							why
						);
						return WrappedApiError::internal_error(Some(Cow::Borrowed(
							"Failed to serialize finalised query parameters",
						)))
						.into_response();
					}
				}
			}
			None => serialized,
		};

		self.0.set_query(Some(query.as_str()));

		let uri = match axum::http::HeaderValue::from_str(self.0.as_str()) {
			Ok(value) => value,
			Err(_) => {
				tracing::error!("Redirect URI {} contains non-visible ASCII, returning error on authentication request", self.0);
				return WrappedApiError::bad_request(Some(Cow::Borrowed(
					"redirect_uri contains non-visible ASCII",
				)))
				.into_response();
			}
		};

		match axum::response::Response::builder()
			.status(axum::http::StatusCode::FOUND)
			.header(axum::http::header::LOCATION, uri)
			.body(axum::body::boxed(axum::body::Empty::new()))
		{
			Ok(response) => response,
			Err(why) => {
				tracing::error!("Failed to build a redirect response: {}", why);
				WrappedApiError::internal_error(Some(Cow::Borrowed(
					"Failed to build redirect response",
				)))
				.into_response()
			}
		}
	}
}

#[derive(Debug, thiserror::Error)]
pub enum SubjectParseError {
	#[error("Subject missing seperator")]
	MissingSeperator,
	#[error("Failed to parse organisation: {0}")]
	OrganisationParse(uuid::Error),
	#[error("Failed to parse user: {0}")]
	UserParse(uuid::Error),
}

#[derive(Debug, Deserialize, Serialize, PartialEq, Eq, Clone, Copy)]
pub struct Subject {
	pub organisation: Uuid,
	pub user: Uuid,
}

impl Subject {
	pub fn new(organisation: Uuid, user: Uuid) -> Self {
		Self { organisation, user }
	}
}

impl FromStr for Subject {
	type Err = SubjectParseError;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		let (org, uid) = s.split_once(':').ok_or(SubjectParseError::MissingSeperator)?;

		Ok(Subject {
			organisation: Uuid::from_str(org).map_err(SubjectParseError::OrganisationParse)?,
			user: Uuid::from_str(uid).map_err(SubjectParseError::UserParse)?,
		})
	}
}

impl Display for Subject {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.write_fmt(format_args!("{}:{}", self.organisation, self.user))
	}
}

#[derive(Debug, Serialize)]
#[serde(untagged)]
pub enum CallbackResponse {
	Code(CodeCallback),
	Implicit(ImplicitCallback),
	Hybrid(HybridCallback),
	None { state: Option<String> },
}

pub(crate) async fn parse_redirect_uri(
	backend: &Backend,
	client_id: Uuid,
	uri: &str,
) -> Result<Url, WrappedApiError> {
	if !backend.redirect_uri_exists(client_id, uri).await? {
		return Err(WrappedApiError::bad_request(Some(Cow::Borrowed("Invalid redirect_uri"))));
	}

	uri.parse::<Url>().map_err(|why| {
		tracing::debug!("Failed to parse requested redirect_uri parameter: {:?}", why);
		WrappedApiError::bad_request(Some(Cow::Borrowed("Invalid redirect_uri")))
	})
}

pub(crate) fn bad_client_id() -> WrappedApiError {
	WrappedApiError::bad_request(Some(Cow::Borrowed("Invalid client ID")))
}

pub(crate) fn bad_client_id_token_endpoint() -> TokenEndpointError {
	bad_client_auth("Invalid client ID")
}

pub(crate) fn bad_client_auth(message: &'static str) -> TokenEndpointError {
	TokenEndpointError(TokenError::new(
		Cow::Borrowed(INVALID_CLIENT),
		Some(Cow::Borrowed(message)),
		None,
	))
}

pub(crate) fn internal_error(message: &'static str) -> TokenEndpointError {
	TokenEndpointError(TokenError::new(
		Cow::Borrowed(SERVER_ERROR),
		Some(Cow::Borrowed(message)),
		None,
	))
}

pub(crate) fn invalid_grant(message: Option<&'static str>) -> TokenEndpointError {
	TokenEndpointError(TokenError::new(
		Cow::Borrowed(INVALID_GRANT),
		message.map(Cow::Borrowed),
		None,
	))
}

#[derive(Debug)]
pub struct TokenEndpointError(pub TokenError);

impl IntoResponse for TokenEndpointError {
	fn into_response(self) -> axum::response::Response {
		fn map_err_to_res<T: Into<WrappedApiError>>(t: T) -> axum::response::Response {
			let error: WrappedApiError = t.into();
			let (parts, body) = error.into_response().into_parts();
			axum::http::Response::from_parts(parts, boxed(body))
		}

		match serde_json::to_vec(&self.0).map_err(map_err_to_res) {
			Ok(body) => {
				let builder = axum::http::Response::builder();

				let builder = if self.0.error.as_ref() == INVALID_CLIENT {
					builder.status(StatusCode::UNAUTHORIZED).header(
						axum::http::header::WWW_AUTHENTICATE,
						HeaderValue::from_static(
							r#"Basic realm="Client credentials needed", charset="UTF-8""#,
						),
					)
				} else {
					builder.status(StatusCode::BAD_REQUEST)
				};

				match builder.body(boxed(axum::body::Full::from(body))).map_err(map_err_to_res) {
					Ok(res) => res,
					Err(err) => err,
				}
			}
			Err(err) => err,
		}
	}
}

impl From<sqlx::Error> for TokenEndpointError {
	fn from(err: sqlx::Error) -> Self {
		tracing::error!("DB Error: {}", err);
		internal_error("Database error")
	}
}

impl From<jsonwebtoken::errors::Error> for TokenEndpointError {
	fn from(err: jsonwebtoken::errors::Error) -> Self {
		tracing::error!("JWT Error: {}", err);
		internal_error("Failed to serialize ID token")
	}
}

impl From<PostgresModelError> for TokenEndpointError {
	fn from(err: PostgresModelError) -> Self {
		match err {
			PostgresModelError::Sql(why) => why.into(),
			PostgresModelError::Bcrypt(bcrypt) => {
				tracing::error!(
					"Bcrypt Error in Token callback (this is a bug, please report this): {}",
					bcrypt
				);
				internal_error("Bcrypt error")
			}
		}
	}
}

impl From<ModelError<BackendError>> for TokenEndpointError {
	fn from(err: ModelError<BackendError>) -> Self {
		match err {
			ModelError::Unsupported => internal_error("Storage backend does not support needed operation (this is a bug, plaese report this)"),
			ModelError::Other(why) => match why {
				BackendError::Postgres(why) => why.into(),
			}
		}
	}
}

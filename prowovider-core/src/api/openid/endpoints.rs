/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use std::{borrow::Cow, sync::Arc};

use axum::{
	extract,
	http::HeaderValue,
	response::{Html, IntoResponse},
	Json,
};
use hyper::StatusCode;
use openid_provider::axum::{BasicAuth, BasicAuthError};
use openid_types::{
	endpoints::{
		errors::{
			authentication::{
				AuthenticationErrorResponse, INTERACTION_REQUIRED, UNSUPPORTED_RESPONSE_TYPE,
			},
			token::{TokenError, UNSUPPORTED_GRANT_TYPE},
			INVALID_REQUEST, INVALID_SCOPE, SERVER_ERROR,
		},
		requests::{
			AuthenticationRequest, BaseTokenRequest, CodeCallback, CodeTokenRequest, GrantType,
			PromptType, RefreshTokenRequest, ResponseType,
		},
		responses::TokenResponse,
	},
	token::{CodeTokenClaims, TokenClaims},
};
use prowovider_api::{JsonWebKeySet, LoginRequest};
use serde::{Deserialize, Serialize};
use serde_json::Map;
use time::OffsetDateTime;
use uuid::Uuid;

use crate::{
	api::{
		error::WrappedApiError,
		openid::helpers::internal_error,
		tokens::{
			LoginState, OpenIDAccessToken, OpenIDAuthorizationCode, OpenIDRefreshToken, Token,
		},
		ApiResult, JsonResult,
	},
	models::{
		self, Backend, ClientType, OrganisationClient, OrganisationClientModel, OrganisationModel,
		OrganisationScopeModel, OrganisationUserClaimModel, OrganisationUserModel,
		OrganisationUserScopeModel,
	},
	settings::{OpenIDConnectSettings, Settings},
};

use super::{
	helpers::{
		bad_client_auth, bad_client_id, bad_client_id_token_endpoint, invalid_grant,
		parse_redirect_uri, CallbackResponse, Redirect, Subject, TokenEndpointError,
	},
	html,
};

#[allow(clippy::too_many_arguments)]
async fn token_endpoint_build_token_response(
	subject: Subject,
	backend: Backend,
	scopes: Vec<String>,
	client: OrganisationClient,
	settings: &OpenIDConnectSettings,
	iss: String,
	auth_time: Option<OffsetDateTime>,
	nonce: Option<String>,
) -> Result<TokenResponse, TokenEndpointError> {
	let signing_key = settings
		.jwt
		.by_optional_id(client.preferred_key.as_deref())
		.ok_or_else(|| internal_error("Failed to find suitable key to fulfil request"))?;

	let access_token_user_claims = backend
		.get_claims_by_user_id_and_scopes_with_destination(
			subject.organisation,
			subject.user,
			scopes.as_slice(),
			models::ClaimDestination::AccessToken,
		)
		.await?;
	let access_token = OpenIDAccessToken::new(
		subject,
		scopes.clone(),
		client.id,
		access_token_user_claims,
		settings,
	);
	let encoded_access_token =
		Token::OpenIDAccessToken(Cow::Borrowed(&access_token)).encode(signing_key)?;

	let at_hash = match CodeTokenClaims::calculate_at_hash(
		encoded_access_token.as_str(),
		signing_key.algorithm,
	) {
		Some(at_hash) => at_hash,
		// Technically unreachable for us as our Algorithm enum for settings only contains valid algorithms, but we will handle it anyways
		None => {
			tracing::error!("Invalid algorithm used and unable to calulate 'at_hash' as a result");
			return Err(internal_error(
				"Calculating at_hash failed as server is using invalid algorithm",
			));
		}
	};

	let id_token_user_claims = backend
		.get_claims_by_user_id_and_scopes_with_destination(
			subject.organisation,
			subject.user,
			scopes.as_slice(),
			models::ClaimDestination::IdToken,
		)
		.await?;

	// Build and encode the OpenID Connect ID Token
	let now = OffsetDateTime::now_utc();
	let exp = now + settings.jwt.exp.id_token_expiry;
	let sub_string = subject.to_string();
	let id_token = Token::OpenIDToken(Cow::Owned(CodeTokenClaims {
		at_hash: Some(at_hash),
		base: TokenClaims {
			iss,
			sub: sub_string.clone(),
			aud: vec![client.id.to_string()],
			exp,
			iat: now,
			auth_time,
			nonce,
			acr: None,
			amr: None,
			azp: Some(sub_string),
		},
		extra: id_token_user_claims,
	}))
	.encode(signing_key)?;

	let refresh_token = OpenIDRefreshToken::new(access_token, client.id, settings);
	let encoded_refresh_token =
		Token::OpenIDRefreshToken(Cow::Borrowed(&refresh_token)).encode(signing_key)?;

	Ok(TokenResponse {
		access_token: encoded_access_token,
		id_token,
		token_type: openid_types::endpoints::TokenType::Bearer,
		expires_in: Some(exp - now),
		refresh_token: Some(encoded_refresh_token),
		scope: scopes,
	})
}

fn token_endpoint_validate_client_auth(
	client: &OrganisationClient,
	form_client_secret: Option<&str>,
	client_auth: Result<BasicAuth, BasicAuthError>,
	form_client_id_parsed: Uuid,
) -> Result<(), TokenEndpointError> {
	// Here we authenticate the client
	// First we check if the client sent a client secret in the form
	if let Some(client_secret) = form_client_secret {
		// We check if the client also tried to authenticate via a header. If so we return an error
		if !matches!(client_auth, Err(BasicAuthError::BadAuthHeader)) {
			return Err(bad_client_auth(
				"Client sent authentication in both the form body and as a header",
			));
		}

		match client.client_type {
			ClientType::Confidential => {
				// If the client secret is not correct we return an error
				if client_secret != client.secret {
					return Err(bad_client_auth("Invalid client secret"));
				}
			}
			// If the client is a public client it should not be attempting to authenticate itself
			ClientType::Public => {
				return Err(bad_client_auth(
					"Sent basic auth credentials whilst using a public client",
				))
			}
		}
	// If not we then check the headers
	} else {
		match client_auth {
			// Basic auth header was succesfully extracted
			Ok(auth) => {
				match client.client_type {
					ClientType::Confidential => {
						// We parse the client ID from the auth header
						let auth_client_id_parsed = Uuid::parse_str(auth.0.as_str())
							.map_err(|_| bad_client_id_token_endpoint())?;
						// And ensure a secret is provided, the specification allows for none to be sent if the secret is expected to be empty but we will never distribute empty secrets to a confidential client
						let secret = auth
							.1
							.as_deref()
							.ok_or_else(|| bad_client_auth("Missing client secret"))?;

						// If the client ID from the auth header is not the same as in in the request we return an error
						if auth_client_id_parsed != form_client_id_parsed {
							return Err(bad_client_auth(
								"Form client ID is different than Basic auth header",
							));
						// If the client secret is not correct we return an error
						} else if secret != client.secret {
							return Err(bad_client_auth("Invalid client secret"));
						}
					}
					// If the client is a public client it should not be attempting to authenticate itself
					ClientType::Public => {
						return Err(bad_client_auth(
							"Sent basic auth credentials whilst using a public client",
						))
					}
				}
			}
			// The auth header was not sent and the client is public which is ok
			Err(BasicAuthError::BadAuthHeader) if client.client_type == ClientType::Public => {}
			// Failed to parse the header
			Err(why) => {
				let error_message = match why {
					BasicAuthError::Base64(_) => "Invalid base64 encoding in Basic auth header",
					BasicAuthError::MoreThanOneColon => {
						"Basic auth header contains more than one colon"
					}
					BasicAuthError::BadAuthHeader => "Missing Basic auth",
					BasicAuthError::AuthHeaderInvalidUtf8 => {
						"Basic auth header contains invalid UTF-8"
					}
					BasicAuthError::BadUrlEncoding => "Failed to URL decode basic auth component",
					BasicAuthError::HeadersAlreadyExtracted => "Headers already extracted",
				};

				return Err(bad_client_auth(error_message));
			}
		}
	}

	Ok(())
}

pub async fn token_endpoint_auth_code_handler(
	raw_form: openid_provider::axum::RawForm,
	organisation_id: Uuid,
	client_auth: Result<BasicAuth, BasicAuthError>,
	backend: Backend,
	settings: &OpenIDConnectSettings,
) -> Result<Json<TokenResponse>, TokenEndpointError> {
	let form: CodeTokenRequest = match serde_urlencoded::from_bytes(&raw_form.0) {
		Ok(form) => form,
		Err(_) => {
			return Err(TokenEndpointError(TokenError::new(
				Cow::Borrowed(INVALID_REQUEST),
				None,
				None,
			)));
		}
	};

	let form_client_id_parsed = Uuid::parse_str(form.extras.client_id.as_str())
		.map_err(|_| bad_client_id_token_endpoint())?;
	let client = backend
		.get_client_by_id(form_client_id_parsed, organisation_id)
		.await?
		.ok_or_else(bad_client_id_token_endpoint)?;

	token_endpoint_validate_client_auth(
		&client,
		form.extras.client_secret.as_deref(),
		client_auth,
		form_client_id_parsed,
	)?;

	// Decode the Authorization Code
	let state =
		match Token::decode_openid_authorization_code(form.extras.code.as_str(), &settings.jwt) {
			Some(Ok(state)) => state,
			_ => {
				return Err(invalid_grant(None));
			}
		};

	let subject = state.sub;

	let original_client_id = Uuid::parse_str(state.original_request.client_id.as_str())
		.map_err(|_| bad_client_id_token_endpoint())?;

	// Ensure that the client that initiated the authentication request is the same as the one making the token request
	if form_client_id_parsed != original_client_id {
		return Err(invalid_grant(Some("Client ID mismatch")));
	}

	// Ensure that the organisation ID remained the same to prevent using a token from organisation A with organisation B
	if subject.organisation != organisation_id {
		return Err(invalid_grant(Some("Organisation ID mismatch")));
	}

	// Verify that an ID Token is actually wanted
	if !state.scopes.iter().any(|x| x.as_str() == "openid") {
		return Err(TokenEndpointError(TokenError::new(
			Cow::Borrowed(INVALID_SCOPE),
			Some(Cow::Borrowed("'openid' scope was not requested")),
			None,
		)));
	}

	let iss = format!("{}/api/v1/openid/{}", settings.base_url, organisation_id);

	Ok(Json(
		token_endpoint_build_token_response(
			subject,
			backend,
			state.scopes,
			client,
			settings,
			iss,
			None,
			state.original_request.nonce,
		)
		.await?,
	))
}

pub async fn token_endpoint_refresh_token_handler(
	raw_form: openid_provider::axum::RawForm,
	organisation_id: Uuid,
	client_auth: Result<BasicAuth, BasicAuthError>,
	backend: Backend,
	settings: &OpenIDConnectSettings,
) -> Result<Json<TokenResponse>, TokenEndpointError> {
	let form: RefreshTokenRequest = match serde_urlencoded::from_bytes(&raw_form.0) {
		Ok(form) => form,
		Err(_) => {
			return Err(TokenEndpointError(TokenError::new(
				Cow::Borrowed(INVALID_REQUEST),
				None,
				None,
			)));
		}
	};

	let form_client_id_parsed = Uuid::parse_str(form.extras.client_id.as_str())
		.map_err(|_| bad_client_id_token_endpoint())?;
	let client = backend
		.get_client_by_id(form_client_id_parsed, organisation_id)
		.await?
		.ok_or_else(bad_client_id_token_endpoint)?;

	token_endpoint_validate_client_auth(
		&client,
		form.extras.client_secret.as_deref(),
		client_auth,
		form_client_id_parsed,
	)?;

	let decoded_refresh_token =
		match Token::decode_openid_refresh_token(form.extras.refresh_token.as_str(), &settings.jwt)
		{
			Some(Ok(token)) => token,
			_ => {
				return Err(TokenEndpointError(TokenError::new(
					Cow::Borrowed(INVALID_REQUEST),
					Some(Cow::Borrowed("Invalid Token")),
					None,
				)));
			}
		};

	let subject = decoded_refresh_token.original_claims.sub;

	let original_client_id = decoded_refresh_token.client_id;

	// Ensure that the client that initiated the authentication request is the same as the one making the token request
	if form_client_id_parsed != original_client_id {
		return Err(invalid_grant(Some("Client ID mismatch")));
	}

	// Ensure that the organisation ID remained the same to prevent using a token from organisation A with organisation B
	if subject.organisation != organisation_id {
		return Err(invalid_grant(Some("Organisation ID mismatch")));
	}

	let mut scopes = form.extras.scope.clone();
	scopes.dedup();

	for scope in &scopes {
		if scope.as_str() != "openid"
			&& !backend.scope_exists(organisation_id, scope.as_str()).await?
		{
			return Err(TokenEndpointError(TokenError::new(
				Cow::Borrowed(INVALID_SCOPE),
				None,
				None,
			)));
		} else if !decoded_refresh_token.original_claims.scopes.contains(scope) {
			return Err(TokenEndpointError(TokenError::new(
				Cow::Borrowed(INVALID_SCOPE),
				Some(Cow::Borrowed("Requesting new scopes with a refresh token is unsupported")),
				None,
			)));
		}
	}

	// Verify that an ID Token is actually wanted
	if !form.extras.scope.iter().any(|x| x.as_str() == "openid") {
		return Err(TokenEndpointError(TokenError::new(
			Cow::Borrowed(INVALID_SCOPE),
			Some(Cow::Borrowed("'openid' scope was not requested")),
			None,
		)));
	}

	let iss = format!("{}/api/v1/openid/{}", settings.base_url, organisation_id);

	if iss != decoded_refresh_token.original_claims.iss {
		return Err(TokenEndpointError(TokenError::new(
			Cow::Borrowed(SERVER_ERROR),
			Some(Cow::Borrowed("Cannot build a valid ID token as the `iss` value has changed")),
			None,
		)));
	}

	let auth_time = match decoded_refresh_token
		.original_claims
		.extra
		.get("auth_time")
		.and_then(|x| x.as_i64())
		.map(OffsetDateTime::from_unix_timestamp)
	{
		Some(Err(_)) => {
			return Err(TokenEndpointError(TokenError::new(
				Cow::Borrowed(SERVER_ERROR),
				Some(Cow::Borrowed("Failed to parse `auth_time` value")),
				None,
			)));
		}
		Some(Ok(x)) => Some(x),
		None => None,
	};

	Ok(Json(
		token_endpoint_build_token_response(
			subject,
			backend,
			scopes,
			client,
			settings,
			iss,
			auth_time,
			decoded_refresh_token
				.original_claims
				.extra
				.get("nonce")
				.and_then(|x| x.as_str())
				.map(String::from),
		)
		.await?,
	))
}

pub async fn token_endpoint(
	raw_form: openid_provider::axum::RawForm,
	organisation_id: extract::Path<Uuid>,
	client_auth: Result<BasicAuth, BasicAuthError>,
	backend: extract::Extension<Backend>,
	settings: extract::Extension<Arc<Settings>>,
) -> Result<Json<TokenResponse>, TokenEndpointError> {
	let form: BaseTokenRequest<()> = match serde_urlencoded::from_bytes(&raw_form.0) {
		Ok(form) => form,
		Err(_) => {
			return Err(TokenEndpointError(TokenError::new(
				Cow::Borrowed(INVALID_REQUEST),
				None,
				None,
			)));
		}
	};

	let settings = settings
		.openid_connect
		.get(&organisation_id.0)
		.ok_or_else(|| internal_error("Failed to find openid config to fulfil request"))?;

	match form.grant_type {
		GrantType::AuthorizationCode => {
			token_endpoint_auth_code_handler(
				raw_form,
				organisation_id.0,
				client_auth,
				backend.0,
				settings,
			)
			.await
		}
		GrantType::RefreshToken => {
			token_endpoint_refresh_token_handler(
				raw_form,
				organisation_id.0,
				client_auth,
				backend.0,
				settings,
			)
			.await
		}
		GrantType::Unknown(_) => Err(TokenEndpointError(TokenError::new(
			Cow::Borrowed(UNSUPPORTED_GRANT_TYPE),
			None,
			None,
		))),
	}
}

pub async fn authentication_endpoint(
	form: extract::Form<AuthenticationRequest>,
	organisation_id: extract::Path<Uuid>,
	backend: extract::Extension<Backend>,
	settings: extract::Extension<Arc<Settings>>,
) -> ApiResult<Result<Html<String>, Redirect<AuthenticationErrorResponse>>> {
	let organisation = match backend.get_organisation_by_id(organisation_id.0).await? {
		Some(org) => org,
		None => {
			return Err(WrappedApiError::item_not_found(Some(Cow::Borrowed(
				"Organisation not found",
			))))
		}
	};

	let form_client_id_parsed =
		Uuid::parse_str(form.client_id.as_str()).map_err(|_| bad_client_id())?;
	let client = backend
		.get_client_by_id(form_client_id_parsed, organisation_id.0)
		.await?
		.ok_or_else(bad_client_id)?;

	let redirect_uri =
		parse_redirect_uri(&backend.0, form_client_id_parsed, form.redirect_uri.as_str()).await?;

	let mut scopes = form.scope.clone();
	scopes.dedup();

	for scope in &scopes {
		if scope.as_str() != "openid"
			&& !backend.scope_exists(organisation_id.0, scope.as_str()).await?
		{
			return Ok(Err(Redirect(
				redirect_uri,
				AuthenticationErrorResponse::new(
					Cow::Borrowed(INVALID_SCOPE),
					None,
					None,
					form.0.state.map(Cow::Owned),
				),
			)));
		}
	}

	if form.prompt.contains(&PromptType::None) {
		return Ok(Err(Redirect(
			redirect_uri,
			AuthenticationErrorResponse::new(
				Cow::Borrowed(INTERACTION_REQUIRED),
				Some(Cow::Borrowed(
					"Provider currently requires login on every authorizaion request",
				)),
				None,
				form.0.state.map(Cow::Owned),
			),
		)));
	}

	if form.response_type.is_empty() {
		Ok(Err(Redirect(
			redirect_uri,
			AuthenticationErrorResponse::new(
				Cow::Borrowed(INVALID_REQUEST),
				Some(Cow::Borrowed("Empty 'response_type' parameter")),
				None,
				form.0.state.map(Cow::Owned),
			),
		)))
	} else if form.response_type.len() > 1 {
		Ok(Err(Redirect(
			redirect_uri,
			AuthenticationErrorResponse::new(
				Cow::Borrowed(UNSUPPORTED_RESPONSE_TYPE),
				Some(Cow::Borrowed("Hybrid flow is not supported")),
				None,
				form.0.state.map(Cow::Owned),
			),
		)))
	} else if form.response_type[0] == ResponseType::Code {
		let settings = settings.openid_connect.get(&organisation_id.0).ok_or_else(|| {
			tracing::error!("Failed to find openid config for organisation {}", organisation_id.0);
			WrappedApiError::internal_error(Some(Cow::Borrowed(
				"Failed to find openid config for organisation",
			)))
		})?;

		let key = settings
			.jwt
			.by_optional_id(client.preferred_key.as_deref())
			.ok_or_else(WrappedApiError::key_not_found)?;
		let login_state = Token::LoginState(Cow::Owned(LoginState::new(
			scopes.clone(),
			form.0,
			organisation_id.0,
			settings,
		)))
		.encode(key)
		.map_err(|why| {
			tracing::error!("Failed to serialize login_state: {}", why);
			WrappedApiError::internal_error(Some(Cow::Borrowed("Failed to serialize login_state")))
		})?;

		Ok(Ok(Html(html::login_html(
			organisation.id,
			organisation.name.as_str(),
			client.name.as_str(),
			scopes,
			login_state.as_str(),
			false,
		))))
	} else {
		Ok(Err(Redirect(
			redirect_uri,
			AuthenticationErrorResponse::new(
				Cow::Borrowed(UNSUPPORTED_RESPONSE_TYPE),
				Some(Cow::Borrowed("Flow is not supported")),
				None,
				form.0.state.map(Cow::Owned),
			),
		)))
	}
}

pub async fn login_endpoint(
	form: extract::Form<LoginRequest>,
	organisation_id: extract::Path<Uuid>,
	backend: extract::Extension<Backend>,
	settings: extract::Extension<Arc<Settings>>,
) -> ApiResult<
	Result<Result<Redirect<CallbackResponse>, Html<String>>, Redirect<AuthenticationErrorResponse>>,
> {
	let settings = settings.openid_connect.get(&organisation_id.0).ok_or_else(|| {
		tracing::error!("Failed to find openid config for organisation {}", organisation_id.0);
		WrappedApiError::internal_error(Some(Cow::Borrowed(
			"Failed to find openid config for organisation",
		)))
	})?;

	if let Some(Ok(claims)) = Token::decode_login_state(form.login_state.as_str(), &settings.jwt) {
		if organisation_id.0 != claims.organisation_id {
			return Err(WrappedApiError::bad_request(Some(Cow::Borrowed(
				"Organisation state mismatch",
			))));
		}

		let client_id = Uuid::parse_str(claims.original_request.client_id.as_str())
			.map_err(|_| bad_client_id())?;
		let client = match backend.get_client_by_id(client_id, organisation_id.0).await? {
			Some(client) => client,
			None => return Err(bad_client_id()),
		};

		let redirect_uri = parse_redirect_uri(
			&backend.0,
			client_id,
			claims.original_request.redirect_uri.as_str(),
		)
		.await?;
		if let Some(user) = backend
			.verify_credentials_and_fetch_user(
				organisation_id.0,
				form.username.as_str(),
				form.password.as_str(),
			)
			.await?
		{
			for scope in &claims.scopes {
				if scope.as_str() != "openid"
					&& !backend
						.user_scope_exists(organisation_id.0, user.id, scope.as_str())
						.await?
				{
					return Ok(Err(Redirect(
						redirect_uri,
						AuthenticationErrorResponse::new(
							Cow::Borrowed(INVALID_SCOPE),
							None,
							None,
							claims.original_request.state.map(Cow::Owned),
						),
					)));
				}
			}

			let state = claims.original_request.state.clone();

			return Ok(Ok(Ok(Redirect(
				redirect_uri,
				CallbackResponse::Code(CodeCallback {
					code: match Token::OpenIDAuthorizationCode(Cow::Owned(
						OpenIDAuthorizationCode::new(
							Subject::new(organisation_id.0, user.id),
							claims.scopes,
							claims.original_request,
							settings,
						),
					))
					.encode(
						settings
							.jwt
							.by_optional_id(client.preferred_key.as_deref())
							.ok_or_else(WrappedApiError::key_not_found)?,
					) {
						Ok(code) => code,
						Err(why) => {
							tracing::error!("Failed to encode CodeClaims: {}", why);
							return Err(WrappedApiError::internal_error(Some(Cow::Borrowed(
								"Failed to serialize code",
							))));
						}
					},
					state,
				}),
			))));
		}

		return match backend.get_organisation_by_id(claims.organisation_id).await? {
			Some(org) => match backend.get_client_by_id(client_id, claims.organisation_id).await? {
				Some(client) => Ok(Ok(Err(Html(html::login_html(
					claims.organisation_id,
					org.name.as_str(),
					client.name.as_str(),
					claims.scopes,
					form.0.login_state.as_str(),
					true,
				))))),
				None => Err(WrappedApiError::bad_request(Some(Cow::Borrowed("Invalid client")))),
			},
			None => {
				Err(WrappedApiError::item_not_found(Some(Cow::Borrowed("Organisation not found"))))
			}
		};
	}

	Err(WrappedApiError::login_failed(Some(Cow::Borrowed("Invalid login state"))))
}

#[derive(Debug, Deserialize, Serialize)]
pub struct UserInfoResponse {
	pub sub: Subject,
	#[serde(flatten)]
	pub extras: Map<String, serde_json::Value>,
}

impl UserInfoResponse {
	pub fn new(sub: Subject, extras: Map<String, serde_json::Value>) -> Self {
		Self { sub, extras }
	}
}

pub struct UserInfoError(HeaderValue);

impl IntoResponse for UserInfoError {
	fn into_response(self) -> axum::response::Response {
		let mut res = axum::response::Response::default();
		*res.status_mut() = StatusCode::UNAUTHORIZED;
		res.headers_mut().insert(axum::http::header::WWW_AUTHENTICATE, self.0);
		res
	}
}

macro_rules! user_info_error {
	($msg:literal) => {
		UserInfoError(HeaderValue::from_static(concat!(
			r#"error="invalid_token", error_description=""#,
			$msg,
			r#"""#
		)))
	};
}

pub async fn user_info_endpoint(
	headers: axum::http::HeaderMap,
	backend: extract::Extension<Backend>,
	organisation_id: extract::Path<Uuid>,
	settings: extract::Extension<Arc<Settings>>,
) -> ApiResult<Result<Json<UserInfoResponse>, UserInfoError>> {
	if let Some(token) = headers.get(axum::http::header::AUTHORIZATION) {
		if let Ok(token_str) = token.to_str() {
			if let Some(encoded_token) = token_str.strip_prefix("Bearer ") {
				let settings =
					settings.openid_connect.get(&organisation_id.0).ok_or_else(|| {
						tracing::error!(
							"Failed to find openid config for organisation {}",
							organisation_id.0
						);
						WrappedApiError::internal_error(Some(Cow::Borrowed(
							"Failed to find openid config for organisation",
						)))
					})?;

				if let Some(Ok(token)) =
					Token::decode_openid_access_token(encoded_token, &settings.jwt)
				{
					let user_claims = backend
						.get_claims_by_user_id_and_scopes_with_destination(
							token.sub.organisation,
							token.sub.user,
							token.scopes.as_slice(),
							models::ClaimDestination::UserInfo,
						)
						.await?;
					let response = UserInfoResponse::new(token.sub, user_claims);

					return Ok(Ok(Json(response)));
				}
			}
		}
	}
	Ok(Err(user_info_error!("Invalid access token")))
}

#[derive(Debug, Serialize, Deserialize)]
pub struct DiscoveryConfiguration {
	pub issuer: String,
	pub authorization_endpoint: String,
	pub token_endpoint: String,
	pub userinfo_endpoint: String,
	pub jwks_uri: String,
	#[serde(skip_serializing_if = "Vec::is_empty", default)]
	pub subject_types_supported: Vec<Cow<'static, str>>,
	#[serde(skip_serializing_if = "Vec::is_empty", default)]
	pub scopes_supported: Vec<Cow<'static, str>>,
	#[serde(skip_serializing_if = "Vec::is_empty", default)]
	pub response_types_supported: Vec<Cow<'static, str>>,
	#[serde(skip_serializing_if = "Vec::is_empty", default)]
	pub id_token_signing_alg_values_supported: Vec<Cow<'static, str>>,
	#[serde(skip_serializing_if = "Vec::is_empty", default)]
	pub claims_supported: Vec<Cow<'static, str>>,
	pub service_documentation: Cow<'static, str>,
	pub claims_locales_supported: Cow<'static, [Cow<'static, str>]>,
	pub ui_locales_supported: Cow<'static, [Cow<'static, str>]>,
	pub request_uri_parameter_supported: bool,
}

impl DiscoveryConfiguration {
	pub fn new(organisation: Uuid, settings: &OpenIDConnectSettings) -> Self {
		Self {
			issuer: format!("{}api/v1/openid/{}", settings.base_url, organisation),
			authorization_endpoint: format!(
				"{}api/v1/openid/{}/authorize",
				settings.base_url, organisation
			),
			token_endpoint: format!("{}api/v1/openid/{}/token", settings.base_url, organisation),
			userinfo_endpoint: format!(
				"{}api/v1/openid/{}/userinfo",
				settings.base_url, organisation
			),
			jwks_uri: format!("{}api/v1/openid/jwks.json", settings.base_url),
			scopes_supported: vec![Cow::Borrowed("openid")],
			response_types_supported: vec![Cow::Borrowed("code")],
			id_token_signing_alg_values_supported: settings
				.jwt
				.keys
				.signing_algorithms()
				.into_iter()
				.map(|algorithm| {
					Cow::Borrowed(match algorithm {
						jsonwebkey::Algorithm::HS256 => "HS256",
						jsonwebkey::Algorithm::HS384 => "HS384",
						jsonwebkey::Algorithm::HS512 => "HS512",
						jsonwebkey::Algorithm::ES256 => "ES256",
						jsonwebkey::Algorithm::ES384 => "ES384",
						jsonwebkey::Algorithm::RS256 => "RS256",
						jsonwebkey::Algorithm::RS384 => "RS384",
						jsonwebkey::Algorithm::RS512 => "RS512",
					})
				})
				.collect(),
			claims_supported: vec![Cow::Borrowed("sub"), Cow::Borrowed("iss")],
			service_documentation: Cow::Borrowed("https://gitlab.com/elise/PrOwOvider"),
			claims_locales_supported: Cow::Borrowed(&[Cow::Borrowed("en-US")]),
			ui_locales_supported: Cow::Borrowed(&[Cow::Borrowed("en-US")]),
			request_uri_parameter_supported: false,
			subject_types_supported: vec![Cow::Borrowed("public"), Cow::Borrowed("pairwise")],
		}
	}
}

pub async fn discovery_configuration_endpoint(
	organisation_id: extract::Path<Uuid>,
	settings: extract::Extension<Arc<Settings>>,
) -> JsonResult<DiscoveryConfiguration> {
	let settings = settings.openid_connect.get(&organisation_id.0).ok_or_else(|| {
		tracing::error!("Failed to find openid config for organisation {}", organisation_id.0);
		WrappedApiError::internal_error(Some(Cow::Borrowed(
			"Failed to find openid config for organisation",
		)))
	})?;

	Ok(Json(DiscoveryConfiguration::new(organisation_id.0, settings)))
}

pub async fn jwks_endpoint(
	organisation_id: extract::Path<Uuid>,
	settings: extract::Extension<Arc<Settings>>,
) -> JsonResult<JsonWebKeySet> {
	let settings = settings.openid_connect.get(&organisation_id.0).ok_or_else(|| {
		tracing::error!("Failed to find openid config for organisation {}", organisation_id.0);
		WrappedApiError::internal_error(Some(Cow::Borrowed(
			"Failed to find openid config for organisation",
		)))
	})?;

	Ok(Json(JsonWebKeySet {
		keys: settings.jwt.keys.0.iter().map(|x| x.public_jwk.clone().into()).collect(),
	}))
}

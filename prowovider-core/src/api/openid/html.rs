/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use std::borrow::Cow;

use html_escape::encode_safe;
use html_to_string_macro::html;
use uuid::Uuid;

pub fn login_html(
	org_id: Uuid,
	org_name: &str,
	client_name: &str,
	scopes: Vec<String>,
	login_state: &str,
	invalid_credentials: bool,
) -> String {
	html! {
		<!DOCTYPE html>
		<html>
			<head>
				<meta charset="utf-8"/>
				<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
				<title>{format!("{} - Login", encode_safe(org_name))}</title>
				<meta name="description" content={format!("{} - Login", encode_safe(org_name))}/>
				<meta name="viewport" content="width=device-width, initial-scale=1"/>
				<link rel="stylesheet" href="/static/css/login.css"/>
			</head>
			<body>
				<div class="outer">
					<div class="container">
						<div class="login">
							<h1>
								{encode_safe(org_name)}" Login"
							</h1>
							<div class="login-col-container">
								<div class="login-col">
									<h4><b>{encode_safe(client_name)}</b>" wants to authenticate you with the following scopes:"</h4>
									<nav>
										<ul>
											{
												scopes.iter().map(|scope| html! {
													<li>{encode_safe(scope)}</li>
												}).collect::<String>()
											}
										</ul>
									</nav>
								</div>
								<div class="login-col">
									<form class="cred-col" id="cred-col" action={format!("/api/v1/openid/{}/login", org_id)} method="post">
										<label for="cred-col"><b>"Credentials"</b></label>
										<div>
											{
												match invalid_credentials {
													true => Cow::Owned(html!(<h4 class="incorrect-creds">"Incorrect Credentials"</h4>)),
													false => Cow::Borrowed("")
												}
											}
											<input type="hidden" id="login_state" name="login_state" value={login_state}/>
										</div>
										<div>
											<label for="username">"Username:"</label>
											<input type="text" id="username" name="username"/>
										</div>
										<div>
											<label for="password">"Password:"</label>
											<input type="password" id="password" name="password"/>
										</div>
										<input type="submit" value="Login"/>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</body>
		</html>
	}
}

/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use std::borrow::Cow;
use uuid::Uuid;

use crate::api::{error::WrappedApiError, ApiResult};

use super::{ModelErrorMarker, ModelResult};

#[derive(Debug, sqlx::FromRow)]
pub struct Organisation {
	pub id: Uuid,
	pub name: String,
}

#[async_trait::async_trait]
pub trait OrganisationModel {
	/// Error returned on a failure by all the functions
	type Error: ModelErrorMarker;

	/// Returns a list of all [Organisation]s
	async fn all_organisations(&self) -> ModelResult<Vec<Organisation>, Self::Error>;

	/// Checks if an organisation exists by UUID
	async fn organisation_exists_by_id(
		&self,
		organisation_id: Uuid,
	) -> ModelResult<bool, Self::Error>;

	/// Checks if an organisation exists by name
	async fn organisation_exists_by_name(&self, name: &str) -> ModelResult<bool, Self::Error>;

	/// Create a new organisation. If an organisation_id is None then an organisation_id needs to be randomly generated.
	async fn create_organisation(
		&self,
		name: &str,
		organisation_id: Option<Uuid>,
	) -> ModelResult<Organisation, Self::Error>;

	/// Delete an organisation by organisation ID
	async fn delete_organisation(&self, organisation_id: Uuid) -> ModelResult<(), Self::Error>;

	/// Rename an organisation
	async fn rename_organisation(
		&self,
		organisation_id: Uuid,
		name: &str,
	) -> ModelResult<(), Self::Error>;

	/// Attempts to fetch an [Organisation] by name
	async fn get_organisation_by_name(
		&self,
		name: &str,
	) -> ModelResult<Option<Organisation>, Self::Error>;

	/// Attempts to fetch an [Organisation] by id
	async fn get_organisation_by_id(
		&self,
		organisation_id: Uuid,
	) -> ModelResult<Option<Organisation>, Self::Error>;

	async fn validate_organisation_exists(&self, id: Uuid) -> ApiResult<()> {
		match self.organisation_exists_by_id(id).await? {
			true => Ok(()),
			false => {
				Err(WrappedApiError::item_not_found(Some(Cow::Borrowed("Organisation not found"))))
			}
		}
	}
}

impl From<Organisation> for prowovider_api::Organisation<'static> {
	fn from(organisation: Organisation) -> Self {
		Self { id: organisation.id, name: Cow::Owned(organisation.name) }
	}
}

#[derive(Debug, sqlx::FromRow)]
pub struct OrganisationUser {
	pub id: Uuid,
	pub username: String,
	pub organisation_id: Uuid,
	pub bcrypt_password: String,
}

#[async_trait::async_trait]
pub trait OrganisationUserModel {
	/// Error returned on a failure by all the functions
	type Error: ModelErrorMarker;

	/// Fetch a list of all [OrganisationUser]s in an organisation
	async fn get_users_by_organisation_id(
		&self,
		organisation_id: Uuid,
	) -> ModelResult<Vec<OrganisationUser>, Self::Error>;

	/// Fetch a user ID from a username inside an organisation
	async fn get_user_id_by_username(
		&self,
		organisation_id: Uuid,
		username: &str,
	) -> ModelResult<Option<Uuid>, Self::Error>;

	/// Fetch a user by ID inside an organisation
	async fn get_user_by_user_id(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
	) -> ModelResult<Option<OrganisationUser>, Self::Error>;

	/// Verifies a username and password against a user inside an organsiation returning the user on success and None on failure. This requires using bcrypt.
	async fn verify_credentials_and_fetch_user(
		&self,
		organisation_id: Uuid,
		username: &str,
		password: &str,
	) -> ModelResult<Option<OrganisationUser>, Self::Error>;

	/// Creates a new user inside an organisation
	async fn new_user(
		&self,
		organisation_id: Uuid,
		username: &str,
		password: &str,
	) -> ModelResult<OrganisationUser, Self::Error>;

	/// Delete a user from an organisation
	async fn delete_user(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
	) -> ModelResult<(), Self::Error>;

	/// Check if a username is taken in an organisation
	async fn user_exists_by_username(
		&self,
		organisation_id: Uuid,
		username: &str,
	) -> ModelResult<bool, Self::Error>;

	/// Check if a user exists by ID inside an organisation
	async fn user_exists_by_id(
		&self,
		organisation_id: Uuid,
		id: Uuid,
	) -> ModelResult<bool, Self::Error>;

	/// Change the username of a user
	async fn user_update_username(
		&self,
		organisation_id: Uuid,
		id: Uuid,
		username: &str,
	) -> ModelResult<(), Self::Error>;

	/// Change the password of a user
	async fn user_update_password(
		&self,
		organisation_id: Uuid,
		id: Uuid,
		password: &str,
	) -> ModelResult<(), Self::Error>;

	/// Change the username and password of a user
	async fn user_update_username_password(
		&self,
		organisation_id: Uuid,
		id: Uuid,
		username: &str,
		password: &str,
	) -> ModelResult<(), Self::Error>;

	async fn validate_user_exists(&self, organisation_id: Uuid, user_id: Uuid) -> ApiResult<()> {
		match self.user_exists_by_id(organisation_id, user_id).await? {
			true => Ok(()),
			false => Err(WrappedApiError::item_not_found(Some(Cow::Borrowed("User not found")))),
		}
	}
}

impl From<OrganisationUser> for prowovider_api::User {
	fn from(user: OrganisationUser) -> Self {
		Self { id: user.id, username: user.username }
	}
}

#[derive(Clone, Copy, Debug, sqlx::Type, PartialEq, Eq)]
#[sqlx(type_name = "claim_type", rename_all = "lowercase")]
pub enum ClaimType {
	Bool,
	Number,
	String,
	Array,
	Object,
}

impl From<ClaimType> for prowovider_api::ClaimType {
	fn from(claim_type: ClaimType) -> Self {
		match claim_type {
			ClaimType::Bool => Self::Bool,
			ClaimType::Number => Self::Number,
			ClaimType::String => Self::String,
			ClaimType::Array => Self::Array,
			ClaimType::Object => Self::Object,
		}
	}
}

impl From<prowovider_api::ClaimType> for ClaimType {
	fn from(claim_type: prowovider_api::ClaimType) -> Self {
		match claim_type {
			prowovider_api::ClaimType::Bool => Self::Bool,
			prowovider_api::ClaimType::Number => Self::Number,
			prowovider_api::ClaimType::String => Self::String,
			prowovider_api::ClaimType::Array => Self::Array,
			prowovider_api::ClaimType::Object => Self::Object,
		}
	}
}

#[derive(Debug, sqlx::FromRow)]
pub struct OrganisationClaim {
	pub id: i64,
	pub organisation_id: Uuid,
	pub r#type: Option<ClaimType>,
	pub claim_name: String,
}

#[async_trait::async_trait]
pub trait OrganisationClaimModel {
	type Error: ModelErrorMarker;

	/// Check if a claim exists on an organisation
	async fn claim_exists(
		&self,
		organisation_id: Uuid,
		claim_name: &str,
	) -> ModelResult<bool, Self::Error>;

	/// Get a list of claims on an organisation
	async fn get_claims_by_organisation_id(
		&self,
		organisation_id: Uuid,
	) -> ModelResult<Vec<OrganisationClaim>, Self::Error>;

	/// Get the type of a claim on an organisation
	async fn get_claim_type_by_organisation_id_and_claim_name(
		&self,
		organisation_id: Uuid,
		claim_name: &str,
	) -> ModelResult<Option<Option<ClaimType>>, Self::Error>;

	/// Register a new claim on an organisation
	async fn new_claim(
		&self,
		organisation_id: Uuid,
		claim_name: &str,
		r#type: Option<ClaimType>,
	) -> ModelResult<OrganisationClaim, Self::Error>;

	/// Delete a claim on an organisation
	async fn delete_claim(
		&self,
		organisation_id: Uuid,
		claim_name: &str,
	) -> ModelResult<(), Self::Error>;

	/// Rename a claim
	async fn rename_claim(
		&self,
		organisation_id: Uuid,
		old_claim_name: &str,
		new_claim_name: &str,
	) -> ModelResult<(), Self::Error>;

	async fn validate_claim_exists(
		&self,
		organisation_id: Uuid,
		claim_name: &str,
	) -> ApiResult<()> {
		match self.claim_exists(organisation_id, claim_name).await? {
			true => Ok(()),
			false => Err(WrappedApiError::item_not_found(Some(Cow::Borrowed("Claim not found")))),
		}
	}
}

#[derive(Debug, sqlx::FromRow)]
pub struct OrganisationUserClaim {
	pub organisation_id: Uuid,
	pub user_id: Uuid,
	pub claim_name: String,
	pub claim_value: serde_json::Value,
}

#[async_trait::async_trait]
pub trait OrganisationUserClaimModel {
	type Error: ModelErrorMarker;

	/// Check if a user has a claim set
	async fn user_claim_exists(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
		claim_name: &str,
	) -> ModelResult<bool, Self::Error>;

	/// Get a list of claims on a user
	async fn get_claims_by_user_id(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
	) -> ModelResult<Vec<OrganisationUserClaim>, Self::Error>;

	/// Get a JSON object of scoped claims on a user for a specific destination
	async fn get_claims_by_user_id_and_scopes_with_destination(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
		scopes: &[String],
		destination: ClaimDestination,
	) -> ModelResult<serde_json::Map<String, serde_json::Value>, Self::Error>;

	/// Get a specific claim on a user
	async fn get_claim_by_user_id_and_claim_name(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
		claim_name: &str,
	) -> ModelResult<Option<OrganisationUserClaim>, Self::Error>;

	/// Upsert the value of a claim on a user
	async fn upsert_claim_on_user(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
		claim_name: &str,
		claim_value: &serde_json::Value,
	) -> ModelResult<(), Self::Error>;

	/// Delete a claim on a user
	async fn delete_claim_on_user(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
		claim_name: &str,
	) -> ModelResult<(), Self::Error>;
}

#[derive(Debug, sqlx::FromRow)]
pub struct OrganisationScope {
	pub organisation_id: Uuid,
	pub scope: String,
}

#[async_trait::async_trait]
pub trait OrganisationScopeModel {
	type Error: ModelErrorMarker;

	/// Check if a scope exists on an organisation
	async fn scope_exists(
		&self,
		organisation_id: Uuid,
		scope: &str,
	) -> ModelResult<bool, Self::Error>;

	/// Get a list of scopes on an organisation
	async fn get_scopes_by_organisation_id(
		&self,
		organisation_id: Uuid,
	) -> ModelResult<Vec<OrganisationScope>, Self::Error>;

	/// Create a new scope on an orgnaisation
	async fn new_scope(
		&self,
		organisation_id: Uuid,
		scope: &str,
	) -> ModelResult<OrganisationScope, Self::Error>;

	/// Delete a scope on an organisation
	async fn delete_scope(
		&self,
		organisation_id: Uuid,
		scope: &str,
	) -> ModelResult<(), Self::Error>;

	async fn validate_scope_exists(
		&self,
		organisation_id: Uuid,
		scope_name: &str,
	) -> ApiResult<()> {
		match self.scope_exists(organisation_id, scope_name).await? {
			true => Ok(()),
			false => Err(WrappedApiError::item_not_found(Some(Cow::Borrowed("Scope not found")))),
		}
	}
}

#[derive(Debug, sqlx::FromRow)]
pub struct OrganisationUserScope {
	pub organisation_id: Uuid,
	pub user_id: Uuid,
	pub scope: String,
}

#[async_trait::async_trait]
pub trait OrganisationUserScopeModel {
	type Error: ModelErrorMarker;

	/// Checks if a user is allowed to use a scope
	async fn user_scope_exists(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
		scope: &str,
	) -> ModelResult<bool, Self::Error>;

	/// Get a list of scopes a user can use
	async fn get_scopes_by_user_id(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
	) -> ModelResult<Vec<OrganisationUserScope>, Self::Error>;

	/// Get a specific scope on a user
	async fn get_scope_by_user_id_and_scope_name(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
		scope: &str,
	) -> ModelResult<Option<OrganisationUserScope>, Self::Error>;

	/// Allow a user to use a scope
	async fn new_scope_on_user(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
		scope: &str,
	) -> ModelResult<OrganisationUserScope, Self::Error>;

	/// Remove permission for a user to use a scope
	async fn delete_scope_on_user(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
		scope: &str,
	) -> ModelResult<Option<OrganisationUserScope>, Self::Error>;
}

#[derive(Debug, sqlx::Type, PartialEq, Eq)]
#[sqlx(type_name = "client_type", rename_all = "lowercase")]
pub enum ClientType {
	Confidential,
	Public,
}

#[derive(Debug, sqlx::FromRow)]
pub struct OrganisationClient {
	pub id: Uuid,
	pub name: String,
	pub organisation_id: Uuid,
	pub client_type: ClientType,
	pub secret: String,
	pub preferred_key: Option<String>,
}

#[async_trait::async_trait]
pub trait OrganisationClientModel {
	type Error: ModelErrorMarker;

	/// Create a new client for an organisation
	async fn new_client(
		&self,
		organisation_id: Uuid,
		name: &str,
		client_type: ClientType,
		preferred_key: Option<&str>,
	) -> ModelResult<OrganisationClient, Self::Error>;

	/// Update the preferred key id for a client
	async fn set_preferred_key_for_client(
		&self,
		id: Uuid,
		organisation_id: Uuid,
		preferred_key: Option<&str>,
	) -> ModelResult<(), Self::Error>;

	/// Fetch a list of clients for an organisation
	async fn get_clients_by_organisation_id(
		&self,
		organisation_id: Uuid,
	) -> ModelResult<Vec<OrganisationClient>, Self::Error>;

	/// Fetch a specific client
	async fn get_client_by_id(
		&self,
		id: Uuid,
		organisation_id: Uuid,
	) -> ModelResult<Option<OrganisationClient>, Self::Error>;

	/// Fetch the name of a specific client
	async fn get_name_of_client(
		&self,
		id: Uuid,
		organisation_id: Uuid,
	) -> ModelResult<Option<String>, Self::Error>;

	/// Check if a client exists
	async fn client_exists(
		&self,
		id: Uuid,
		organisation_id: Uuid,
	) -> ModelResult<bool, Self::Error>;

	/// Delete a client
	async fn delete_client(&self, id: Uuid, organisation_id: Uuid) -> ModelResult<(), Self::Error>;

	async fn validate_client_exists(
		&self,
		organisation_id: Uuid,
		client_id: Uuid,
	) -> ApiResult<()> {
		match self.client_exists(client_id, organisation_id).await? {
			true => Ok(()),
			false => Err(WrappedApiError::item_not_found(Some(Cow::Borrowed("Client not found")))),
		}
	}
}

impl From<OrganisationClient> for prowovider_api::RegisteredClient {
	fn from(client: OrganisationClient) -> Self {
		Self {
			id: client.id,
			secret: match client.client_type {
				ClientType::Confidential => Some(client.secret),
				ClientType::Public => None,
			},
		}
	}
}

#[derive(Debug, sqlx::FromRow)]
pub struct ClientRedirectUri {
	pub client_id: Uuid,
	pub redirect_uri: String,
}

#[async_trait::async_trait]
pub trait ClientRedirectUriModel {
	type Error: ModelErrorMarker;

	/// Get a list of valid redirect URIs for a client
	async fn get_redirect_uris_by_client_id(
		&self,
		client_id: Uuid,
	) -> ModelResult<Vec<String>, Self::Error>;

	/// Delete a redirect URI for a client
	async fn delete_redirect_uri(
		&self,
		client_id: Uuid,
		redirect_uri: &str,
	) -> ModelResult<(), Self::Error>;

	/// Allow a redirect URI for a client
	async fn create_redirect_uri(
		&self,
		client_id: Uuid,
		redirect_uri: &str,
	) -> ModelResult<(), Self::Error>;

	/// Check if a redirect URI is valid for a client
	async fn redirect_uri_exists(
		&self,
		client_id: Uuid,
		redirect_uri: &str,
	) -> ModelResult<bool, Self::Error>;
}

#[derive(Clone, Copy, Debug, sqlx::Type)]
#[sqlx(type_name = "claim_destination", rename_all = "snake_case")]
pub enum ClaimDestination {
	IdToken,
	AccessToken,
	UserInfo,
}

impl From<prowovider_api::ClaimDestination> for ClaimDestination {
	fn from(dest: prowovider_api::ClaimDestination) -> Self {
		match dest {
			prowovider_api::ClaimDestination::IdToken => Self::IdToken,
			prowovider_api::ClaimDestination::AccessToken => Self::AccessToken,
			prowovider_api::ClaimDestination::UserInfo => Self::UserInfo,
		}
	}
}

impl From<ClaimDestination> for prowovider_api::ClaimDestination {
	fn from(dest: ClaimDestination) -> Self {
		match dest {
			ClaimDestination::IdToken => Self::IdToken,
			ClaimDestination::AccessToken => Self::AccessToken,
			ClaimDestination::UserInfo => Self::UserInfo,
		}
	}
}

#[derive(Debug, sqlx::FromRow)]
pub struct OrganisationClaimDestination {
	pub organisation_id: Uuid,
	pub claim_name: String,
	pub destination: ClaimDestination,
}

#[async_trait::async_trait]
pub trait OrganisationClaimDestinationModel {
	type Error: ModelErrorMarker;

	/// Check if a claim destination exists
	async fn destination_exists_for_claim(
		&self,
		organisation_id: Uuid,
		claim_name: &str,
		destination: ClaimDestination,
	) -> ModelResult<bool, Self::Error>;

	/// Set a destination for a claim
	async fn create_destination(
		&self,
		organisation_id: Uuid,
		claim_name: &str,
		destination: ClaimDestination,
	) -> ModelResult<(), Self::Error>;

	/// Fetch a list of destinations for a claim
	async fn get_destinations_by_claim_name(
		&self,
		organisation_id: Uuid,
		claim_name: &str,
	) -> ModelResult<Vec<ClaimDestination>, Self::Error>;

	/// Remove a destination for a claim
	async fn delete_destination(
		&self,
		organisation_id: Uuid,
		claim_name: &str,
		destination: ClaimDestination,
	) -> ModelResult<(), Self::Error>;
}

#[derive(Debug, sqlx::FromRow)]
pub struct OrganisationScopeClaimMap {
	pub organisation_id: Uuid,
	pub scope: String,
	pub claim_name: String,
}

#[async_trait::async_trait]
pub trait OrganisationScopeClaimMapModel {
	type Error: ModelErrorMarker;

	/// Check if a mapping between a scope and a claim exists
	async fn scope_claim_mapping_exists(
		&self,
		organisation_id: Uuid,
		scope: &str,
		claim_name: &str,
	) -> ModelResult<bool, Self::Error>;

	/// Create a mapping between a scope and a claim
	async fn create_scope_claim_mapping(
		&self,
		organisation_id: Uuid,
		scope: &str,
		claim_name: &str,
	) -> ModelResult<(), Self::Error>;

	/// Fetch a list of claims for a scope
	async fn get_mapped_claims_by_scope(
		&self,
		organisation_id: Uuid,
		scope: &str,
	) -> ModelResult<Vec<String>, Self::Error>;

	/// Fetch a list of scopes for a claim
	async fn get_scopes_by_claim_name(
		&self,
		organisation_id: Uuid,
		claim_name: &str,
	) -> ModelResult<Vec<String>, Self::Error>;

	/// Delete a mapping between a scope and a claim
	async fn delete_scope_claim_mapping(
		&self,
		organisation_id: Uuid,
		scope: &str,
		claim_name: &str,
	) -> ModelResult<(), Self::Error>;
}

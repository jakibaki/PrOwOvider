/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use sqlx::{postgres::PgPoolOptions, PgPool};

use crate::{api::error::WrappedApiError, provider::ProviderError, settings::DatabaseSettings};

use super::{ModelError, ModelErrorMarker};

mod organisation;

pub async fn init(settings: &DatabaseSettings) -> Result<PgPool, ProviderError> {
	let pool = PgPoolOptions::new()
		.max_connections(settings.max_connections.unwrap_or(5))
		.connect(&settings.url)
		.await?;

	sqlx::migrate!("./migrations").run(&pool).await?;

	Ok(pool)
}

#[derive(Debug, thiserror::Error)]
pub enum PostgresModelError {
	/// SQLx error
	#[error("Database error: {0}")]
	Sql(#[from] sqlx::Error),
	/// Bcrypt error
	#[error("Bcrypt error: {0}")]
	Bcrypt(#[from] bcrypt::BcryptError),
}

impl<T: Into<PostgresModelError>> From<T> for ModelError<PostgresModelError> {
	fn from(error: T) -> Self {
		Self::Other(error.into())
	}
}

impl From<PostgresModelError> for WrappedApiError {
	fn from(error: PostgresModelError) -> Self {
		match error {
			PostgresModelError::Sql(err) => err.into(),
			PostgresModelError::Bcrypt(err) => err.into(),
		}
	}
}

impl ModelErrorMarker for PostgresModelError {}

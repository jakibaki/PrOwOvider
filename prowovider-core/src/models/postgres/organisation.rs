/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use std::collections::HashSet;

use sqlx::PgPool;
use uuid::Uuid;

use crate::models::{
	organisation::{
		ClientRedirectUriModel, OrganisationClaimDestinationModel, OrganisationClaimModel,
		OrganisationClientModel, OrganisationModel, OrganisationScopeClaimMapModel,
		OrganisationScopeModel, OrganisationUserClaimModel, OrganisationUserModel,
		OrganisationUserScopeModel,
	},
	ClaimDestination, ClaimType, ClientType, ModelResult, Organisation, OrganisationClaim,
	OrganisationClient, OrganisationScope, OrganisationUser, OrganisationUserClaim,
	OrganisationUserScope, BCRYPT_DIFFICULTY,
};

use super::PostgresModelError;

#[async_trait::async_trait]
impl OrganisationModel for PgPool {
	/// Error returned on a failure by all the functions
	type Error = PostgresModelError;

	/// Returns a list of all [Organisation]s
	async fn all_organisations(&self) -> ModelResult<Vec<Organisation>, Self::Error> {
		sqlx::query_as!(Organisation, "SELECT id, name FROM organisations")
			.fetch_all(self)
			.await
			.map_err(Into::into)
	}

	/// Checks if an organisation exists by UUID
	async fn organisation_exists_by_id(
		&self,
		organisation_id: Uuid,
	) -> ModelResult<bool, Self::Error> {
		sqlx::query_scalar!(
			"SELECT EXISTS(SELECT 1 FROM organisations WHERE id = $1)",
			organisation_id
		)
		.fetch_one(self)
		.await
		.map(Option::unwrap_or_default)
		.map_err(Into::into)
	}

	/// Checks if an organisation exists by name
	async fn organisation_exists_by_name(&self, name: &str) -> ModelResult<bool, Self::Error> {
		sqlx::query_scalar!("SELECT EXISTS(SELECT 1 FROM organisations WHERE name = $1)", name)
			.fetch_one(self)
			.await
			.map(Option::unwrap_or_default)
			.map_err(Into::into)
	}

	/// Create a new organisation. If an organisation_id is None then an organisation_id needs to be generated.
	async fn create_organisation(
		&self,
		name: &str,
		organisation_id: Option<Uuid>,
	) -> ModelResult<Organisation, Self::Error> {
		match organisation_id {
			Some(organisation_id) => {
				sqlx::query_as!(
					Organisation,
					"INSERT INTO organisations (id, name) VALUES ($1, $2) RETURNING id, name",
					organisation_id,
					name
				)
				.fetch_one(self)
				.await
			}
			None => {
				sqlx::query_as!(
					Organisation,
					"INSERT INTO organisations (name) VALUES ($1) RETURNING id, name",
					name
				)
				.fetch_one(self)
				.await
			}
		}
		.map_err(Into::into)
	}

	/// Delete an organisation by organisation ID
	async fn delete_organisation(&self, organisation_id: Uuid) -> ModelResult<(), Self::Error> {
		sqlx::query!("DELETE FROM organisations WHERE id = $1", organisation_id)
			.execute(self)
			.await?;

		Ok(())
	}

	/// Rename an organisation
	async fn rename_organisation(
		&self,
		organisation_id: Uuid,
		name: &str,
	) -> ModelResult<(), Self::Error> {
		sqlx::query!("UPDATE organisations SET name = $2 WHERE id = $1", organisation_id, name)
			.execute(self)
			.await?;

		Ok(())
	}

	/// Attempts to fetch an [Organisation] by name
	async fn get_organisation_by_name(
		&self,
		name: &str,
	) -> ModelResult<Option<Organisation>, Self::Error> {
		sqlx::query_as!(Organisation, "SELECT id, name FROM organisations WHERE name = $1", name)
			.fetch_optional(self)
			.await
			.map_err(Into::into)
	}

	/// Attempts to fetch an [Organisation] by id
	async fn get_organisation_by_id(
		&self,
		organisation_id: Uuid,
	) -> ModelResult<Option<Organisation>, Self::Error> {
		sqlx::query_as!(
			Organisation,
			"SELECT id, name FROM organisations WHERE id = $1",
			organisation_id
		)
		.fetch_optional(self)
		.await
		.map_err(Into::into)
	}
}

#[async_trait::async_trait]
impl OrganisationUserModel for PgPool {
	/// Error returned on a failure by all the functions
	type Error = PostgresModelError;

	/// Fetch a list of all [OrganisationUser]s in an organisation
	async fn get_users_by_organisation_id(
		&self,
		organisation_id: Uuid,
	) -> ModelResult<Vec<OrganisationUser>, Self::Error> {
		sqlx::query_as!(
			OrganisationUser,
			"SELECT id, username, organisation_id, bcrypt_password
				FROM organisation_users
					WHERE organisation_id = $1",
			organisation_id
		)
		.fetch_all(self)
		.await
		.map_err(Into::into)
	}

	/// Fetch a user ID from a username inside an organisation
	async fn get_user_id_by_username(
		&self,
		organisation_id: Uuid,
		username: &str,
	) -> ModelResult<Option<Uuid>, Self::Error> {
		sqlx::query_scalar!(
			"SELECT id FROM organisation_users WHERE organisation_id = $1 AND username = $2",
			organisation_id,
			username
		)
		.fetch_optional(self)
		.await
		.map_err(Into::into)
	}

	/// Fetch a user by ID inside an organisation
	async fn get_user_by_user_id(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
	) -> ModelResult<Option<OrganisationUser>, Self::Error> {
		sqlx::query_as!(
			OrganisationUser,
			"SELECT id, username, organisation_id, bcrypt_password
				FROM organisation_users
					WHERE id = $1 AND organisation_id = $2",
			user_id,
			organisation_id,
		)
		.fetch_optional(self)
		.await
		.map_err(Into::into)
	}

	/// Verifies a username and password against a user inside an organsiation returning the . This requires using bcrypt.
	async fn verify_credentials_and_fetch_user(
		&self,
		organisation_id: Uuid,
		username: &str,
		password: &str,
	) -> ModelResult<Option<OrganisationUser>, Self::Error> {
		match sqlx::query_as!(
			OrganisationUser,
			"SELECT id, username, organisation_id, bcrypt_password
				FROM organisation_users
					WHERE username = $1 AND organisation_id = $2",
			username,
			organisation_id,
		)
		.fetch_optional(self)
		.await?
		{
			Some(user) => match bcrypt::verify(password, user.bcrypt_password.as_str())? {
				true => Ok(Some(user)),
				false => Ok(None),
			},
			None => Ok(None),
		}
	}

	/// Creates a new user inside an organisation
	async fn new_user(
		&self,
		organisation_id: Uuid,
		username: &str,
		password: &str,
	) -> ModelResult<OrganisationUser, Self::Error> {
		let hash = bcrypt::hash(password, BCRYPT_DIFFICULTY)?;
		sqlx::query_as!(
			OrganisationUser,
			"INSERT INTO organisation_users (organisation_id, username, bcrypt_password)
				VALUES ($1, $2, $3)
					RETURNING id, username, organisation_id, bcrypt_password",
			organisation_id,
			username,
			hash,
		)
		.fetch_one(self)
		.await
		.map_err(Into::into)
	}

	/// Delete a user from an organisation
	async fn delete_user(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
	) -> ModelResult<(), Self::Error> {
		sqlx::query!(
			"DELETE FROM organisation_users WHERE id = $1 AND organisation_id = $2",
			user_id,
			organisation_id
		)
		.execute(self)
		.await?;

		Ok(())
	}

	/// Check if a username is taken in an organisation
	async fn user_exists_by_username(
		&self,
		organisation_id: Uuid,
		username: &str,
	) -> ModelResult<bool, Self::Error> {
		sqlx::query_scalar!(
			"SELECT EXISTS(SELECT 1 FROM organisation_users WHERE organisation_id = $1 AND username = $2)",
			organisation_id,
			username
		)
		.fetch_one(self)
		.await
		.map(Option::unwrap_or_default)
		.map_err(Into::into)
	}

	/// Check if a user exists by ID inside an organisation
	async fn user_exists_by_id(
		&self,
		organisation_id: Uuid,
		id: Uuid,
	) -> ModelResult<bool, Self::Error> {
		sqlx::query_scalar!(
			"SELECT EXISTS(SELECT 1 FROM organisation_users WHERE organisation_id = $1 AND id = $2)",
			organisation_id,
			id
		)
		.fetch_one(self)
		.await
		.map(Option::unwrap_or_default)
		.map_err(Into::into)
	}

	/// Change the username of a user
	async fn user_update_username(
		&self,
		organisation_id: Uuid,
		id: Uuid,
		username: &str,
	) -> ModelResult<(), Self::Error> {
		sqlx::query!(
			"UPDATE organisation_users SET username = $3 WHERE id = $1 AND organisation_id = $2",
			id,
			organisation_id,
			username
		)
		.execute(self)
		.await
		.map(|_| ())
		.map_err(Into::into)
	}

	/// Change the password of a user
	async fn user_update_password(
		&self,
		organisation_id: Uuid,
		id: Uuid,
		password: &str,
	) -> ModelResult<(), Self::Error> {
		let hash = bcrypt::hash(password, BCRYPT_DIFFICULTY)?;
		sqlx::query!(
			"UPDATE organisation_users SET bcrypt_password = $3 WHERE id = $1 AND organisation_id = $2",
			id,
			organisation_id,
			hash
		)
		.execute(self)
		.await?;

		Ok(())
	}

	/// Change the username and password of a user
	async fn user_update_username_password(
		&self,
		organisation_id: Uuid,
		id: Uuid,
		username: &str,
		password: &str,
	) -> ModelResult<(), Self::Error> {
		let hash = bcrypt::hash(password, BCRYPT_DIFFICULTY)?;
		sqlx::query!(
			"UPDATE organisation_users SET username = $3, bcrypt_password = $4 WHERE id = $1 AND organisation_id = $2",
			id,
			organisation_id,
			username,
			hash,
		)
		.execute(self)
		.await?;

		Ok(())
	}
}

#[async_trait::async_trait]
impl OrganisationClaimModel for PgPool {
	type Error = PostgresModelError;

	async fn claim_exists(
		&self,
		organisation_id: Uuid,
		claim_name: &str,
	) -> ModelResult<bool, Self::Error> {
		sqlx::query_scalar!(
            "SELECT EXISTS(SELECT 1 FROM organisation_claims WHERE organisation_id = $1 AND claim_name = $2 )",
            organisation_id,
            claim_name
        )
        .fetch_one(self)
        .await
        .map(Option::unwrap_or_default).map_err(Into::into)
	}

	async fn get_claims_by_organisation_id(
		&self,
		organisation_id: Uuid,
	) -> ModelResult<Vec<OrganisationClaim>, Self::Error> {
		sqlx::query_as!(
			OrganisationClaim,
			r#"SELECT id, organisation_id, claim_name, type AS "type: ClaimType"
				FROM organisation_claims
				WHERE organisation_id = $1"#,
			organisation_id
		)
		.fetch_all(self)
		.await
		.map_err(Into::into)
	}

	async fn get_claim_type_by_organisation_id_and_claim_name(
		&self,
		organisation_id: Uuid,
		claim_name: &str,
	) -> ModelResult<Option<Option<ClaimType>>, Self::Error> {
		sqlx::query_scalar!(
			r#"SELECT type AS "type: ClaimType" FROM organisation_claims WHERE organisation_id = $1 AND claim_name = $2"#,
			organisation_id,
			claim_name
		)
		.fetch_optional(self)
		.await
		.map_err(Into::into)
	}

	async fn new_claim(
		&self,
		organisation_id: Uuid,
		claim_name: &str,
		r#type: Option<ClaimType>,
	) -> ModelResult<OrganisationClaim, Self::Error> {
		sqlx::query_as!(
			OrganisationClaim,
			r#"INSERT INTO organisation_claims (organisation_id, claim_name, type)
				VALUES ($1, $2, $3)
					RETURNING id, organisation_id, claim_name, type AS "type: ClaimType""#,
			organisation_id,
			claim_name,
			r#type as Option<ClaimType>,
		)
		.fetch_one(self)
		.await
		.map_err(Into::into)
	}

	async fn delete_claim(
		&self,
		organisation_id: Uuid,
		claim_name: &str,
	) -> ModelResult<(), Self::Error> {
		sqlx::query!(
			"DELETE FROM organisation_claims WHERE organisation_id = $1 AND claim_name = $2",
			organisation_id,
			claim_name
		)
		.execute(self)
		.await?;

		Ok(())
	}

	async fn rename_claim(
		&self,
		organisation_id: Uuid,
		old_claim_name: &str,
		new_claim_name: &str,
	) -> ModelResult<(), Self::Error> {
		sqlx::query!(
			"UPDATE organisation_claims SET claim_name = $3 WHERE organisation_id = $1 AND claim_name = $2",
			organisation_id,
			old_claim_name,
			new_claim_name
		)
		.execute(self)
		.await?;

		Ok(())
	}
}

#[async_trait::async_trait]
impl OrganisationScopeModel for PgPool {
	type Error = PostgresModelError;

	async fn scope_exists(
		&self,
		organisation_id: Uuid,
		scope: &str,
	) -> ModelResult<bool, Self::Error> {
		sqlx::query_scalar!(
            "SELECT EXISTS(SELECT 1 FROM organisation_scopes WHERE organisation_id = $1 AND scope = $2 )",
            organisation_id,
            scope
        )
        .fetch_one(self)
        .await
        .map(Option::unwrap_or_default)
		.map_err(Into::into)
	}

	async fn get_scopes_by_organisation_id(
		&self,
		organisation_id: Uuid,
	) -> ModelResult<Vec<OrganisationScope>, Self::Error> {
		sqlx::query_as!(
			OrganisationScope,
			"SELECT organisation_id, scope FROM organisation_scopes WHERE organisation_id = $1",
			organisation_id
		)
		.fetch_all(self)
		.await
		.map_err(Into::into)
	}

	async fn new_scope(
		&self,
		organisation_id: Uuid,
		scope: &str,
	) -> ModelResult<OrganisationScope, Self::Error> {
		sqlx::query_as!(
			OrganisationScope,
			"INSERT INTO organisation_scopes (organisation_id, scope) VALUES ($1, $2) RETURNING organisation_id, scope",
			organisation_id,
			scope,
		)
		.fetch_one(self)
		.await
		.map_err(Into::into)
	}

	async fn delete_scope(
		&self,
		organisation_id: Uuid,
		scope: &str,
	) -> ModelResult<(), Self::Error> {
		sqlx::query!(
			"DELETE FROM organisation_scopes WHERE organisation_id = $1 AND scope = $2",
			organisation_id,
			scope
		)
		.execute(self)
		.await?;

		Ok(())
	}
}

#[async_trait::async_trait]
impl OrganisationUserClaimModel for PgPool {
	type Error = PostgresModelError;

	async fn user_claim_exists(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
		claim_name: &str,
	) -> ModelResult<bool, Self::Error> {
		sqlx::query_scalar!(
			"SELECT EXISTS(
				SELECT 1
					FROM organisation_claim_entries
						WHERE organisation_id = $1 AND user_id = $2 AND claim_name = $3
			)",
			organisation_id,
			user_id,
			claim_name
		)
		.fetch_one(self)
		.await
		.map(Option::unwrap_or_default)
		.map_err(Into::into)
	}

	async fn get_claims_by_user_id(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
	) -> ModelResult<Vec<OrganisationUserClaim>, Self::Error> {
		sqlx::query_as!(
			OrganisationUserClaim,
			"SELECT organisation_id, user_id, claim_name, claim_value
				FROM organisation_claim_entries
					WHERE organisation_id = $1 AND user_id = $2",
			organisation_id,
			user_id
		)
		.fetch_all(self)
		.await
		.map_err(Into::into)
	}

	async fn get_claims_by_user_id_and_scopes_with_destination(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
		scopes: &[String],
		destination: ClaimDestination,
	) -> ModelResult<serde_json::Map<String, serde_json::Value>, Self::Error> {
		let claims = self.get_claims_by_user_id(organisation_id, user_id).await?;

		let mut allowed_claims_for_scopes = HashSet::new();
		for scope in scopes {
			for claim in self.get_mapped_claims_by_scope(organisation_id, scope.as_str()).await? {
				allowed_claims_for_scopes.insert(claim);
			}
		}

		let mut out = serde_json::Map::new();
		for claim in claims {
			if self
				.destination_exists_for_claim(
					organisation_id,
					claim.claim_name.as_str(),
					destination,
				)
				.await? && allowed_claims_for_scopes.contains(&claim.claim_name)
			{
				out.insert(claim.claim_name, claim.claim_value);
			}
		}

		Ok(out)
	}

	async fn get_claim_by_user_id_and_claim_name(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
		claim_name: &str,
	) -> ModelResult<Option<OrganisationUserClaim>, Self::Error> {
		sqlx::query_as!(
			OrganisationUserClaim,
			"SELECT organisation_id, user_id, claim_name, claim_value
				FROM organisation_claim_entries
					WHERE organisation_id = $1 AND user_id = $2 AND claim_name = $3",
			organisation_id,
			user_id,
			claim_name
		)
		.fetch_optional(self)
		.await
		.map_err(Into::into)
	}

	async fn upsert_claim_on_user(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
		claim_name: &str,
		claim_value: &serde_json::Value,
	) -> ModelResult<(), Self::Error> {
		sqlx::query!(
			"INSERT INTO organisation_claim_entries (organisation_id, user_id, claim_name, claim_value)
				VALUES ($1, $2, $3, $4)
					ON CONFLICT (organisation_id, user_id, claim_name) DO UPDATE SET claim_value = $4",
			organisation_id,
			user_id,
			claim_name,
			claim_value
		)
		.fetch_optional(self)
		.await?;

		Ok(())
	}

	async fn delete_claim_on_user(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
		claim_name: &str,
	) -> ModelResult<(), Self::Error> {
		sqlx::query!(
			"DELETE FROM organisation_claim_entries WHERE organisation_id = $1 AND user_id = $2 AND claim_name = $3",
			organisation_id,
			user_id,
			claim_name
		).fetch_optional(self).await?;

		Ok(())
	}
}

#[async_trait::async_trait]
impl OrganisationUserScopeModel for PgPool {
	type Error = PostgresModelError;

	async fn user_scope_exists(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
		scope: &str,
	) -> ModelResult<bool, Self::Error> {
		sqlx::query_scalar!(
			"SELECT EXISTS(
				SELECT 1
					FROM organisation_user_scopes
						WHERE organisation_id = $1 AND user_id = $2 AND scope = $3
			)",
			organisation_id,
			user_id,
			scope
		)
		.fetch_one(self)
		.await
		.map(Option::unwrap_or_default)
		.map_err(Into::into)
	}

	async fn get_scopes_by_user_id(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
	) -> ModelResult<Vec<OrganisationUserScope>, Self::Error> {
		sqlx::query_as!(
			OrganisationUserScope,
			"SELECT organisation_id, user_id, scope
				FROM organisation_user_scopes
					WHERE organisation_id = $1 AND user_id = $2",
			organisation_id,
			user_id
		)
		.fetch_all(self)
		.await
		.map_err(Into::into)
	}

	async fn get_scope_by_user_id_and_scope_name(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
		scope: &str,
	) -> ModelResult<Option<OrganisationUserScope>, Self::Error> {
		sqlx::query_as!(
			OrganisationUserScope,
			"SELECT organisation_id, user_id, scope
				FROM organisation_user_scopes
					WHERE organisation_id = $1 AND user_id = $2 AND scope = $3",
			organisation_id,
			user_id,
			scope
		)
		.fetch_optional(self)
		.await
		.map_err(Into::into)
	}

	async fn new_scope_on_user(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
		scope: &str,
	) -> ModelResult<OrganisationUserScope, Self::Error> {
		sqlx::query_as!(
			OrganisationUserScope,
			"INSERT INTO organisation_user_scopes (organisation_id, user_id, scope)
				VALUES ($1, $2, $3)
					RETURNING organisation_id, user_id, scope",
			organisation_id,
			user_id,
			scope
		)
		.fetch_one(self)
		.await
		.map_err(Into::into)
	}

	async fn delete_scope_on_user(
		&self,
		organisation_id: Uuid,
		user_id: Uuid,
		scope: &str,
	) -> ModelResult<Option<OrganisationUserScope>, Self::Error> {
		sqlx::query_as!(
			OrganisationUserScope,
			"DELETE FROM organisation_user_scopes
				WHERE organisation_id = $1 AND user_id = $2 AND scope = $3
					RETURNING organisation_id, user_id, scope",
			organisation_id,
			user_id,
			scope
		)
		.fetch_optional(self)
		.await
		.map_err(Into::into)
	}
}

#[async_trait::async_trait]
impl OrganisationClientModel for PgPool {
	type Error = PostgresModelError;

	async fn new_client(
		&self,
		organisation_id: Uuid,
		name: &str,
		client_type: ClientType,
		preferred_key: Option<&str>,
	) -> ModelResult<OrganisationClient, Self::Error> {
		const CHARS: &[u8] = b"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@*^%";
		let secret = rand::random::<[u8; 32]>()
			.into_iter()
			.map(|x| CHARS[(x as usize) % CHARS.len()] as char)
			.collect::<String>();

		sqlx::query_as!(
			OrganisationClient,
			"INSERT INTO organisation_clients (organisation_id, name, client_type, secret, preferred_key)
				VALUES ($1, $2, $3, $4, $5)
					RETURNING id, name, organisation_id, secret, client_type AS \"client_type: ClientType\", preferred_key",
			organisation_id,
			name,
			client_type as ClientType,
			secret,
			preferred_key,
		)
		.fetch_one(self)
		.await
		.map_err(Into::into)
	}

	async fn set_preferred_key_for_client(
		&self,
		id: Uuid,
		organisation_id: Uuid,
		preferred_key: Option<&str>,
	) -> ModelResult<(), Self::Error> {
		sqlx::query!(
			"UPDATE organisation_clients SET preferred_key = $1 WHERE id = $2 AND organisation_id = $3",
			preferred_key,
			id,
			organisation_id
		)
		.execute(self)
		.await?;

		Ok(())
	}

	async fn get_clients_by_organisation_id(
		&self,
		organisation_id: Uuid,
	) -> ModelResult<Vec<OrganisationClient>, Self::Error> {
		sqlx::query_as!(
			OrganisationClient,
			"SELECT id, organisation_id, name, secret, client_type AS \"client_type: ClientType\", preferred_key
				FROM organisation_clients
					WHERE organisation_id = $1",
			organisation_id
		)
		.fetch_all(self)
		.await
		.map_err(Into::into)
	}

	async fn get_client_by_id(
		&self,
		id: Uuid,
		organisation_id: Uuid,
	) -> ModelResult<Option<OrganisationClient>, Self::Error> {
		sqlx::query_as!(
            OrganisationClient,
            "SELECT id, organisation_id, name, secret, client_type AS \"client_type: ClientType\", preferred_key
				FROM organisation_clients
					WHERE id = $1 AND organisation_id = $2",
            id,
            organisation_id
        )
        .fetch_optional(self)
        .await
		.map_err(Into::into)
	}

	async fn get_name_of_client(
		&self,
		id: Uuid,
		organisation_id: Uuid,
	) -> ModelResult<Option<String>, Self::Error> {
		sqlx::query_scalar!(
			"SELECT name FROM organisation_clients WHERE id = $1 AND organisation_id = $2",
			id,
			organisation_id
		)
		.fetch_optional(self)
		.await
		.map_err(Into::into)
	}

	async fn client_exists(
		&self,
		id: Uuid,
		organisation_id: Uuid,
	) -> ModelResult<bool, Self::Error> {
		sqlx::query_scalar!(
            "SELECT EXISTS(SELECT 1 FROM organisation_clients WHERE id = $1 AND organisation_id = $2)",
            id,
            organisation_id
        )
        .fetch_one(self)
        .await
        .map(Option::unwrap_or_default)
		.map_err(Into::into)
	}

	async fn delete_client(&self, id: Uuid, organisation_id: Uuid) -> ModelResult<(), Self::Error> {
		sqlx::query!(
			"DELETE FROM organisation_clients WHERE id = $1 AND organisation_id = $2",
			id,
			organisation_id
		)
		.execute(self)
		.await?;

		Ok(())
	}
}

#[async_trait::async_trait]
impl ClientRedirectUriModel for PgPool {
	type Error = PostgresModelError;

	async fn get_redirect_uris_by_client_id(
		&self,
		client_id: Uuid,
	) -> ModelResult<Vec<String>, Self::Error> {
		sqlx::query_scalar!(
			"SELECT redirect_uri FROM client_redirect_uris WHERE client_id = $1",
			client_id
		)
		.fetch_all(self)
		.await
		.map_err(Into::into)
	}

	async fn delete_redirect_uri(
		&self,
		client_id: Uuid,
		redirect_uri: &str,
	) -> ModelResult<(), Self::Error> {
		sqlx::query!(
			"DELETE FROM client_redirect_uris WHERE client_id = $1 AND redirect_uri = $2",
			client_id,
			redirect_uri
		)
		.execute(self)
		.await?;
		Ok(())
	}

	async fn create_redirect_uri(
		&self,
		client_id: Uuid,
		redirect_uri: &str,
	) -> ModelResult<(), Self::Error> {
		sqlx::query!(
            "INSERT INTO client_redirect_uris (client_id, redirect_uri) VALUES ($1, $2) ON CONFLICT DO NOTHING",
            client_id,
            redirect_uri
        )
        .execute(self)
        .await?;
		Ok(())
	}

	async fn redirect_uri_exists(
		&self,
		client_id: Uuid,
		redirect_uri: &str,
	) -> ModelResult<bool, Self::Error> {
		sqlx::query_scalar!(
            "SELECT EXISTS(SELECT 1 FROM client_redirect_uris WHERE client_id = $1 AND redirect_uri = $2)",
            client_id,
            redirect_uri
        )
        .fetch_one(self)
        .await
		.map(Option::unwrap_or_default)
		.map_err(Into::into)
	}
}

#[async_trait::async_trait]
impl OrganisationScopeClaimMapModel for PgPool {
	type Error = PostgresModelError;

	async fn scope_claim_mapping_exists(
		&self,
		organisation_id: Uuid,
		scope: &str,
		claim_name: &str,
	) -> ModelResult<bool, Self::Error> {
		Ok(sqlx::query_scalar!(
			"SELECT EXISTS(
				SELECT 1
					FROM organisation_scope_claim_map
						WHERE organisation_id = $1 AND scope = $2 AND claim_name = $3
			)",
			organisation_id,
			scope,
			claim_name,
		)
		.fetch_one(self)
		.await?
		.unwrap_or_default())
	}

	async fn create_scope_claim_mapping(
		&self,
		organisation_id: Uuid,
		scope: &str,
		claim_name: &str,
	) -> ModelResult<(), Self::Error> {
		sqlx::query!(
			"INSERT INTO organisation_scope_claim_map (organisation_id, scope, claim_name) VALUES ($1, $2, $3)",
			organisation_id,
			scope,
			claim_name,
		)
		.execute(self)
		.await?;

		Ok(())
	}

	async fn get_mapped_claims_by_scope(
		&self,
		organisation_id: Uuid,
		scope: &str,
	) -> ModelResult<Vec<String>, Self::Error> {
		sqlx::query_scalar!(
			"SELECT claim_name FROM organisation_scope_claim_map WHERE organisation_id = $1 AND scope = $2",
			organisation_id,
			scope,
		)
		.fetch_all(self)
		.await
		.map_err(Into::into)
	}

	async fn get_scopes_by_claim_name(
		&self,
		organisation_id: Uuid,
		claim_name: &str,
	) -> ModelResult<Vec<String>, Self::Error> {
		sqlx::query_scalar!(
			"SELECT claim_name FROM organisation_scope_claim_map WHERE organisation_id = $1 AND claim_name = $2",
			organisation_id,
			claim_name,
		)
		.fetch_all(self)
		.await
		.map_err(Into::into)
	}

	async fn delete_scope_claim_mapping(
		&self,
		organisation_id: Uuid,
		scope: &str,
		claim_name: &str,
	) -> ModelResult<(), Self::Error> {
		sqlx::query!(
			"DELETE FROM organisation_scope_claim_map WHERE organisation_id = $1 AND scope = $2 AND claim_name = $3",
			organisation_id,
			scope,
			claim_name,
		)
		.execute(self)
		.await?;

		Ok(())
	}
}

#[async_trait::async_trait]
impl OrganisationClaimDestinationModel for PgPool {
	type Error = PostgresModelError;

	async fn destination_exists_for_claim(
		&self,
		organisation_id: Uuid,
		claim_name: &str,
		destination: ClaimDestination,
	) -> ModelResult<bool, Self::Error> {
		Ok(sqlx::query_scalar!(
			r#"SELECT EXISTS(
				SELECT 1
					FROM organisation_claim_destinations
						WHERE organisation_id = $1 AND claim_name = $2 AND destination = $3
			)"#,
			organisation_id,
			claim_name,
			destination as ClaimDestination
		)
		.fetch_one(self)
		.await?
		.unwrap_or_default())
	}

	async fn create_destination(
		&self,
		organisation_id: Uuid,
		claim_name: &str,
		destination: ClaimDestination,
	) -> ModelResult<(), Self::Error> {
		sqlx::query!(
			"INSERT INTO organisation_claim_destinations (organisation_id, claim_name, destination) VALUES ($1, $2, $3)",
			organisation_id,
			claim_name,
			destination as ClaimDestination,
		)
		.execute(self)
		.await?;

		Ok(())
	}

	async fn get_destinations_by_claim_name(
		&self,
		organisation_id: Uuid,
		claim_name: &str,
	) -> ModelResult<Vec<ClaimDestination>, Self::Error> {
		sqlx::query_scalar!(
			r#"SELECT destination AS "destination: ClaimDestination"
				FROM organisation_claim_destinations
					WHERE organisation_id = $1 AND claim_name = $2"#,
			organisation_id,
			claim_name,
		)
		.fetch_all(self)
		.await
		.map_err(Into::into)
	}

	async fn delete_destination(
		&self,
		organisation_id: Uuid,
		claim_name: &str,
		destination: ClaimDestination,
	) -> ModelResult<(), Self::Error> {
		sqlx::query!(
			"DELETE FROM organisation_claim_destinations
				WHERE organisation_id = $1 AND claim_name = $2 AND destination = $3",
			organisation_id,
			claim_name,
			destination as ClaimDestination,
		)
		.execute(self)
		.await?;

		Ok(())
	}
}

/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use std::{collections::HashSet, fmt::Debug};

use jsonwebkey::{Algorithm, JsonWebKey, Key, KeyOps, KeyUse, X509Params};
use jsonwebtoken::{DecodingKey, EncodingKey};
use serde::{Deserialize, Serialize};
use time::Duration;

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Jwk {
	#[serde(flatten)]
	pub key: Box<Key>,
	#[serde(rename = "use")]
	pub key_use: KeyUse,
	#[serde(rename = "kid")]
	pub key_id: String,
	#[serde(rename = "alg")]
	pub algorithm: Algorithm,
}

impl From<Jwk> for JsonWebKey {
	fn from(jwk: Jwk) -> Self {
		Self {
			key: jwk.key,
			key_use: Some(jwk.key_use),
			key_ops: KeyOps::empty(),
			key_id: Some(jwk.key_id.clone()),
			algorithm: Some(jwk.algorithm),
			x5: X509Params::default(),
		}
	}
}

impl Jwk {
	pub fn to_public(&self) -> Self {
		let key = self
			.key
			.to_public()
			.map(|cow| Box::new(cow.into_owned()))
			.unwrap_or_else(|| self.key.clone());

		Self { key, key_use: self.key_use, key_id: self.key_id.clone(), algorithm: self.algorithm }
	}
}

#[derive(Clone)]
pub struct ParsedKey {
	pub encoding: EncodingKey,
	pub decoding: DecodingKey,
	pub algorithm: jsonwebtoken::Algorithm,
	pub jwk: Jwk,
	pub public_jwk: Jwk,
}

impl Debug for ParsedKey {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.debug_struct("ParsedKey").field("jwk", &self.jwk).finish()
	}
}

impl<'de> Deserialize<'de> for ParsedKey {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
	where
		D: serde::Deserializer<'de>,
	{
		let jwk = Jwk::deserialize(deserializer)?;

		let public_jwk = jwk.to_public();
		let encoding = jwk.key.try_to_encoding_key().map_err(|_| {
			serde::de::Error::custom(format!("Key {} does not contain a private key", jwk.key_id))
		})?;
		let decoding = public_jwk.key.to_decoding_key();
		Ok(Self { encoding, decoding, algorithm: jwk.algorithm.into(), jwk, public_jwk })
	}
}

#[derive(Clone, Debug)]
pub struct KeySet(pub Vec<ParsedKey>);

impl<'de> Deserialize<'de> for KeySet {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
	where
		D: serde::Deserializer<'de>,
	{
		let vec = Vec::<ParsedKey>::deserialize(deserializer)?;

		{
			let mut set = HashSet::new();
			for key in &vec {
				if !set.insert(&key.jwk.key_id) {
					return Err(serde::de::Error::custom(format!(
						"Key with duplicate ID: {}",
						key.jwk.key_id
					)));
				}
			}
		}

		if !vec
			.iter()
			.any(|x| x.jwk.algorithm == Algorithm::RS256 && x.jwk.key_use == KeyUse::Signing)
		{
			return Err(serde::de::Error::custom(
				"Missing RS256 signing key which is needed for OpenID Connect discovery",
			));
		}

		Ok(Self(vec))
	}
}

impl KeySet {
	/// Finds a key by ID
	pub fn by_id(&self, key_id: &str) -> Option<&ParsedKey> {
		self.0.iter().find(|x| x.jwk.key_id.as_str() == key_id)
	}

	/// Provides no guarantees about the returned key beyond it being for signing
	pub fn signing(&self) -> Option<&ParsedKey> {
		self.0.iter().find(|x| x.jwk.key_use == KeyUse::Signing)
	}

	/// Provides no guarantees about the returned key beyond it being for encryption
	pub fn encryption(&self) -> Option<&ParsedKey> {
		self.0.iter().find(|x| x.jwk.key_use == KeyUse::Encryption)
	}

	/// Fetch a set of algorithms that can be used for signing
	pub fn signing_algorithms(&self) -> HashSet<Algorithm> {
		self.0.iter().fold(HashSet::new(), |mut set, rhs| {
			if rhs.jwk.key_use == KeyUse::Signing {
				set.insert(rhs.jwk.algorithm);
			}
			set
		})
	}
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize, Hash)]
pub struct ExpiryConfig {
	/// Lifetime in seconds of authorization code (this is the amount of time allowed between the user logging in and the relying party trading the code for an ID token)
	#[serde(with = "openid_types::duration_as_seconds")]
	pub authorization_code_expiry: Duration,
	/// Lifetime in seconds of access tokens
	#[serde(with = "openid_types::duration_as_seconds")]
	pub access_token_expiry: Duration,
	/// Lifetime in seconds of refresh tokens
	#[serde(with = "openid_types::duration_as_seconds")]
	pub refresh_token_expiry: Duration,
	/// Lifetime in seconds of ID tokens
	#[serde(with = "openid_types::duration_as_seconds")]
	pub id_token_expiry: Duration,
	/// Lifetime in seconds of all other JWTs
	#[serde(with = "openid_types::duration_as_seconds")]
	pub generic_expiry: Duration,
}

#[derive(Clone, Debug, Deserialize)]
pub struct KeyConfig {
	#[serde(flatten)]
	pub exp: ExpiryConfig,
	pub keys: KeySet,
}

impl KeyConfig {
	pub fn by_optional_id(&self, key_id: Option<&str>) -> Option<&ParsedKey> {
		key_id.and_then(|key_id| self.keys.by_id(key_id)).or_else(|| self.keys.signing())
	}
}

/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use std::{collections::HashMap, path::Path};

use config::Config;
use uuid::Uuid;

/// Username/Password wrapper
#[derive(Debug, serde::Deserialize)]
pub struct Credentials {
	/// API username
	#[serde(alias = "user")]
	pub username: String,
	/// API password
	#[serde(alias = "pass")]
	pub password: String,
}

/// Organisations settings
#[derive(Debug, serde::Deserialize)]
pub struct OrganisationSettings {
	/// Organisation ID
	pub id: Uuid,
	/// Optional credentials (if both global and scoped credentials are provided then the scoped ones will be used)
	pub credentials: Option<Credentials>,
}

/// Root settings struct
#[derive(Debug, serde::Deserialize)]
pub struct Settings {
	/// Base URL of the deployment
	pub url: String,
	/// Global credentials. If none are provided then each organisation you access needs scoped credentials
	pub credentials: Option<Credentials>,
	/// Map of local alias to organisation settings
	#[serde(default)]
	pub organisations: HashMap<String, OrganisationSettings>,
}

impl Settings {
	pub fn read(config_path_override: Option<&Path>) -> Result<Self, config::ConfigError> {
		Config::builder()
			.add_source(match config_path_override {
				Some(config) => config::File::from(config),
				None => config::File::with_name("settings"),
			})
			.build()?
			.try_deserialize()
	}
}

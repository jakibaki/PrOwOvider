/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use std::fmt::Display;

use prowovider_api::ClaimType;
use reqwest::blocking::Client;
use rustyline::error::ReadlineError;
use serde_json::Value;
use uuid::Uuid;

use crate::{
	api::{write_claim, AddressClaim},
	Error,
};

#[derive(Debug, Clone)]
pub struct Birthdate {
	pub year: u16,
	pub month: u8,
	pub day: u8,
}

impl Birthdate {
	pub fn new(year: u16, month: u8, day: u8) -> Self {
		Self { year, month, day }
	}

	pub fn parse_str(year: &str, month: &str, day: &str) -> Option<Self> {
		let year = match year.parse::<u16>() {
			Ok(x) if x <= 9999 => x,
			_ => {
				eprintln!("Invalid year");
				return None;
			}
		};

		let month = match month.parse::<u8>() {
			Ok(x) if x > 0 && x <= 12 => x,
			_ => {
				eprintln!("Invalid month");
				return None;
			}
		};

		let day = match day.parse::<u8>() {
			Ok(x) if x > 0 && x <= 31 => x,
			_ => {
				eprintln!("Invalid day");
				return None;
			}
		};

		Some(Self::new(year, month, day))
	}

	pub fn parse_full_str(date: &str) -> Option<Self> {
		let (year, rhs) = date.split_once('-')?;
		let (month, day) = rhs.split_once('-')?;
		let year = match year.parse::<u16>() {
			Ok(x) if x <= 9999 => x,
			_ => {
				eprintln!("Invalid year");
				return None;
			}
		};

		let month = match month.parse::<u8>() {
			Ok(x) if x > 0 && x <= 12 => x,
			_ => {
				eprintln!("Invalid month");
				return None;
			}
		};

		let day = match day.parse::<u8>() {
			Ok(x) if x > 0 && x <= 31 => x,
			_ => {
				eprintln!("Invalid day");
				return None;
			}
		};

		Some(Self::new(year, month, day))
	}
}

impl Display for Birthdate {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.write_fmt(format_args!("{:04}-{:02}-{:02}", self.year, self.month, self.day))
	}
}

#[derive(Debug, Clone)]
pub struct InteractiveUserRegistration {
	pub username: String,
	pub password: String,
	pub name: String,
	pub email: String,
	pub birthdate: Birthdate,
	pub phone_number: String,
	pub formatted_address: String,
}

impl InteractiveUserRegistration {
	fn handle_res(res: Result<String, ReadlineError>) -> Option<String> {
		match res {
			Ok(line) => Some(line),
			Err(ReadlineError::Eof) | Err(ReadlineError::Interrupted) => None,
			Err(why) => {
				eprintln!("Failed to readline: {}", why);
				None
			}
		}
	}

	pub fn read_stdin() -> Option<Self> {
		let mut rl = rustyline::Editor::<()>::new();

		let username = Self::handle_res(rl.readline("Username: "))?;
		let password = Self::handle_res(rl.readline("Password: "))?;

		let name = Self::handle_res(rl.readline("Full Name: "))?;
		let email = Self::handle_res(rl.readline("Email: "))?;
		if !email_address::EmailAddress::is_valid(email.as_str()) {
			eprintln!("Invalid Email");
			return None;
		}

		let year = Self::handle_res(rl.readline("Year of birth (0000-9999): "))?;
		let month = Self::handle_res(rl.readline("Month of birth (1-12): "))?;
		let day = Self::handle_res(rl.readline("Day of birth (1-31): "))?;

		let phone_number = Self::handle_res(rl.readline("Phone Number: "))?;
		let formatted_address = Self::handle_res(rl.readline("Formatted Address: "))?;

		Some(Self {
			username,
			password,
			name,
			email,
			birthdate: Birthdate::parse_str(year.as_str(), month.as_str(), day.as_str())?,
			phone_number,
			formatted_address,
		})
	}

	pub fn write_claims(
		self,
		url: &str,
		client: &Client,
		admin_username: &str,
		admin_password: &str,
		organisation_id: Uuid,
		user_id: Uuid,
	) -> Result<(), Error> {
		write_claim(
			url,
			client,
			admin_username,
			admin_password,
			organisation_id,
			user_id,
			"name",
			&Value::String(self.name),
			Some(ClaimType::String),
		)?;
		write_claim(
			url,
			client,
			admin_username,
			admin_password,
			organisation_id,
			user_id,
			"email",
			&Value::String(self.email),
			Some(ClaimType::String),
		)?;
		write_claim(
			url,
			client,
			admin_username,
			admin_password,
			organisation_id,
			user_id,
			"birthdate",
			&Value::String(self.birthdate.to_string()),
			Some(ClaimType::String),
		)?;
		write_claim(
			url,
			client,
			admin_username,
			admin_password,
			organisation_id,
			user_id,
			"phone_number",
			&Value::String(self.phone_number),
			Some(ClaimType::String),
		)?;
		write_claim(
			url,
			client,
			admin_username,
			admin_password,
			organisation_id,
			user_id,
			"address",
			&serde_json::to_value(&AddressClaim::new(self.formatted_address))?,
			Some(ClaimType::Object),
		)?;

		Ok(())
	}
}

macro_rules! readline {
	($msg:literal, $name:ident, $rl:ident) => {
		Self::handle_res(match $name {
			Some($name) => $rl.readline_with_initial($msg, ($name, "")),
			None => $rl.readline($msg),
		})?
	};
}

#[derive(Debug, Clone)]
pub struct InteractiveUserEdit {
	pub name: String,
	pub email: String,
	pub birthdate: Birthdate,
	pub phone_number: String,
	pub formatted_address: String,
}

impl InteractiveUserEdit {
	fn handle_res(res: Result<String, ReadlineError>) -> Option<String> {
		match res {
			Ok(line) => Some(line),
			Err(ReadlineError::Eof) | Err(ReadlineError::Interrupted) => None,
			Err(why) => {
				eprintln!("Failed to readline: {}", why);
				None
			}
		}
	}

	pub fn read_stdin(
		name: Option<&str>,
		email: Option<&str>,
		birthdate: Option<Birthdate>,
		phone_number: Option<&str>,
		formatted_address: Option<&str>,
	) -> Option<Self> {
		let mut rl = rustyline::Editor::<()>::new();

		let name = readline!("Full Name: ", name, rl);

		let email = readline!("Email: ", email, rl);
		if !email_address::EmailAddress::is_valid(email.as_str()) {
			eprintln!("Invalid Email");
			return None;
		}

		let (year, month, day) = match birthdate {
			Some(birthdate) => {
				let year = birthdate.year.to_string();
				let month = birthdate.month.to_string();
				let day = birthdate.day.to_string();

				(
					Self::handle_res(rl.readline_with_initial(
						"Year of birth (0000-9999): ",
						(year.as_str(), ""),
					))?,
					Self::handle_res(
						rl.readline_with_initial("Month of birth (1-12): ", (month.as_str(), "")),
					)?,
					Self::handle_res(
						rl.readline_with_initial("Day of birth (1-31): ", (day.as_str(), "")),
					)?,
				)
			}
			None => (
				Self::handle_res(rl.readline("Year of birth (0000-9999): "))?,
				Self::handle_res(rl.readline("Month of birth (1-12): "))?,
				Self::handle_res(rl.readline("Day of birth (1-31): "))?,
			),
		};

		let phone_number = readline!("Phone Number: ", phone_number, rl);
		let formatted_address = readline!("Formatted Address: ", formatted_address, rl);

		Some(Self {
			name,
			email,
			birthdate: Birthdate::parse_str(year.as_str(), month.as_str(), day.as_str())?,
			phone_number,
			formatted_address,
		})
	}

	pub fn write_claims(
		self,
		url: &str,
		client: &Client,
		admin_username: &str,
		admin_password: &str,
		organisation_id: Uuid,
		user_id: Uuid,
	) -> Result<(), Error> {
		write_claim(
			url,
			client,
			admin_username,
			admin_password,
			organisation_id,
			user_id,
			"name",
			&Value::String(self.name),
			Some(ClaimType::String),
		)?;
		write_claim(
			url,
			client,
			admin_username,
			admin_password,
			organisation_id,
			user_id,
			"email",
			&Value::String(self.email),
			Some(ClaimType::String),
		)?;
		write_claim(
			url,
			client,
			admin_username,
			admin_password,
			organisation_id,
			user_id,
			"birthdate",
			&Value::String(self.birthdate.to_string()),
			Some(ClaimType::String),
		)?;
		write_claim(
			url,
			client,
			admin_username,
			admin_password,
			organisation_id,
			user_id,
			"phone_number",
			&Value::String(self.phone_number),
			Some(ClaimType::String),
		)?;
		write_claim(
			url,
			client,
			admin_username,
			admin_password,
			organisation_id,
			user_id,
			"address",
			&serde_json::to_value(&AddressClaim::new(self.formatted_address))?,
			Some(ClaimType::Object),
		)?;

		Ok(())
	}
}

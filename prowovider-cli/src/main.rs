/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use api::AddressClaim;
use args::Args;
use clap::StructOpt;
use interactive::{Birthdate, InteractiveUserEdit, InteractiveUserRegistration};
use owo_colors::OwoColorize;
use prowovider_api::ClaimType;
use reqwest::StatusCode;
use serde_json::Value;
use settings::{Credentials, Settings};
use uuid::Uuid;

mod api;
mod args;
mod interactive;
mod settings;

#[derive(Debug, thiserror::Error)]
pub enum Error {
	#[error("Receieved status {0} (body: {1:?})")]
	BadResponse(StatusCode, Option<String>),
	#[error(transparent)]
	Reqwest(#[from] reqwest::Error),
	#[error("API returned {1:#?} (status: {0})")]
	Api(StatusCode, prowovider_api::JsonError<'static, 'static>),
	#[error("To update a user please specify either a new username or a new password")]
	SpecifiedNoUsernamePassword,
	#[error("Config is missing credentials")]
	MissingCredentials,
	#[error("Missing organisation in config")]
	MissingOrganisation,
	#[error("Invalid JSON {0}")]
	Json(#[from] serde_json::Error),
}

fn print_success() {
	println!("{}", "Successful".green().bold());
}

fn map_credentials(credentials: &Credentials) -> (&str, &str) {
	(credentials.username.as_str(), credentials.password.as_str())
}

fn credentials<'settings>(
	settings: &'settings Settings,
	organisation_alias: Option<&String>,
) -> Option<(&'settings str, &'settings str)> {
	if let Some(organisation_alias) = organisation_alias {
		if let Some(Some(credentials)) = settings
			.organisations
			.get(organisation_alias)
			.map(|org_settings| &org_settings.credentials)
		{
			return Some(map_credentials(credentials));
		}
	}

	settings.credentials.as_ref().map(map_credentials)
}

fn id(settings: &Settings, organisation_alias: &String) -> Option<Uuid> {
	Some(settings.organisations.get(organisation_alias)?.id)
}

fn handle(args: Args, settings: Settings) -> Result<(), Error> {
	let client = reqwest::blocking::Client::builder()
		.user_agent(concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION")))
		.build()?;

	match args.action {
		args::MainOperation::Health => {
			api::health(settings.url.as_str(), &client)?;
			println!("{}", "Server is ok!".green().bold());
		}
		args::MainOperation::Organisation { action } => match *action {
			args::OrganisationOperation::Create { organisation_name } => {
				let (admin_username, admin_password) =
					credentials(&settings, None).ok_or(Error::MissingCredentials)?;

				let org = api::create_organisation(
					settings.url.as_str(),
					&client,
					admin_username,
					admin_password,
					organisation_name.as_str(),
				)?;
				print_success();
				println!("ID: {}\nName: {}", org.id, org.name);
			}
			args::OrganisationOperation::Delete { organisation_alias } => {
				let (admin_username, admin_password) =
					credentials(&settings, Some(&organisation_alias))
						.ok_or(Error::MissingCredentials)?;
				let organisation_id =
					id(&settings, &organisation_alias).ok_or(Error::MissingOrganisation)?;

				api::delete_organisation(
					settings.url.as_str(),
					&client,
					admin_username,
					admin_password,
					organisation_id,
				)?;
				print_success();
			}
			args::OrganisationOperation::Rename { organisation_alias, organisation_name } => {
				let (admin_username, admin_password) =
					credentials(&settings, Some(&organisation_alias))
						.ok_or(Error::MissingCredentials)?;
				let organisation_id =
					id(&settings, &organisation_alias).ok_or(Error::MissingOrganisation)?;

				api::rename_organisation(
					settings.url.as_str(),
					&client,
					admin_username,
					admin_password,
					organisation_id,
					organisation_name.as_str(),
				)?;
				print_success();
			}
			args::OrganisationOperation::Scopes { organisation_alias, action } => {
				let (admin_username, admin_password) =
					credentials(&settings, Some(&organisation_alias))
						.ok_or(Error::MissingCredentials)?;
				let organisation_id =
					id(&settings, &organisation_alias).ok_or(Error::MissingOrganisation)?;

				match action {
					args::ScopesOperation::List => {
						let scopes = api::get_scopes_on_organisation(
							settings.url.as_str(),
							&client,
							admin_username,
							admin_password,
							organisation_id,
						)?;
						println!(
							"{} {} {}",
							"Successfully fetched".green().bold(),
							scopes.len().to_string().green().bold(),
							"scopes".green().bold()
						);
						for scope in scopes {
							println!("{}", scope);
						}
					}
					args::ScopesOperation::Exists { scope_name } => {
						let scope_exists = api::get_scope_on_organisation(
							settings.url.as_str(),
							&client,
							admin_username,
							admin_password,
							organisation_id,
							scope_name.as_str(),
						)?;
						match scope_exists {
							true => println!("Scope {} {}", scope_name, "exists".green().bold()),
							false => {
								println!("Scope {} {}", scope_name, "does not exist".red().bold())
							}
						}
					}
					args::ScopesOperation::Create { scope_name } => {
						api::put_scope_on_organisation(
							settings.url.as_str(),
							&client,
							admin_username,
							admin_password,
							organisation_id,
							scope_name.as_str(),
						)?;
						print_success();
					}
					args::ScopesOperation::Delete { scope_name } => {
						api::delete_scope_on_organisation(
							settings.url.as_str(),
							&client,
							admin_username,
							admin_password,
							organisation_id,
							scope_name.as_str(),
						)?;
						print_success();
					}
					args::ScopesOperation::Claims { scope_name, action } => match action {
						args::ImpliedClaimsOperation::List => {
							let claims = api::get_claims_on_scope_on_organisation(
								settings.url.as_str(),
								&client,
								admin_username,
								admin_password,
								organisation_id,
								scope_name.as_str(),
							)?;
							println!(
								"{} {} {}",
								"Successfully fetched".green().bold(),
								claims.len().to_string().green().bold(),
								"claims".green().bold()
							);

							for claim in claims {
								println!("{}", claim);
							}
						}
						args::ImpliedClaimsOperation::Add { claim_name } => {
							api::post_claim_on_scope_on_organisation(
								settings.url.as_str(),
								&client,
								admin_username,
								admin_password,
								organisation_id,
								scope_name.as_str(),
								claim_name.as_str(),
							)?;
							print_success();
						}
						args::ImpliedClaimsOperation::Remove { claim_name } => {
							api::delete_claim_on_scope_on_organisation(
								settings.url.as_str(),
								&client,
								admin_username,
								admin_password,
								organisation_id,
								scope_name.as_str(),
								claim_name.as_str(),
							)?;
							print_success();
						}
					},
				}
			}
			args::OrganisationOperation::Claims { organisation_alias, action } => {
				let (admin_username, admin_password) =
					credentials(&settings, Some(&organisation_alias))
						.ok_or(Error::MissingCredentials)?;
				let organisation_id =
					id(&settings, &organisation_alias).ok_or(Error::MissingOrganisation)?;

				match action {
					args::ClaimsOperation::List => {
						let claims = api::get_claims_on_organisation(
							settings.url.as_str(),
							&client,
							admin_username,
							admin_password,
							organisation_id,
						)?;
						println!(
							"{} {} {}",
							"Successfully fetched".green().bold(),
							claims.len().to_string().green().bold(),
							"claims".green().bold()
						);
						for claim in claims {
							println!("{}", claim);
						}
					}
					args::ClaimsOperation::Exists { claim_name } => {
						let claim_exists = api::get_claim_on_organisation(
							settings.url.as_str(),
							&client,
							admin_username,
							admin_password,
							organisation_id,
							claim_name.as_str(),
						)?;
						match claim_exists {
							true => println!("Scope {} {}", claim_name, "exists".green().bold()),
							false => {
								println!("Scope {} {}", claim_name, "does not exist".red().bold())
							}
						}
					}
					args::ClaimsOperation::Create { claim_name, r#type } => {
						api::put_claim_on_organisation(
							settings.url.as_str(),
							&client,
							admin_username,
							admin_password,
							organisation_id,
							claim_name.as_str(),
							r#type,
						)?;
						print_success();
					}
					args::ClaimsOperation::Delete { claim_name } => {
						api::delete_claim_on_organisation(
							settings.url.as_str(),
							&client,
							admin_username,
							admin_password,
							organisation_id,
							claim_name.as_str(),
						)?;
						print_success();
					}
					args::ClaimsOperation::Destinations { claim_name, action } => match action {
						args::ClaimDestinationOperation::List => {
							let destinations = api::get_claim_destinations(
								settings.url.as_str(),
								&client,
								admin_username,
								admin_password,
								organisation_id,
								claim_name.as_str(),
							)?;
							if destinations.is_empty() {
								println!("{}", "No destinations".green().bold());
							} else {
								println!("{}: {:#?}", "Destinations".green().bold(), destinations);
							}
						}
						args::ClaimDestinationOperation::Add { destination } => {
							api::post_claim_destination(
								settings.url.as_str(),
								&client,
								admin_username,
								admin_password,
								organisation_id,
								claim_name.as_str(),
								destination,
							)?;
							print_success()
						}
						args::ClaimDestinationOperation::Remove { destination } => {
							api::delete_claim_destination(
								settings.url.as_str(),
								&client,
								admin_username,
								admin_password,
								organisation_id,
								claim_name.as_str(),
								destination,
							)?;
							print_success()
						}
					},
				}
			}
			args::OrganisationOperation::Users { organisation_alias, action } => {
				let (admin_username, admin_password) =
					credentials(&settings, Some(&organisation_alias))
						.ok_or(Error::MissingCredentials)?;
				let organisation_id =
					id(&settings, &organisation_alias).ok_or(Error::MissingOrganisation)?;

				match action {
					args::UsersOperation::List => {
						let users = api::get_users_on_organisation(
							settings.url.as_str(),
							&client,
							admin_username,
							admin_password,
							organisation_id,
						)?;
						println!(
							"{} {} {}",
							"Successfully fetched".green().bold(),
							users.len().to_string().green().bold(),
							"users".green().bold()
						);
						for (idx, user) in users.iter().enumerate() {
							println!(
								"-- {} {} --\nID: {}\nUsername: {}",
								"User".magenta(),
								idx + 1,
								user.id,
								user.username
							);
						}
					}
					args::UsersOperation::FindId { username } => {
						let user_id = api::resolve_username_on_organisation(
							settings.url.as_str(),
							&client,
							admin_username,
							admin_password,
							organisation_id,
							username.as_str(),
						)?;

						println!("{}! ID: {}", "Found".green().bold(), user_id);
					}
					args::UsersOperation::Exists { user_id } => {
						let users = api::get_user_on_organisation(
							settings.url.as_str(),
							&client,
							admin_username,
							admin_password,
							organisation_id,
							user_id,
						)?;
						match users {
							true => {
								println!("{}", "User exists".green().bold());
							}
							false => {
								println!("{}", "User not found".red().bold());
							}
						}
					}
					args::UsersOperation::Edit { user_id, params } => {
						if !api::get_user_on_organisation(
							settings.url.as_str(),
							&client,
							admin_username,
							admin_password,
							organisation_id,
							user_id,
						)? {
							println!("{}", "User not found".red().bold());
						} else {
							match params {
								args::EditUserParametersSubcommand::Interactive => {
									let claims = api::get_claims_on_user_on_organisation(
										settings.url.as_str(),
										&client,
										admin_username,
										admin_password,
										organisation_id,
										user_id,
									)?;
									let name = claims.get("name").and_then(Value::as_str);
									let email = claims.get("email").and_then(Value::as_str);
									let birthdate = claims
										.get("birthdate")
										.and_then(Value::as_str)
										.and_then(Birthdate::parse_full_str);
									let phone_number =
										claims.get("phone_number").and_then(Value::as_str);
									let formatted_address =
										claims.get("address").cloned().and_then(|value| {
											Some(
												serde_json::from_value::<AddressClaim>(value)
													.ok()?
													.formatted,
											)
										});

									if let Some(interactive_edit) = InteractiveUserEdit::read_stdin(
										name,
										email,
										birthdate,
										phone_number,
										formatted_address.as_deref(),
									) {
										interactive_edit.write_claims(
											settings.url.as_str(),
											&client,
											admin_username,
											admin_password,
											organisation_id,
											user_id,
										)?;
										print_success();
									}
								}
								args::EditUserParametersSubcommand::Args {
									name,
									email,
									birthdate,
									phone_number,
									formatted_address,
								} => {
									if let Some(email) = email {
										api::write_claim(
											settings.url.as_str(),
											&client,
											admin_username,
											admin_password,
											organisation_id,
											user_id,
											"email",
											&Value::String(email),
											Some(ClaimType::String),
										)?;
									}

									if let Some(name) = name {
										api::write_claim(
											settings.url.as_str(),
											&client,
											admin_username,
											admin_password,
											organisation_id,
											user_id,
											"name",
											&Value::String(name),
											Some(ClaimType::String),
										)?;
									}

									if let Some(birthdate) =
										birthdate.as_deref().and_then(Birthdate::parse_full_str)
									{
										api::write_claim(
											settings.url.as_str(),
											&client,
											admin_username,
											admin_password,
											organisation_id,
											user_id,
											"birthdate",
											&Value::String(birthdate.to_string()),
											Some(ClaimType::String),
										)?;
									}

									if let Some(phone_number) = phone_number {
										api::write_claim(
											settings.url.as_str(),
											&client,
											admin_username,
											admin_password,
											organisation_id,
											user_id,
											"phone_number",
											&Value::String(phone_number),
											Some(ClaimType::String),
										)?;
									}

									if let Some(formatted_address) = formatted_address {
										api::write_claim(
											settings.url.as_str(),
											&client,
											admin_username,
											admin_password,
											organisation_id,
											user_id,
											"address",
											&serde_json::to_value(&AddressClaim::new(
												formatted_address,
											))?,
											Some(ClaimType::Object),
										)?;
									}

									print_success();
								}
							}
						}
					}
					args::UsersOperation::Create { params } => match params {
						args::CreateUserParametersSubcommand::Interactive => {
							if let Some(interactive_registration) =
								InteractiveUserRegistration::read_stdin()
							{
								let user = api::create_user_on_organisation(
									settings.url.as_str(),
									&client,
									admin_username,
									admin_password,
									organisation_id,
									interactive_registration.username.as_str(),
									interactive_registration.password.as_str(),
								)?;
								interactive_registration.write_claims(
									settings.url.as_str(),
									&client,
									admin_username,
									admin_password,
									organisation_id,
									user.id,
								)?;
								println!(
									"Created user with ID: {}",
									user.id.to_string().green().bold()
								);
							}
						}
						args::CreateUserParametersSubcommand::Args {
							username,
							password,
							name,
							email,
							birthdate,
							phone_number,
							formatted_address,
						} => {
							let user = api::create_user_on_organisation(
								settings.url.as_str(),
								&client,
								admin_username,
								admin_password,
								organisation_id,
								username.as_str(),
								password.as_str(),
							)?;

							if let Some(email) = email {
								api::write_claim(
									settings.url.as_str(),
									&client,
									admin_username,
									admin_password,
									organisation_id,
									user.id,
									"email",
									&Value::String(email),
									Some(ClaimType::String),
								)?;
							}

							if let Some(name) = name {
								api::write_claim(
									settings.url.as_str(),
									&client,
									admin_username,
									admin_password,
									organisation_id,
									user.id,
									"name",
									&Value::String(name),
									Some(ClaimType::String),
								)?;
							}

							if let Some(birthdate) =
								birthdate.as_deref().and_then(Birthdate::parse_full_str)
							{
								api::write_claim(
									settings.url.as_str(),
									&client,
									admin_username,
									admin_password,
									organisation_id,
									user.id,
									"birthdate",
									&Value::String(birthdate.to_string()),
									Some(ClaimType::String),
								)?;
							}

							if let Some(phone_number) = phone_number {
								api::write_claim(
									settings.url.as_str(),
									&client,
									admin_username,
									admin_password,
									organisation_id,
									user.id,
									"phone_number",
									&Value::String(phone_number),
									Some(ClaimType::String),
								)?;
							}

							if let Some(formatted_address) = formatted_address {
								api::write_claim(
									settings.url.as_str(),
									&client,
									admin_username,
									admin_password,
									organisation_id,
									user.id,
									"address",
									&serde_json::to_value(&AddressClaim::new(formatted_address))?,
									Some(ClaimType::Object),
								)?;
							}

							println!(
								"Created user with ID: {}",
								user.id.to_string().green().bold()
							);
						}
					},
					args::UsersOperation::Delete { user_id } => {
						api::delete_user_on_organisation(
							settings.url.as_str(),
							&client,
							admin_username,
							admin_password,
							organisation_id,
							user_id,
						)?;
						print_success();
					}
					args::UsersOperation::Change { user_id, username, password } => {
						if username.is_none() && password.is_none() {
							return Err(Error::SpecifiedNoUsernamePassword);
						}
						api::update_user_on_organisation(
							settings.url.as_str(),
							&client,
							admin_username,
							admin_password,
							organisation_id,
							user_id,
							username.as_deref(),
							password.as_deref(),
						)?;
						print_success();
					}
					args::UsersOperation::Scopes { user_id, action } => match action {
						args::UsersScopesOperation::List => {
							let scopes = api::get_scopes_on_user_on_organisation(
								settings.url.as_str(),
								&client,
								admin_username,
								admin_password,
								organisation_id,
								user_id,
							)?;
							println!(
								"{} {} {}",
								"Successfully fetched".green().bold(),
								scopes.len().to_string().green().bold(),
								"scopes".green().bold()
							);
							for scope in scopes {
								println!("{}", scope);
							}
						}
						args::UsersScopesOperation::Exists { scope_name } => {
							let allowed = api::scope_exists_on_user_on_organisation(
								settings.url.as_str(),
								&client,
								admin_username,
								admin_password,
								organisation_id,
								user_id,
								scope_name.as_str(),
							)?;
							match allowed {
								true => println!("{}", "User can use scope".green().bold()),
								false => println!("{}", "User can't use scope".red().bold()),
							}
						}
						args::UsersScopesOperation::Allow { scope_name } => {
							api::post_scope_on_user_on_organisation(
								settings.url.as_str(),
								&client,
								admin_username,
								admin_password,
								organisation_id,
								user_id,
								scope_name.as_str(),
							)?;
							print_success();
						}
						args::UsersScopesOperation::Disallow { scope_name } => {
							api::delete_scope_on_user_on_organisation(
								settings.url.as_str(),
								&client,
								admin_username,
								admin_password,
								organisation_id,
								user_id,
								scope_name.as_str(),
							)?;
							print_success();
						}
					},
					args::UsersOperation::Claims { user_id, action } => match action {
						args::UsersClaimsOperation::List => {
							let value = api::get_claims_on_user_on_organisation(
								settings.url.as_str(),
								&client,
								admin_username,
								admin_password,
								organisation_id,
								user_id,
							)?;
							println!(
								"{}\n{}",
								"Successfully fetched claims:".green().bold(),
								value
							);
						}
						args::UsersClaimsOperation::Get { claim_name } => {
							let value = api::get_claim_on_user_on_organisation(
								settings.url.as_str(),
								&client,
								admin_username,
								admin_password,
								organisation_id,
								user_id,
								claim_name.as_str(),
							)?;
							println!(
								"{}\nValue: {}",
								"Successfully fetched claim:".green().bold(),
								value
							);
						}
						args::UsersClaimsOperation::Set { claim_name, claim_value, preview } => {
							let value = serde_json::from_str(claim_value.as_str())?;
							if preview {
								println!("Command would set {} to {}", claim_name, value);
							} else {
								api::put_claim_on_user_on_organisation(
									settings.url.as_str(),
									&client,
									admin_username,
									admin_password,
									organisation_id,
									user_id,
									claim_name.as_str(),
									&value,
								)?;
								print_success();
							}
						}
						args::UsersClaimsOperation::Remove { claim_name } => {
							api::delete_claim_on_user_on_organisation(
								settings.url.as_str(),
								&client,
								admin_username,
								admin_password,
								organisation_id,
								user_id,
								claim_name.as_str(),
							)?;
						}
					},
				}
			}
			args::OrganisationOperation::List => {
				let (admin_username, admin_password) =
					credentials(&settings, None).ok_or(Error::MissingCredentials)?;

				let orgs = api::get_organisations(
					settings.url.as_str(),
					&client,
					admin_username,
					admin_password,
				)?;
				println!(
					"{} {} {}",
					"Successfully fetched".green().bold(),
					orgs.len().to_string().green().bold(),
					"organisations".green().bold()
				);
				for (idx, org) in orgs.iter().enumerate() {
					println!(
						"-- {} {} --\nID: {}\nName: {}",
						"Organisation".magenta(),
						idx + 1,
						org.id,
						org.name
					);
				}
			}
			args::OrganisationOperation::Clients { organisation_alias, action } => {
				let (admin_username, admin_password) =
					credentials(&settings, Some(&organisation_alias))
						.ok_or(Error::MissingCredentials)?;
				let organisation_id =
					id(&settings, &organisation_alias).ok_or(Error::MissingOrganisation)?;

				match action {
					args::ClientOperation::List => {
						let list = api::get_clients_on_organisation(
							settings.url.as_str(),
							&client,
							admin_username,
							admin_password,
							organisation_id,
						)?;
						println!("{}", serde_json::to_string_pretty(&list)?);
					}
					args::ClientOperation::Name { id } => {
						let name = api::get_client_name_on_organisation(
							settings.url.as_str(),
							&client,
							admin_username,
							admin_password,
							organisation_id,
							id,
						)?;
						println!("{}: {}", "Name".green().bold(), name);
					}
					args::ClientOperation::Create { name, public, key } => {
						let creation_res = api::create_client_on_organisation(
							settings.url.as_str(),
							&client,
							admin_username,
							admin_password,
							organisation_id,
							name.as_str(),
							public,
							key,
						)?;
						println!(
							"{}: {}\n{}: {}",
							"ID".bold().green(),
							creation_res.id,
							"Secret".bold().green(),
							match creation_res.secret {
								Some(secret) => secret,
								None => "None".bold().to_string(),
							}
						);
					}
					args::ClientOperation::Delete { id } => {
						api::delete_client_on_organisation(
							settings.url.as_str(),
							&client,
							admin_username,
							admin_password,
							organisation_id,
							id,
						)?;
						println!("{}", "Done".bold().green());
					}
					args::ClientOperation::RedirectUri { id, action } => match action {
						args::ClientUriOperation::List => {
							let list = api::get_redirect_uris_on_client_on_organisation(
								settings.url.as_str(),
								&client,
								admin_username,
								admin_password,
								organisation_id,
								id,
							)?;
							println!("Fetched {} URIs", list.len().to_string().bold().green());
							for uri in list {
								println!("{}", uri);
							}
						}
						args::ClientUriOperation::Create { url } => {
							api::put_redirect_uri_on_client_on_organisation(
								settings.url.as_str(),
								&client,
								admin_username,
								admin_password,
								organisation_id,
								id,
								url.as_str(),
							)?;
							print_success();
						}
						args::ClientUriOperation::Delete { url } => {
							api::delete_redirect_uri_on_client_on_organisation(
								settings.url.as_str(),
								&client,
								admin_username,
								admin_password,
								organisation_id,
								id,
								url.as_str(),
							)?;
							print_success();
						}
					},
					args::ClientOperation::PreferredKey { id, action } => match action {
						args::ClientPreferredKeyOperation::Get => {
							let preferred_key = api::get_preferred_key_on_client_on_organisation(
								settings.url.as_str(),
								&client,
								admin_username,
								admin_password,
								organisation_id,
								id,
							)?;
							println!("{}: {}", "Preferred Key".green().bold(), preferred_key);
						}
						args::ClientPreferredKeyOperation::Set { key } => {
							api::put_preferred_key_on_client_on_organisation(
								settings.url.as_str(),
								&client,
								admin_username,
								admin_password,
								organisation_id,
								id,
								key.as_str(),
							)?;
							print_success();
						}
						args::ClientPreferredKeyOperation::Delete => {
							api::delete_preferred_key_on_client_on_organisation(
								settings.url.as_str(),
								&client,
								admin_username,
								admin_password,
								organisation_id,
								id,
							)?;
							print_success();
						}
					},
				}
			}
		},
	}

	Ok(())
}

fn exit(msg: &str, why: impl std::error::Error) -> ! {
	eprintln!("{}: {}", msg.red().bold(), why);
	std::process::exit(1);
}

fn main() {
	let args = Args::parse();
	let settings = match Settings::read(args.config.as_deref()) {
		Ok(settings) => settings,
		Err(why) => exit("Failed to parse settings", why),
	};

	if let Err(why) = handle(args, settings) {
		exit("Error", why);
	}
}

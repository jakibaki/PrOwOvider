/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use std::borrow::Cow;

use prowovider_api::{
	ClaimDestination, ClaimParameters, ClaimType, ClientList, ClientRegistrationParameters,
	Organisation, OrganisationNameQuery, PreferredKeyUpdate, RedirectUriQuery, RegisteredClient,
};
use reqwest::{blocking::Client, StatusCode};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::Error;

#[derive(Debug, Serialize, Deserialize)]
pub struct AddressClaim {
	pub formatted: String,
}

impl AddressClaim {
	pub fn new(formatted: String) -> Self {
		Self { formatted }
	}
}

pub(crate) fn health(url: &str, client: &Client) -> Result<(), Error> {
	let response = client.get(format!("{}/", url)).send()?;
	let status = response.status();
	let text = response.text()?;

	if status == StatusCode::OK && text.as_str() == "Mew!" {
		Ok(())
	} else {
		Err(Error::BadResponse(status, Some(text)))
	}
}

pub(crate) fn create_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_name: &str,
) -> Result<Organisation<'static>, Error> {
	let response = client
		.post(format!("{}/api/v1/organisations", url))
		.query(&OrganisationNameQuery { name: Cow::Borrowed(organisation_name) })
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(response.json()?),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn delete_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
) -> Result<Organisation<'static>, Error> {
	let response = client
		.delete(format!("{}/api/v1/organisations/{}", url, organisation_id))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(response.json()?),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn rename_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	name: &str,
) -> Result<(), Error> {
	let response = client
		.put(format!("{}/api/v1/organisations/{}", url, organisation_id))
		.query(&OrganisationNameQuery { name: Cow::Borrowed(name) })
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(()),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn get_organisations(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
) -> Result<Vec<Organisation<'static>>, Error> {
	let response = client
		.get(format!("{}/api/v1/organisations", url))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(response.json()?),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn get_scopes_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
) -> Result<Vec<String>, Error> {
	let response = client
		.get(format!("{}/api/v1/organisations/{}/scopes", url, organisation_id))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(response.json()?),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn get_scope_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	scope_name: &str,
) -> Result<bool, Error> {
	let response = client
		.get(format!("{}/api/v1/organisations/{}/scopes/{}", url, organisation_id, scope_name))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(true),
		StatusCode::NOT_FOUND => Ok(false),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn put_scope_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	scope_name: &str,
) -> Result<(), Error> {
	let response = client
		.put(format!("{}/api/v1/organisations/{}/scopes/{}", url, organisation_id, scope_name))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(()),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn delete_scope_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	scope_name: &str,
) -> Result<(), Error> {
	let response = client
		.delete(format!("{}/api/v1/organisations/{}/scopes/{}", url, organisation_id, scope_name))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(()),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn get_claims_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
) -> Result<Vec<String>, Error> {
	let response = client
		.get(format!("{}/api/v1/organisations/{}/claims", url, organisation_id))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(response.json()?),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn get_claim_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	claim_name: &str,
) -> Result<bool, Error> {
	let response = client
		.get(format!("{}/api/v1/organisations/{}/claims/{}", url, organisation_id, claim_name))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(true),
		StatusCode::NOT_FOUND => Ok(false),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn put_claim_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	claim_name: &str,
	claim_type: Option<ClaimType>,
) -> Result<(), Error> {
	let response = client
		.put(format!("{}/api/v1/organisations/{}/claims/{}", url, organisation_id, claim_name))
		.json(&ClaimParameters { r#type: claim_type })
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(()),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn delete_claim_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	claim_name: &str,
) -> Result<(), Error> {
	let response = client
		.delete(format!("{}/api/v1/organisations/{}/claims/{}", url, organisation_id, claim_name))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(()),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn get_users_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
) -> Result<Vec<prowovider_api::User>, Error> {
	let response = client
		.get(format!("{}/api/v1/organisations/{}/users", url, organisation_id))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(response.json()?),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn get_user_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	user_id: Uuid,
) -> Result<bool, Error> {
	let response = client
		.get(format!("{}/api/v1/organisations/{}/users/{}", url, organisation_id, user_id))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(true),
		StatusCode::NOT_FOUND => Ok(false),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn resolve_username_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	username: &str,
) -> Result<Uuid, Error> {
	let response = client
		.get(format!("{}/api/v1/organisations/{}/users/resolve/{}", url, organisation_id, username))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(response.json()?),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn create_user_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	username: &str,
	password: &str,
) -> Result<prowovider_api::User, Error> {
	let response = client
		.post(format!("{}/api/v1/organisations/{}/users", url, organisation_id))
		.basic_auth(admin_username, Some(admin_password))
		.json(&prowovider_api::UserPassword {
			username: Some(Cow::Borrowed(username)),
			password: Some(Cow::Borrowed(password)),
		})
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(response.json()?),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

#[allow(clippy::too_many_arguments)]
pub(crate) fn update_user_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	user_id: Uuid,
	username: Option<&str>,
	password: Option<&str>,
) -> Result<(), Error> {
	let response = client
		.put(format!("{}/api/v1/organisations/{}/users/{}", url, organisation_id, user_id))
		.basic_auth(admin_username, Some(admin_password))
		.json(&prowovider_api::UserPassword {
			username: username.map(Cow::Borrowed),
			password: password.map(Cow::Borrowed),
		})
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(()),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn delete_user_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	user_id: Uuid,
) -> Result<(), Error> {
	let response = client
		.delete(format!("{}/api/v1/organisations/{}/users/{}", url, organisation_id, user_id))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(()),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn get_scopes_on_user_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	user_id: Uuid,
) -> Result<Vec<String>, Error> {
	let response = client
		.get(format!("{}/api/v1/organisations/{}/users/{}/scopes", url, organisation_id, user_id))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(response.json()?),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn post_scope_on_user_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	user_id: Uuid,
	scope_name: &str,
) -> Result<(), Error> {
	let response = client
		.post(format!(
			"{}/api/v1/organisations/{}/users/{}/scopes/{}",
			url, organisation_id, user_id, scope_name
		))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(()),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn scope_exists_on_user_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	user_id: Uuid,
	scope_name: &str,
) -> Result<bool, Error> {
	let response = client
		.get(format!(
			"{}/api/v1/organisations/{}/users/{}/scopes/{}",
			url, organisation_id, user_id, scope_name
		))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(true),
		StatusCode::NOT_FOUND => Ok(false),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn delete_scope_on_user_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	user_id: Uuid,
	scope_name: &str,
) -> Result<(), Error> {
	let response = client
		.delete(format!(
			"{}/api/v1/organisations/{}/users/{}/scopes/{}",
			url, organisation_id, user_id, scope_name
		))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(()),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn get_claims_on_user_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	user_id: Uuid,
) -> Result<serde_json::Value, Error> {
	let response = client
		.get(format!("{}/api/v1/organisations/{}/users/{}/claims", url, organisation_id, user_id))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(response.json()?),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn get_claim_on_user_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	user_id: Uuid,
	claim: &str,
) -> Result<serde_json::Value, Error> {
	let response = client
		.get(format!(
			"{}/api/v1/organisations/{}/users/{}/claims/{}",
			url, organisation_id, user_id, claim
		))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(response.json()?),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

#[allow(clippy::too_many_arguments)]
pub(crate) fn put_claim_on_user_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	user_id: Uuid,
	claim: &str,
	value: &serde_json::Value,
) -> Result<(), Error> {
	let response = client
		.put(format!(
			"{}/api/v1/organisations/{}/users/{}/claims/{}",
			url, organisation_id, user_id, claim
		))
		.json(value)
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(()),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn delete_claim_on_user_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	user_id: Uuid,
	claim: &str,
) -> Result<(), Error> {
	let response = client
		.delete(format!(
			"{}/api/v1/organisations/{}/users/{}/claims/{}",
			url, organisation_id, user_id, claim
		))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(()),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn get_clients_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
) -> Result<ClientList, Error> {
	let response = client
		.get(format!("{}/api/v1/organisations/{}/clients", url, organisation_id))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(response.json()?),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

#[allow(clippy::too_many_arguments)]
pub(crate) fn create_client_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	client_name: &str,
	public: bool,
	preferred_key: Option<String>,
) -> Result<RegisteredClient, Error> {
	let response = client
		.post(format!("{}/api/v1/organisations/{}/clients", url, organisation_id))
		.json(&ClientRegistrationParameters {
			name: client_name.to_string(),
			public,
			preferred_key,
		})
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(response.json()?),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn get_client_name_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	client_id: Uuid,
) -> Result<String, Error> {
	let response = client
		.get(format!("{}/api/v1/organisations/{}/clients/{}", url, organisation_id, client_id))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(response.text()?),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn delete_client_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	client_id: Uuid,
) -> Result<(), Error> {
	let response = client
		.delete(format!("{}/api/v1/organisations/{}/clients/{}", url, organisation_id, client_id))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(()),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn get_redirect_uris_on_client_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	client_id: Uuid,
) -> Result<Vec<String>, Error> {
	let response = client
		.get(format!(
			"{}/api/v1/organisations/{}/clients/{}/redirect_uris",
			url, organisation_id, client_id
		))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(response.json()?),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn put_redirect_uri_on_client_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	client_id: Uuid,
	uri: &str,
) -> Result<(), Error> {
	let response = client
		.put(format!(
			"{}/api/v1/organisations/{}/clients/{}/redirect_uris",
			url, organisation_id, client_id
		))
		.query(&RedirectUriQuery { uri: Cow::Borrowed(uri) })
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(()),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn delete_redirect_uri_on_client_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	client_id: Uuid,
	uri: &str,
) -> Result<(), Error> {
	let response = client
		.delete(format!(
			"{}/api/v1/organisations/{}/clients/{}/redirect_uris",
			url, organisation_id, client_id
		))
		.query(&RedirectUriQuery { uri: Cow::Borrowed(uri) })
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(()),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn get_preferred_key_on_client_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	client_id: Uuid,
) -> Result<String, Error> {
	let response = client
		.get(format!(
			"{}/api/v1/organisations/{}/clients/{}/preferred_key",
			url, organisation_id, client_id
		))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(response.text()?),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn put_preferred_key_on_client_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	client_id: Uuid,
	preferred_key: &str,
) -> Result<(), Error> {
	let response = client
		.put(format!(
			"{}/api/v1/organisations/{}/clients/{}/preferred_key",
			url, organisation_id, client_id
		))
		.query(&PreferredKeyUpdate { id: Cow::Borrowed(preferred_key) })
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(()),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn delete_preferred_key_on_client_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	client_id: Uuid,
) -> Result<(), Error> {
	let response = client
		.delete(format!(
			"{}/api/v1/organisations/{}/clients/{}/preferred_key",
			url, organisation_id, client_id
		))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(()),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

#[allow(clippy::too_many_arguments)]
pub(crate) fn write_claim(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	user_id: Uuid,
	claim: &str,
	value: &serde_json::Value,
	claim_type: Option<ClaimType>,
) -> Result<(), Error> {
	if !get_claim_on_organisation(
		url,
		client,
		admin_username,
		admin_password,
		organisation_id,
		claim,
	)? {
		put_claim_on_organisation(
			url,
			client,
			admin_username,
			admin_password,
			organisation_id,
			claim,
			claim_type,
		)?;
	}

	put_claim_on_user_on_organisation(
		url,
		client,
		admin_username,
		admin_password,
		organisation_id,
		user_id,
		claim,
		value,
	)?;
	Ok(())
}

pub(crate) fn get_claims_on_scope_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	scope_name: &str,
) -> Result<Vec<String>, Error> {
	let response = client
		.get(format!(
			"{}/api/v1/organisations/{}/scopes/{}/claims",
			url, organisation_id, scope_name
		))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(response.json()?),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn post_claim_on_scope_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	scope_name: &str,
	claim_name: &str,
) -> Result<(), Error> {
	let response = client
		.post(format!(
			"{}/api/v1/organisations/{}/scopes/{}/claims/{}",
			url, organisation_id, scope_name, claim_name
		))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(()),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn delete_claim_on_scope_on_organisation(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	scope_name: &str,
	claim_name: &str,
) -> Result<(), Error> {
	let response = client
		.delete(format!(
			"{}/api/v1/organisations/{}/scopes/{}/claims/{}",
			url, organisation_id, scope_name, claim_name
		))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(()),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn get_claim_destinations(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	claim_name: &str,
) -> Result<Vec<ClaimDestination>, Error> {
	let response = client
		.get(format!(
			"{}/api/v1/organisations/{}/claims/{}/destinations",
			url, organisation_id, claim_name
		))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(response.json()?),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn post_claim_destination(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	claim_name: &str,
	destination: ClaimDestination,
) -> Result<(), Error> {
	let response = client
		.post(format!(
			"{}/api/v1/organisations/{}/claims/{}/destinations/{}",
			url, organisation_id, claim_name, destination
		))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(()),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

pub(crate) fn delete_claim_destination(
	url: &str,
	client: &Client,
	admin_username: &str,
	admin_password: &str,
	organisation_id: Uuid,
	claim_name: &str,
	destination: ClaimDestination,
) -> Result<(), Error> {
	let response = client
		.delete(format!(
			"{}/api/v1/organisations/{}/claims/{}/destinations/{}",
			url, organisation_id, claim_name, destination
		))
		.basic_auth(admin_username, Some(admin_password))
		.send()?;
	let status = response.status();

	match status {
		StatusCode::OK => Ok(()),
		s => Err(match response.json() {
			Ok(error) => Error::Api(status, error),
			Err(_) => Error::BadResponse(s, None),
		}),
	}
}

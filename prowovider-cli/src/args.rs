/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use std::path::PathBuf;

use clap::{Parser, Subcommand};
use prowovider_api::{ClaimDestination, ClaimType};
use uuid::Uuid;

#[derive(Debug, Parser)]
#[clap(author, version, about, long_about = None)]
pub struct Args {
	#[clap(long)]
	pub config: Option<PathBuf>,
	/// Action
	#[clap(subcommand)]
	pub action: MainOperation,
}

#[derive(Debug, Subcommand)]
pub enum MainOperation {
	Health,
	Organisation {
		#[clap(subcommand)]
		action: Box<OrganisationOperation>,
	},
}

#[derive(Debug, Subcommand)]
pub enum OrganisationOperation {
	/// List all organisations
	List,
	/// Create a new organisation
	Create {
		/// Name of the organisation
		organisation_name: String,
	},
	/// Rename an organisation
	Rename {
		/// Alias of the organisation
		organisation_alias: String,
		/// Desired name of the organisation
		organisation_name: String,
	},
	/// Delete an organisation
	Delete {
		/// Alias of the organisation
		organisation_alias: String,
	},
	/// Operate on defined scopes on an organisation
	Scopes {
		/// Alias of the organisation
		organisation_alias: String,
		#[clap(subcommand)]
		action: ScopesOperation,
	},
	/// Operate on defined claims on an organisation
	Claims {
		/// Alias of the organisation
		organisation_alias: String,
		#[clap(subcommand)]
		action: ClaimsOperation,
	},
	/// Manage users on an organisation
	Users {
		/// Alias of the organisation
		organisation_alias: String,
		#[clap(subcommand)]
		action: UsersOperation,
	},
	/// Operate on clients on an organisation
	Clients {
		/// Alias of the organisation
		organisation_alias: String,
		#[clap(subcommand)]
		action: ClientOperation,
	},
}

#[derive(Debug, Subcommand)]
pub enum ScopesOperation {
	/// List all defined scopes
	List,
	/// Check if a specific scope is defined
	Exists { scope_name: String },
	/// Define a new scope
	Create { scope_name: String },
	/// Delete a scope
	Delete { scope_name: String },
	/// Operate on claims that are included when a scope is requested
	Claims {
		scope_name: String,
		#[clap(subcommand)]
		action: ImpliedClaimsOperation,
	},
}

#[derive(Debug, Subcommand)]
pub enum ClaimsOperation {
	/// List all defined claims
	List,
	/// Check if a claim is defined
	Exists { claim_name: String },
	/// Define a new claim
	Create {
		claim_name: String,
		/// If present enforces typing of a claim. One of: 'bool', 'number', 'string', 'array', 'object'
		#[clap(long)]
		r#type: Option<ClaimType>,
	},
	/// Delete a claim
	Delete { claim_name: String },
	Destinations {
		claim_name: String,
		#[clap(subcommand)]
		action: ClaimDestinationOperation,
	},
}

#[derive(Debug, Subcommand)]
pub enum ClaimDestinationOperation {
	/// List all destinations for a claim
	List,
	/// Add a destination to a claim (either `id_token`, `access_token`, `user_info`)
	Add { destination: ClaimDestination },
	/// Remove a destination for a claim (either `id_token`, `access_token`, `user_info`)
	Remove { destination: ClaimDestination },
}

#[derive(Debug, Subcommand)]
pub enum ImpliedClaimsOperation {
	/// List all claims implied by a scope
	List,
	/// Add a claim to tokens when scope is requested
	Add { claim_name: String },
	/// Remove the mapping of scope to claim
	Remove { claim_name: String },
}

#[derive(Debug, Subcommand)]
pub enum UsersOperation {
	/// List all registered users
	List,
	/// Finds the UUID of a user by username
	FindId { username: String },
	/// Check if a user is registered
	Exists { user_id: Uuid },
	/// Edit standard claims of a user
	Edit {
		user_id: Uuid,
		#[clap(subcommand)]
		params: EditUserParametersSubcommand,
	},
	/// Manually register a new user
	Create {
		#[clap(subcommand)]
		params: CreateUserParametersSubcommand,
	},
	/// Change a user's username and/or password
	Change {
		user_id: Uuid,
		#[clap(short, long)]
		username: Option<String>,
		#[clap(short, long)]
		password: Option<String>,
	},
	/// Delete a user
	Delete { user_id: Uuid },
	/// Operate on the scopes a user can use
	Scopes {
		user_id: Uuid,
		#[clap(subcommand)]
		action: UsersScopesOperation,
	},
	/// Operate on claims on a user
	Claims {
		user_id: Uuid,
		#[clap(subcommand)]
		action: UsersClaimsOperation,
	},
}

#[derive(Debug, Subcommand)]
pub enum EditUserParametersSubcommand {
	Interactive,
	Args {
		#[clap(long)]
		name: Option<String>,
		#[clap(long)]
		email: Option<String>,
		#[clap(long)]
		birthdate: Option<String>,
		#[clap(long)]
		phone_number: Option<String>,
		#[clap(long)]
		formatted_address: Option<String>,
	},
}

#[derive(Debug, Subcommand)]
pub enum CreateUserParametersSubcommand {
	Interactive,
	Args {
		username: String,
		password: String,
		#[clap(long)]
		name: Option<String>,
		#[clap(long)]
		email: Option<String>,
		#[clap(long)]
		birthdate: Option<String>,
		#[clap(long)]
		phone_number: Option<String>,
		#[clap(long)]
		formatted_address: Option<String>,
	},
}

#[derive(Debug, Subcommand)]
pub enum UsersScopesOperation {
	List,
	Exists { scope_name: String },
	Allow { scope_name: String },
	Disallow { scope_name: String },
}

#[derive(Debug, Subcommand)]
pub enum UsersClaimsOperation {
	List,
	Get {
		claim_name: String,
	},
	Set {
		claim_name: String,
		/// JSON value
		claim_value: String,
		/// Preview the parsed JSON value of 'claim_value'
		#[clap(short, long)]
		preview: bool,
	},
	Remove {
		claim_name: String,
	},
}

#[derive(Debug, Subcommand)]
pub enum ClientOperation {
	List,
	Name {
		id: Uuid,
	},
	Create {
		name: String,
		/// Option for public client, defaults to confidential
		#[clap(short, long)]
		public: bool,
		/// Preferred key for signing tokens
		#[clap(long)]
		key: Option<String>,
	},
	PreferredKey {
		id: Uuid,
		#[clap(subcommand)]
		action: ClientPreferredKeyOperation,
	},
	Delete {
		id: Uuid,
	},
	RedirectUri {
		id: Uuid,
		#[clap(subcommand)]
		action: ClientUriOperation,
	},
}

#[derive(Debug, Subcommand)]
pub enum ClientPreferredKeyOperation {
	Get,
	Set { key: String },
	Delete,
}

#[derive(Debug, Subcommand)]
pub enum ClientUriOperation {
	List,
	Create { url: String },
	Delete { url: String },
}

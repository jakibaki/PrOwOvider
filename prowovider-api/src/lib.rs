/*
	Copyright 2022 Elise Mansbridge <mail@elise.moe>

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

use std::{borrow::Cow, fmt::Display, str::FromStr};

use jsonwebkey::JsonWebKey;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Clone, Debug, Default, Deserialize, Serialize, PartialEq, Eq, Hash)]
pub struct Organisation<'name> {
	pub id: Uuid,
	pub name: Cow<'name, str>,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize, PartialEq, Eq, Hash)]
pub struct OrganisationNameQuery<'name> {
	pub name: Cow<'name, str>,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize, PartialEq, Eq, Hash)]
pub struct JsonError<'error_code, 'error> {
	pub error_code: Cow<'error_code, str>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub error: Option<Cow<'error, str>>,
}

impl<'error_code, 'error> JsonError<'error_code, 'error> {
	pub fn new(error_code: Cow<'error_code, str>, error: Option<Cow<'error, str>>) -> Self {
		Self { error_code, error }
	}
}

#[derive(Clone, Debug, Default, Deserialize, Serialize, PartialEq, Eq, Hash)]
pub struct RegisteredClient {
	pub id: Uuid,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub secret: Option<String>,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize, PartialEq, Eq, Hash)]
pub struct UserPassword<'username, 'password> {
	#[serde(skip_serializing_if = "Option::is_none")]
	pub username: Option<Cow<'username, str>>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub password: Option<Cow<'password, str>>,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize, PartialEq, Eq, Hash)]
pub struct User {
	pub id: Uuid,
	pub username: String,
}

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq, Eq, Hash)]
#[serde(untagged)]
pub enum ApiResult<T> {
	Ok(T),
	Err(JsonError<'static, 'static>),
}

#[derive(Clone, Debug, Default, Deserialize, Serialize, PartialEq, Eq, Hash)]
pub struct ClientRegistrationParameters {
	pub name: String,
	#[serde(default)]
	pub public: bool,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub preferred_key: Option<String>,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize, PartialEq, Eq, Hash)]
pub struct ClientList {
	#[serde(default)]
	pub public: Vec<Uuid>,
	#[serde(default)]
	pub confidential: Vec<Uuid>,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize, PartialEq, Eq, Hash)]
pub struct LoginRequest {
	pub login_state: String,
	pub username: String,
	pub password: String,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize, PartialEq, Eq, Hash)]
pub struct RedirectUriQuery<'uri> {
	pub uri: Cow<'uri, str>,
}

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq, Eq)]
pub struct JsonWebKeySet {
	pub keys: Vec<JsonWebKey>,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize, PartialEq, Eq, Hash)]
pub struct PreferredKeyUpdate<'id> {
	pub id: Cow<'id, str>,
}

#[derive(Clone, Copy, Debug, Deserialize, Serialize, PartialEq, Eq, Hash)]
#[serde(rename = "snake_case")]
pub enum ClaimDestination {
	IdToken,
	AccessToken,
	UserInfo,
}

impl FromStr for ClaimDestination {
	type Err = &'static str;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		match s {
			"id_token" => Ok(Self::IdToken),
			"access_token" => Ok(Self::AccessToken),
			"user_info" => Ok(Self::UserInfo),
			_ => Err(r#"Expected any of: "id_token", "access_token", "user_info""#),
		}
	}
}

impl Display for ClaimDestination {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.write_str(match self {
			ClaimDestination::IdToken => "id_token",
			ClaimDestination::AccessToken => "access_token",
			ClaimDestination::UserInfo => "user_info",
		})
	}
}

#[derive(Clone, Copy, Debug, Deserialize, Serialize, PartialEq, Eq, Hash)]
#[serde(rename = "lowercase")]
pub enum ClaimType {
	Bool,
	Number,
	String,
	Array,
	Object,
}

impl FromStr for ClaimType {
	type Err = &'static str;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		match s {
			"bool" => Ok(ClaimType::Bool),
			"number" => Ok(ClaimType::Number),
			"string" => Ok(ClaimType::String),
			"array" => Ok(ClaimType::Array),
			"object" => Ok(ClaimType::Object),
			_ => Err(r#"Expected any of: "bool", "number", "string", "array", "object""#),
		}
	}
}

impl Display for ClaimType {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.write_str(match self {
			ClaimType::Bool => "bool",
			ClaimType::Number => "number",
			ClaimType::String => "string",
			ClaimType::Array => "array",
			ClaimType::Object => "object",
		})
	}
}

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq, Eq, Hash)]
pub struct ClaimParameters {
	#[serde(skip_serializing_if = "Option::is_none")]
	pub r#type: Option<ClaimType>,
}

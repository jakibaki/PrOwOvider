# PrOwOvider

A lightweight and headache free (work-in-progress) identity provider

## Features

* Multi-Organisation shared deployment
* OpenID Connect provider (Authorization Code flow only)

## Screenshot

![Login](./static/screenshot-login.png)
